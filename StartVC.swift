//
//  StartVC.swift
//  Food Service
//
//  Created by Hassan  on 15/10/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit



class StartVC: CommonVc , CheckOutVCDelegate {
    
    func myOrderButtonPressed() {
        let story = UIStoryboard(name: "Main", bundle:nil)
        let MyOrdersVc = story.instantiateViewController(withIdentifier: "MyOrdersVC") as! MyOrdersVc
        self.navigationController?.pushViewController(MyOrdersVc, animated: true)
    }
    

    @IBOutlet weak var btn_bookNow: UIButton!
    @IBOutlet weak var btn_shopNow: UIButton!
    @IBOutlet weak var lbl_premiumDelivery: UILabel!
    @IBOutlet weak var lbl_shopOnline: UILabel!
    
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI(button: btn_shopNow)
        setUpUI(button: btn_bookNow)

    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        AppDelegate.sharedInstance.getMinOrderPrice(lable: nil)
    }
    
    
      
// MARK: - Setup UI
    func setUpUI(button:UIButton){


        setupButtonUI(for: button)
        button.layer.borderWidth = CGFloat(1)
        button.layer.borderColor = button.titleColor(for: .normal)?.cgColor



    }
    
    
    
//    MARK: - Button Shop Now Tapped
    @IBAction func btn_shopNow_tapped(){
//        --- side menu segue -----
        if let Drawer = self.drawer(){
            Drawer.setMainWith(identifier: "gotoHome")
        }
    }

//   MARK: - Button Book Now Tapped
    @IBAction func btn_bookNow_tapped() {
 
//            if AppDelegate.sharedInstance.userLoggedIn{
            self.performSegue(withIdentifier: "gotoPremiumDelivery", sender: self)
                
//            }else{
//                loginResquest(VC: self)
//            }
            
        }

    
    
}
