//
//  SearchVc.swift
//  Food Service
//
//  Created by Hassan  on 19/08/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import SCLAlertView
import AudioToolbox
import SDWebImage
import SDWebImageWebPCoder


class SearchVc: CommonVc,UITableViewDataSource,UITableViewDelegate,productCellDelegate,informationVCDelegate {
    
    func update_ProductID_For_AppCart(cell: UITableViewCell) {
        if let indexPath = tbl_searchProducts.indexPath(for: cell){

            tappedProduct = arr_searchedProducts[indexPath.row]
        
        }
    }
    
   
    
    var selected_item_for_Information = 0
//    let searchBar = UISearchBar()
    var rowHeight : CGFloat = 200
    var arr_searchedProducts = [ProductData]()
    
    @IBOutlet weak var lbl_title : UILabel!
    @IBOutlet weak var tbl_searchProducts: UITableView!
    
    
    
    override func viewDidLoad() {
//        self.customNavBar()
        super.viewDidLoad()
        Update_lbl_badge()
        searchBar.becomeFirstResponder()
        searchBar.showsCancelButton = true
        searchProduct(searchTxt: searchBar.text!)
        lbl_title.text = "search result: "+searchBar.text!
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        tbl_searchProducts.reloadData()
        
//         tbl_searchProducts.beginUpdates()
//         tbl_searchProducts.endUpdates()
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_searchedProducts.count
    }
    
            func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = tbl_searchProducts.dequeueReusableCell(withIdentifier: "productcell") as! ProductCell
                var product = [ProductData]()
                product = self.arr_searchedProducts
                cell.delegate = self
                cell.tag = indexPath.row
                
                
                
                
                cell.img_Product.image = UIImage(named: "loading")
                if let url = product[indexPath.row].imageUrl {
                    
                    cell.img_Product.sd_setImage(with: url)

                }else{
                    cell.img_Product.image = UIImage(named: "loading")
                }
                
                if product[indexPath.row].favorite == "yes"{
                    cell.btn_fav.tintColor = UIColor.init(hex: themeColour)
                    cell.btn_fav.isSelected = true
                }else{
                    cell.btn_fav.tintColor = UIColor.lightGray
                    cell.btn_fav.isSelected = false
                }
                
                
               if product[indexPath.row].quantity != "0" {
                    cell.lbl_quantity.isHidden = false
                    cell.txt_quantity.text = product[indexPath.row].quantity
                    cell.lbl_quantity.text = product[indexPath.row].quantity
                }else{
                    cell.lbl_quantity.isHidden = true
                    cell.txt_quantity.text = "0"
                }
                
                if product[indexPath.row].discountEndDate != nil {
                    cell.lbl_discountDate.text =  "Offer Ends\n" + product[indexPath.row].discountEndDate!
                }else{
                    cell.lbl_discountDate.isHidden = true
                }
                
                if let discount = product[indexPath.row].discount,
                    Double(discount) ?? 0 > 0{
                    cell.lbl_discount.text = "\(discount)% OFF "
                    cell.lbl_discount.isHidden = false
                }else{
                    cell.lbl_discount.isHidden = true
                }
                
                cell.lbl_product_id.text = product[indexPath.row].id
//                cell.lbl_product_id.isHidden = true
                cell.lbl_product_title.text = product[indexPath.row].title
//                cell.lbl_product_title.isHidden = true
                
                
                cell.lbl_product_description.text = product[indexPath.row].Description
                
                
                cell.lbl_product_our_price.text = Currency + " " + (product[indexPath.row].Ourprice ?? "0")
                
                if let orignalPrice = product[indexPath.row].orignalPrice ,
                    Int(orignalPrice) ?? 0 > 0 {
                    cell.view_orignal_price.isHidden = false
                    cell.lbl_product_orignal_price.text = Currency + " " + (product[indexPath.row].orignalPrice!)
                }else{
                    cell.view_orignal_price.isHidden = true
                }
                
                
                return cell
            }
    //    MARK: - Table View Delegates
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//           if selectedIndex == indexPath{
//               return rowHeight
//           }
//           return 267
        return 200
       }
       
       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
//           self.changeRowHeight(at: indexPath)
        selected_item_for_Information = indexPath.row
        tappedProduct = arr_searchedProducts[indexPath.row]
           AudioServicesPlaySystemSound(1519)
//           self.tbl_searchProducts.scrollToRow(at: indexPath, at: .none , animated: true)
        self.performSegue(withIdentifier: "gotoInfoVc", sender: self)
        
//           updateRow()
       }
    //    MARK: - Update Row for product in App cart
    func updateRow(){
        
        for i in 0..<AppDelegate.sharedInstance.appCart.count{
            if AppDelegate.sharedInstance.appCart[i].id == tappedProduct.id{
                Row = i
            }
        }
        
    }
    //    MARK: - Change Row Height
//    func changeRowHeight(at indexPath:IndexPath){
//
//            ProductId = arr_searchedProducts[indexPath.row].id!
//
//        if selectedIndex == indexPath{
//            rowHeight = 200
//            tbl_searchProducts.beginUpdates()
//            tbl_searchProducts.endUpdates()
//            selectedIndex = nil
//            rowHeight = 267
//            return
//        }else{
//            selectedIndex = indexPath
//            tbl_searchProducts.beginUpdates()
//            tbl_searchProducts.endUpdates()
//        }
//    }

    
//    MARK: - Product Cell Delegate
    func quantityUpdated(tag: Int) {
           Update_lbl_badge()
           arr_searchedProducts[tag].quantity = AppDelegate.sharedInstance.appCart[Row].quantity
           arr_searchedProducts[tag].favorite = AppDelegate.sharedInstance.appCart[Row].favorite
       }
    
    
    //    MARK: - Serach Bar Delegate
     override func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        lbl_title.text = "search result: "+searchBar.text!
        searchProduct(searchTxt: searchText)

    }
    
    func searchProduct(searchTxt:String){
        APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: ["office_name":office_name,"type":"search_product","search_text":searchTxt]).responseJSON(completionHandler: {(response) in
                    print(response.request?.url)
                    print(response.result.value)
                    
                    self.arr_searchedProducts = []
                    self.tbl_searchProducts.reloadData()
                    switch response.result{
                    case .success:
                        
                        let  dict = response.result.value as! [String:AnyObject]
                        if dict["message"] as! String != "No Product Found"{
                            if let data = dict["DATA"]?["products"] as? [[String : AnyObject]]{
                                for i in 0..<data.count{
                                    let PD = ProductData()
                                    if let productID = data[i]["product_id"] as? String{
                                        PD.id = productID
                                        PD.info = data[i]["product_info"] as? String
//                                        PD.info = PD.info?.withoutHtml
                                        PD.Description = data[i]["product_description"] as? String
//                                        PD.Description = PD.Description?.withoutHtml
                                        PD.Ourprice = data[i]["p_sell_price"] as? String
                                        PD.orignalPrice = data[i]["p_market_cutoff_price"] as? String
                                        PD.Stock = data[i]["product_stock"] as? String
                                        PD.discount = data[i]["p_discount_perc"] as? String
                                        PD.title = data[i]["product_title"] as? String
                                        PD.otherImgUrl = data[i]["other_img"] as? String
                                        
                                        if let rating = data[i]["product_rating"] as? Int{
                                            PD.rating = String(rating)
                                            
                                        }
                                        
                                        if let int_sold = data[i]["product_sold"] as? Int
                                        {
                                            PD.sold = String(int_sold)
                                        }
                                        
                                        
//
                                        PD.quantity = getQunatityFromAppCart(ProductID: PD.id!)
                                        //                            PD.productIndexInTable = i
                                        PD.favorite = data[i]["favorite"] as? String ?? "no"
                                        let imgUrl = (data[i]["product_image"] as? String)?.replaceURL()
                                        PD.imageUrl = URL(string:imgUrl!)
                                        
                                        
                                        
                                        self.arr_searchedProducts.append(PD)
                                    }
                                }
        //                        self.searchProductsEanble = true
                                self.tbl_searchProducts.reloadData()
                            }
                        }else if dict["message"] as! String == "No Product Found"{
//                            self.arr_searchedProducts = []
//                            self.tbl_searchProducts.reloadData()
                        }else {
                            AppDelegate.sharedInstance.showerror(str: dict["message"] as? String)
                        }
                        
                    case .failure(let error):
                        SCLAlertView().showError(error.localizedDescription)
                    }
                    
                })
    }
    
    //    MARK: - Information VC delegate
    func InfoVc_ProductQuantity_Updated(quantity: String) {
        
        arr_searchedProducts[selected_item_for_Information].quantity = quantity
        
    }

      //    MARK: - Prepare for segue
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "gotoInfoVc"{
                
                let destination = segue.destination as! InformationVc
                destination.delegate = self
                destination.productData = AppDelegate.sharedInstance.appCart[Row]
                destination.PriceNProducts = AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: nil)
            }else if segue.identifier == "gotoCheckOut"{
                let destination = segue.destination as! CheckOutVc
                InitiateCart()
                destination.PriceNProducts = AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: nil)
                destination.checkOutCart = cart
            }else if segue.identifier == "gotoCartVc"{
                CartEnabled = true
            }
        }
    
}


