//
//  CommonData.swift
//  Food Service
//
//  Created by Hassan  on 04/03/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import Foundation
import UIKit

// MARK: Singleton Data

// MARK: - OrderPad2Vc
var selectedIndex : IndexPath?
var Row : Int!
var cart = [ProductData]()
var favouriteIsEnabled = false
var tappedProduct = ProductData(){
    didSet{
        updateRow()
    }
}
var CartEnabled = false
var deliveryProductisEnabled = false
var minOrderPRICE = "--"
var specialOffersIsEnabled = false
var IsProductFound = false
//MARK: - Information Vc
 var recomendedProducts = [ProductData]()
var recommendedProductsIsEnabled = false
// MARK: - Delivery Section
var arr_dateNtime = [dateNtime]()
var selectedDateIndex = 0
var selectedStartTimeIndex = 0
var selectedEndTimeIndex = 0
var UserNote = ""
//MARK: - checkout vc
var deliveryType = "delivery_selection"
//MARK: - Choose Branch VC
var selectedBranch = Branch()
   
var senderCheckOutVc = false

func updateAppCart(productID:String,qunatity:String){
    for i in 0..<AppDelegate.sharedInstance.appCart.count{
        if AppDelegate.sharedInstance.appCart[i].id == productID{
            AppDelegate.sharedInstance.appCart[i].quantity = qunatity
            break
        }
    }
}

//func getQunatityFromAppCart(ProductID:String)->String{
//       var q = ""
//    var c = 0
//       for i in 0..<AppDelegate.sharedInstance.appCart.count{
//        print("product ID::> \(AppDelegate.sharedInstance.appCart[i].id)")
//        print(i)
//        c += i
//
//           if AppDelegate.sharedInstance.appCart[i].id == ProductID{
//               q = AppDelegate.sharedInstance.appCart[i].quantity
//            break
//           }
//       }
//    print(c)
//       return q
//   }

func getQunatityFromAppCart(ProductID:String)->String{
    var q = ""
 var c = 0
    for i in 0..<cart.count{
//     print("product ID::> \(AppDelegate.sharedInstance.appCart[i].id)")
     print(i)
     c += i
 
        if cart[i].id == ProductID{
            q = cart[i].quantity
         break
        }
    }
 print(c)
    return q
}


func checkAuth() -> Bool {
    let authType = LocalAuthManager.shared.biometricType
       switch authType {
       case .none:
           print("Device not registered with TouchID/FaceID")
        return false
       case .touchID:
           print("Device support TouchID")
        return true
       case .faceID:
           print("Device support FaceID")
        return false
       }
}
//    MARK: - Custom Border
func DiscountBorder(label:UILabel!){
    let CustomBorder = CAShapeLayer()
    CustomBorder.strokeColor = UIColor.red.cgColor
    CustomBorder.lineDashPattern = [9, 9]
    CustomBorder.frame =  label.bounds
    CustomBorder.fillColor = nil
    CustomBorder.path = UIBezierPath(rect: label.bounds).cgPath
    label.layer.addSublayer(CustomBorder)
}

// MARK: - Login Request

func loginResquest(VC : UIViewController , nextViewController : UIViewController?){
    let story = UIStoryboard(name: "Main", bundle:nil)
    let loginVc = story.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
    if (VC.navigationController) != nil{
        if nextViewController != nil{
        loginVc.nextViewController = nextViewController!
        loginVc.delegate = VC as! LoginVcDelegate
        loginVc.nextViewController = nextViewController!
        }
    VC.navigationController?.pushViewController(loginVc, animated: true)
        
}
}


//    MARK: - Update Row for product in App cart
func updateRow(){
            IsProductFound = false
    for i in 0..<AppDelegate.sharedInstance.appCart.count{
        if AppDelegate.sharedInstance.appCart[i].id == tappedProduct.id{
            Row = i
            IsProductFound = true
        }
    }
    if !IsProductFound{
        AppDelegate.sharedInstance.appCart.append(tappedProduct)
        updateRow()
    }
    
}

//    MARK: - Get Date and Time
func getSelectedDateTime()->(selectedDate:String,selectedStartTime:String,selectedEndTime:String){
   let C_date = Date()
    let date_formater = DateFormatter()
    date_formater.dateFormat = "dd-MM-yyyy"
    let today = date_formater.string(from: C_date)
    if arr_dateNtime.isEmpty{return (today,"","")}
    let selectedDate = arr_dateNtime[selectedDateIndex].dates
    let startTime = arr_dateNtime[selectedDateIndex].StartTime[selectedStartTimeIndex]
    let endtime = arr_dateNtime[selectedDateIndex].endTimeAfterSelection[selectedEndTimeIndex]
    return (selectedDate,startTime,endtime)
    
    
}

// MARK: - setup Button UI
func setupButtonUI(for button: UIView) {
    button.layer.cornerRadius = button.frame.size.height / 10
    button.layer.shadowColor = button.backgroundColor?.cgColor
    button.layer.shadowRadius = 0.0
    button.layer.masksToBounds = false
}



extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension String{
func replaceURL()->String{
   return self.replacingOccurrences(of: ".com", with: ".lk")
}
}
