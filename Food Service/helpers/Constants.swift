//
//  Constants.swift
//  Food Service
//
//  Created by Hassan  on 18/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import Foundation
import UIKit
import Reachability
import SCLAlertView

let AppName = "VShop"
var Currency = "Rs"
let office_name = "vshop"

let themeColour = "7B1384"

let countryCode = "lk" // for google places

let appDelegate = UIApplication.shared.delegate as! AppDelegate
@discardableResult
func internetConnectionIsOk()->Bool{
    do {
        if try Reachability().connection == .unavailable{
            SCLAlertView().showError(AppName,subTitle: "Please check your internet connection")
            return false
        }
        }catch{
        print("error while checking internet connection")
    }
    return true
}
