//
//  APIManager.swift
//  Food Service
//
//  Created by Hassan  on 18/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//


import UIKit

import Alamofire

class APIManager: NSObject {

    static let sharedInstance = APIManager()
    
    
  static  let serverTrustPolicies: [String: ServerTrustPolicy] = [
        "tbmslive.com": .disableEvaluation
    ]
    
    let sessionManager = SessionManager(
        serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
    )
}

