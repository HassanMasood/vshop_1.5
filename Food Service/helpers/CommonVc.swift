//
//  CommonVs.swift
//  Food Service
//
//  Created by Hassan  on 08/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import PKHUD
import SCLAlertView
import MMDrawController
class CommonVc: UIViewController,UISearchBarDelegate,LoginVcDelegate {
    
    
    
    static let sharedInstance = CommonVc()
   var previousVC = UIViewController()
   let searchBar = UISearchBar()
    
    let lbl_itemsInCart = UILabel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        
        searchBar.placeholder = "Search here"
        
        var prefersStatusBarHidden: Bool {
          return true
        }
        setUp_lbl_badge()
//        Update_lbl_badge()
        self.view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        addCustomNavBar(navigationController: self.navigationController)
        
    }
    
  override func viewWillAppear(_ animated: Bool) {
    Update_lbl_badge()
  }
    
    
    func setupButtonUI(for button: UIButton) {
        button.layer.cornerRadius = button.frame.size.height / 10
        button.layer.shadowColor = button.backgroundColor?.cgColor
        button.layer.shadowRadius = 0.0
        button.layer.masksToBounds = false
        
    }

//    MARK: - Custom Back Button
    func addCustomizedBackBtn() {
//        navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back")
//        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back")
//        navigationItem?.backBarButtonItem = UIBarButtonItem(title: "BACK", style: .plain, target: nil, action: nil)
//        navigationItem.backBarButtonItem?.action = #selector(backButton)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "back-2"), style: .plain, target: self, action:#selector(backButton))
    }
    
//    MARK: - Custom Nav Bar
    func addCustomNavBar(navigationController: UINavigationController?) {
//        let CustomView = UIView()
//        CustomView.addSubview(UISearchBar())
        self.navigationItem.titleView = searchBar
        if self.restorationIdentifier != "InformationVc"{
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.init(systemName: "list.dash")?.withTintColor(.white), style: .plain, target: self, action:#selector(SideMenu))
        }
        
        
        let CustomView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        let CartImg = UIImageView(frame: CGRect(x: 0, y: 0, width: 28, height: 28))
        CartImg.image = UIImage.init(systemName: "cart")
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btn.addTarget(self, action: #selector(openCart), for: .touchUpInside)
        CustomView.addSubview(CartImg)
        CustomView.addSubview(btn)
        CustomView.addSubview(lbl_itemsInCart)
        
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: CustomView)
        self.navigationController?.navigationBar.backgroundColor = UIColor(hex:themeColour)
        
    }
//    MARK:  - Button back pressed
    @objc func backButton(){
//        self.dismiss(animated: true, completion: nil)
//        if let Dra = self.drawer(){
//            Dra.showLeftSlider(isShow: false)
////            print(CommonVc.sharedInstance.previousVC)
//            Dra.set(main: MMdrawerPreviousVC.sharedInstace.vc)
//
//        }
        if (self.navigationController?.viewControllers.count)! > 1{
        self.navigationController?.popViewController(animated: true)
        }else{
            if let drawer = self.drawer() {
                drawer.setMainWith(identifier: "gotoHome")
                
            }
        }
        
        
//    self.navigationController?.pushViewController(MMdrawerPreviousVC.sharedInstace.vc, animated: true)
    }
//    MARK: - Button Side Menu Pressed
    @objc func SideMenu() {
        if let drawer = self.drawer() ,
            let manager = drawer.getManager(direction: .left){
            let value = !manager.isShow
            drawer.showLeftSlider(isShow: value)
            
        }
    }
//  MARK: - Button Cart Pressed
    @objc func openCart(){
    InitiateCart()
    if cart.isEmpty {
        appDelegate.showerror(str: "Cart is Empty")
        return
    }
    
    if AppDelegate.sharedInstance.userLoggedIn{
        let story = UIStoryboard(name: "Main", bundle:nil)
        let CartVc = story.instantiateViewController(withIdentifier: "CartVc") as! CartVc
        self.navigationController?.pushViewController(CartVc, animated: true)
        
    }else{
        let story = UIStoryboard(name: "Main", bundle:nil)
        let cartVc = story.instantiateViewController(withIdentifier: "CartVc") as! CartVc
        loginResquest(VC: self , nextViewController: cartVc)
        
    }
    }

//    func openCartForMyorders(priceNproducts:[String:Any]){
////    InitiateCart()
//    if cart.isEmpty {
//        appDelegate.showerror(str: "Cart is Empty")
//        return
//    }
//
//    if AppDelegate.sharedInstance.userLoggedIn{
//        let story = UIStoryboard(name: "Main", bundle:nil)
//        let CartVc = story.instantiateViewController(withIdentifier: "CartVc") as! CartVc
//        self.navigationController?.pushViewController(CartVc, animated: true)
//
//    }else{
//        let story = UIStoryboard(name: "Main", bundle:nil)
//        let cartVc = story.instantiateViewController(withIdentifier: "CartVc") as! CartVc
//        loginResquest(VC: self , nextViewController: cartVc)
//
//    }
//    }
    
//    MARK: - ADD Shadow
    func AddshadowTo(view:UIView){
        view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        view.layer.shadowRadius = 5
        view.layer.masksToBounds = false
    }
    //    MARK: - loginVC Delegate
    func loggedinSucessfully(NextViewController: UIViewController) {
        if NextViewController.restorationIdentifier == "CartVc"{
            perform(#selector(openCart))
        }
        if let dra = self.drawer(){
            if NextViewController.restorationIdentifier == "MyOrdersVC"{
              dra.setMainWith(identifier: "gotoMyOrders")

            }else if NextViewController.restorationIdentifier == "AccountVC"{
                dra.setMainWith(identifier: "gotoProfile")
            }
        }
    }

    //    MARK: - Initiate Cart
    func InitiateCart(){
        cart = []
        for i in 0..<AppDelegate.sharedInstance.appCart.count{
            if Int(AppDelegate.sharedInstance.appCart[i].quantity) ?? 0 > 0 {
                cart.append(AppDelegate.sharedInstance.appCart[i])
            }
        }
    }
    
//    MARK: - Search Bar Delegate
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        let story = UIStoryboard(name: "Main", bundle:nil)
//        let searchVc = story.instantiateViewController(withIdentifier: "searchVc") as! SearchVc
//        self.navigationController?.pushViewController(searchVc, animated: false)
//    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let story = UIStoryboard(name: "Main", bundle:nil)
        let searchVc = story.instantiateViewController(withIdentifier: "searchVc") as! SearchVc
        searchVc.searchBar.text = self.searchBar.text
        searchBar.text = nil
        self.navigationController?.pushViewController(searchVc, animated: false)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        navigationController?.popViewController(animated: false)
        
    }
    
    
//    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        navigationController?.popViewController(animated: false)
//    }
}

//extension String {
//    public var withoutHtml: String {
//        guard let data = self.data(using: .utf8) else {
//            return self
//        }
//
//        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
//            .documentType: NSAttributedString.DocumentType.html,
//            .characterEncoding: String.Encoding.utf8.rawValue
//        ]
//
//        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
//            return self
//        }
//
//        return attributedString.string
//    }
//}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var withoutHtml: String { html2AttributedString?.string ?? "" }
}

extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var withoutHtml: String {
        html2AttributedString?.string ?? ""
    }
}



extension CommonVc{
    
    
    func setUp_lbl_badge(){
        lbl_itemsInCart.frame = CGRect(x: 15, y: -18, width: 20, height: 20)
                
                
//                lbl_itemsInCart.layer.borderWidth = CGFloat(2)
//                lbl_itemsInCart.layer.borderColor = UIColor(hex: themeColour)!.cgColor
        //        lbl_itemsInCart.adjustsFontSizeToFitWidth = true
                lbl_itemsInCart.textColor = .white
                lbl_itemsInCart.font = UIFont.boldSystemFont(ofSize: 14)
                lbl_itemsInCart.addConstraint(NSLayoutConstraint(item: lbl_itemsInCart, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 25))
                lbl_itemsInCart.textAlignment = .center
                lbl_itemsInCart.backgroundColor = .red
                lbl_itemsInCart.layer.shadowColor = UIColor(red: 123, green: 100, blue: 122, alpha: 0.23).cgColor
          
                lbl_itemsInCart.layer.cornerRadius = lbl_itemsInCart.layer.frame.height / 2
                lbl_itemsInCart.widthAnchor.constraint(greaterThanOrEqualToConstant: 20).isActive = true

        //        lbl_itemsInCart.translatesAutoresizingMaskIntoConstraints = false
                lbl_itemsInCart.clipsToBounds = true
                lbl_itemsInCart.layer.masksToBounds = true
    }
    
    
    func Update_lbl_badge(){
        
        let PriceNProducts = AppDelegate.sharedInstance.UpdateCost()
        
        
        lbl_itemsInCart.text = PriceNProducts["total_items"] as? String ?? "0"
        
        if lbl_itemsInCart.text! == "0"{
            lbl_itemsInCart.isHidden = true
        }else{
            lbl_itemsInCart.isHidden = false
        }
        
        
        if lbl_itemsInCart.text?.count == 2 || lbl_itemsInCart.text?.count == 1{
            lbl_itemsInCart.frame = CGRect(x: 12, y: -11, width: 20, height: 20)
        }
        else if lbl_itemsInCart.text!.count == 3{
            lbl_itemsInCart.frame = CGRect(x: 10, y: -11, width: 30, height: 20)
        }
        else if lbl_itemsInCart.text!.count > 3{
           lbl_itemsInCart.frame = CGRect(x: 6, y: -11, width: 40, height: 20)
        }
        
        let IntItems = Int(lbl_itemsInCart.text!)
        if IntItems! > 999 {
            
            lbl_itemsInCart.text = "999+"
        }
        
        
    }
    
    
    
}

