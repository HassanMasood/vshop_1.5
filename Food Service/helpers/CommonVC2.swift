//
//  CommonVC2.swift
//  Food Service
//
//  Created by Hassan  on 30/12/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit

class CommonVC2: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        addCustomBackButton()
        // Do any additional setup after loading the view.
    }
    

    func addCustomBackButton(){
//        self.navigationController?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "back-2"), style: .plain, target: self, action: #selector(backButton))
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "back-2"), style: .plain, target: self, action:#selector(backButton))
        
    }
    
    @objc func backButton(){
        self.navigationController?.popViewController(animated: true)
    }

}
