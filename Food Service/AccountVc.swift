//
//  ProfileVc.swift
//  Food Service
//
//  Created by Hassan  on 18/03/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import SCLAlertView
import PKHUD


class AccountVc: CommonVc , PersonalVcDelegate {
    
    

    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_mobile: UILabel!
    @IBOutlet weak var lbl_address: UILabel!
    @IBOutlet weak var lbl_city: UILabel!
    @IBOutlet weak var lbl_post_code: UILabel!
    @IBOutlet weak var lbl_email_address: UILabel!
    @IBOutlet weak var btn_update: UIButton!
    @IBOutlet weak var stack_view_profile: UIStackView!
    @IBOutlet weak var btn_reset_password: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if AppDelegate.sharedInstance.userLoggedIn{
            setUp()
//        }else{
//            loginResquest(VC: self )
//        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if AppDelegate.sharedInstance.userLoggedIn{
            setUp()
            
        }else{
            btn_reset_password.isHidden = true
            btn_update.isHidden = true
        }
        Update_lbl_badge()
    }
    
//  MARK:- SetUp UI
    func setUp(){
        fetchProfile()
        btn_update.isHidden = false
        btn_reset_password.isHidden = false
        setupButtonUI(for: btn_update)
    }
    
    func updateButtonPressed() {
        fetchProfile()
    }
//    MARK: - Fetch Profile
    func fetchProfile(){
        if internetConnectionIsOk(){
        let user_info = UserDefaults.standard.value(forKey: "user_information") as! [String:AnyObject]
        let parameter = ["office_name":office_name,"type":"user_profile","email":user_info["email"] as! String ] as [String:String]
        HUD.show(.progress)
        APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: {(response) in
            HUD.hide()
            print(response.request?.url)
            print(response.result.value)
            switch response.result{
            case .success:
            let dict = response.result.value as! [String:AnyObject]
            if let msg = dict["message"]?["msg"] as? String{
                if msg  == "user profile" {
                let user_profile = dict["DATA"]!["user_profile"] as! [String:AnyObject]
                if let name = user_profile["name"] as? String{
                self.lbl_name.text = name
                }
                if let address = user_profile["address"] as? String{
                self.lbl_address.text = address
                }
                if let town = user_profile["town"] as? String{
                self.lbl_city.text = town
                }
                if let mobile = user_profile["mobile"] as? String{
                self.lbl_mobile.text = mobile
                }
                if let email = user_profile["email"] as? String{
                self.lbl_email_address.text = email
                }
                if let post_code = user_profile["post_code"] as? String{
                self.lbl_post_code.text = post_code
                }
                }else{
                    SCLAlertView().showError(msg)
                }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    AppDelegate.sharedInstance.showerror(str: error.localizedDescription)
                }
                
        }
        })
    }
    }
    

//    MARK: - Button Update
    @IBAction func Update(_ sender: Any) {
        
//        let Appearance = SCLAlertView.SCLAppearance()
//        let alert = SCLAlertView(appearance: Appearance)
//
//        let firstName = alert.addTextField("Enter Name")
////        let lastName = alert.addTextField("Last Name")
////        var email = UITextField()
//        let address = alert.addTextField("Enter Address")
//        let town = alert.addTextField("Enter City")
//        let postCode = alert.addTextField("Enter Post Code")
//        let mobile = alert.addTextField("Enter Mobile Number")
//
//        let name = lbl_name.text
//        let arr_name = name?.components(separatedBy: " ")
//
//        firstName.text = arr_name![0]
////        lastName.text = arr_name![1]
////        email.text = lbl_email_address.text
//        town.text = lbl_city.text
//        address.text = lbl_address.text
//        postCode.text = lbl_post_code.text
//        mobile.text = lbl_mobile.text
//
//        postCode.keyboardType = .numberPad
//        mobile.keyboardType = .namePhonePad
//
////        email.isEnabled = false
////        email.backgroundColor = UIColor.lightGray
//
//        alert.addButton("Done") {
//            if internetConnectionIsOk() {
//                let parameter = ["office_name":office_name , "type":"edit_user_profile" , "email":self.lbl_email_address.text , "name":firstName.text! ,"mobile":mobile.text! , "address":address.text! , "city":town.text! , "post_code":postCode.text! ,"title":""]
//                    HUD.show(.progress)
//                    APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: {(response) in
//                        HUD.hide()
//                        print(response.request?.url)
//                        print(response.result.value)
//                        switch response.result{
//                        case .success:
//                            let dict = response.result.value as! [String:AnyObject]
//                            if let msg = dict["message"]?["msg"] as? String{
//                                if msg != "user profile"{
//                                    appDelegate.showerror(str:msg)
//                                }else{
//                                    self.fetchProfile()
//                                    self.updateUserInfo(dict: dict["DATA"]!["user_profile"] as! [String:AnyObject])
//                                }
//                            }
//                        case .failure(let error):
//                            DispatchQueue.main.async {
//                                appDelegate.showerror(str: error.localizedDescription)
//                            }
//                        }
//                    })
//
//
//            }
//        }
//        alert.showEdit(AppName, subTitle: "Update Profile", closeButtonTitle: "Cancel", timeout: .none, colorStyle: 999999,   animationStyle: .bottomToTop)
    }
    

    

//    MARK: - Side Menu
    @IBAction func sideMenu(_ sender: Any) {
           
               if let drawer = self.drawer() ,
                   let manager = drawer.getManager(direction: .left){
                   let value = !manager.isShow
                   drawer.showLeftSlider(isShow: value)
               }
           
       }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? PersonalRegVc{
        destination.sender = self
        destination.delegate = self
            destination.navigationController?.navigationBar.isHidden = true
        }
    }
    
}
