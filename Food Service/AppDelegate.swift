//
//  AppDelegate.swift
//  Food Service
//
//  Created by Hassan  on 06/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit

import CoreData
import PKHUD
import UserNotifications

import IQKeyboardManagerSwift
import SCLAlertView
import Reachability

import CoreLocation
import SDWebImage
import SDWebImageWebPCoder

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,CLLocationManagerDelegate {
    
    
    static var sharedInstance = AppDelegate()
    var window: UIWindow?
    var appCart = [ProductData]()
    var deliveryStatus = "0"
    var userLoggedIn = false
     var reachability : Reachability!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//       print("Document Directory", FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))
        
        if let loginInfo = UserDefaults.standard.value(forKey: "user_information") as? [String:String]{
            AppDelegate.sharedInstance.userLoggedIn = true
        }
        IQKeyboardManager.shared.enable = true
        
        let WebPCoder = SDImageWebPCoder.shared
        SDImageCodersManager.shared.addCoder(WebPCoder)
        
        do{
        try self.reachability = Reachability()
        }catch{
            print("ERROR: Could not create reachablitity object")
        }

        do
        {
           try reachability?.startNotifier()
        }
        catch
        {
           print( "ERROR: Could not start reachability notifier." )
        }
        
        return true
    }

    // MARK:- UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {

    }

//    MARK: - Show Alert
   func showerror(str:String!){
          let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
              // action here
          }
          SCLAlertView().showError(AppName, subTitle:str,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
      }
    
    func showSuccess(str:String!){
        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
            // action here
        }
        SCLAlertView().showSuccess(AppName, subTitle:str,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 1.0, timeoutAction:timeoutAction))
    }

    //MARK:- Change RootView
       
       func changeRootViewController(with identifier:String!) {
           let storyboard = self.window?.rootViewController?.storyboard
           let desiredViewController = storyboard?.instantiateViewController(withIdentifier: identifier)
           
           let snapshot:UIView = (self.window?.snapshotView(afterScreenUpdates: true))!
           desiredViewController?.view.addSubview(snapshot);
           
           self.window?.rootViewController = desiredViewController;
           
           UIView.animate(withDuration: 0.3, animations: {() in
               snapshot.layer.opacity = 0;
               snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5);
           }, completion: {
               (value: Bool) in
               snapshot.removeFromSuperview();
           });
       }

 //    MARK: - Update Minimum Price
//    withLabel lbl_min_price:UILabel?
    func getMinOrderPrice(lable : UILabel?){
//    if minOrderPRICE == "--"{}else{return}
//       if internetConnectionIsOk(){
        let parameter = ["type":"min_price","office_name":office_name] as [String:String]
//           HUD.show(.progress)
           APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: {(response)in
//               HUD.hide()
            print(response.request?.url)
               print(response.result.value)
               switch response.result{
               case .success:
                   let dict = response.result.value as! [String:AnyObject]
                   if dict["message"] as! String == "success"{
                    var minPrice = dict["DATA"]!["min_price"] as! [AnyObject]
//                       if let lbl = lbl_min_price{
                       minOrderPRICE = (" \(Currency) \(minPrice[0]["minimum_price"] as! String)")
                    lable?.text = minOrderPRICE
//                       }
                   }else{
                       appDelegate.showerror(str: dict["DATA"]?["message"] as? String)
                   }
                   
               case .failure(let error):
                   appDelegate.showerror(str: error.localizedDescription)
               }
               
           })
//        }
   }
        
        
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "coreData")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

//    MARK: - show popup
    func showPopUP(message:String, sender : UIViewController){
        let alert = UIAlertController(title: office_name, message: message, preferredStyle: .alert)
        
        let subview :UIView = alert.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        
        alertContentView.backgroundColor = UIColor.init(hex: themeColour)
        
        sender.present(alert, animated: true) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
        
        
    }

    //    MARK: - Update Cost
        @discardableResult
    func UpdateCost(lbl_itemsCost:UILabel? = nil ,of cart:[ProductData] = AppDelegate.sharedInstance.appCart)->[String:Any] {
            var noOfItems = 0
            var noOfproducts = 0
            var totalPrice = 0.00
            
    //       to save product ids and qunatity in a string to save in databse (comma seprated)
            var pIds = ","
            var pQunatities = ","
            
            for i in 0..<cart.count{
                if Int(cart[i].quantity) ?? 0 > 0 {
                    
                    noOfItems += Int(cart[i].quantity)!
                    pIds += cart[i].id! + ","
                    pQunatities += cart[i].quantity + ","
                    let quantity = Double(cart[i].quantity )!
                    var discount : Double = 0.00
                    
//                    if let d = Double(cart[i].discount ?? "0.00"),
//                    d > 0.00 {
//                        discount = d
//                    }
//                    print(discount)
//                    totalPrice += (Double(appCart[i].price ?? "0.00")! - discount ) * quantity
//                    print(totalPrice)
                    totalPrice += ( Double(cart[i].Ourprice ?? "0.00")! - ( (Double(cart[i].Ourprice ?? "0.00")! * discount ) / 100.0 ) ) * quantity
                    
                    noOfproducts += 1
                }
                
            }
//            print(totalPrice)
            
          totalPrice = totalPrice.rounded(toPlaces : 2)
//            print(totalPrice)
         var productIds = pIds.dropLast()
            var productQunatities = pQunatities.dropLast()
             productIds = productIds.dropFirst()
            productQunatities = productQunatities.dropFirst()
            
            
            if lbl_itemsCost != nil {
                lbl_itemsCost!.text = "\(noOfItems) items of \(noOfproducts) Products / \(Currency) \(totalPrice)"
            }
            
            
            return ["total_price":String(totalPrice),"total_products":String(noOfproducts),"total_items":String(noOfItems),"productIds":String(productIds),"productQunatites":String(productQunatities)]
        }
    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
}



