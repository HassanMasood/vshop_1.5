//
//  DataBaseHelper.swift
//  Food Service
//
//  Created by Hassan  on 10/03/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//
import UIKit
import Foundation
import CoreData
class DataBaseHelper{
    static var sharedInstance = DataBaseHelper()
    let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
    
//    MARK: - save
    func save(object:[String:String]){
        let orderData = NSEntityDescription.insertNewObject(forEntityName: "Order", into: context!) as! Order
        
       let  DateNtime = getSelectedDateTime()
        
        orderData.selectedDate = DateNtime.selectedDate
        orderData.selectedTimeSlot = DateNtime.selectedStartTime + " - " + DateNtime.selectedEndTime
        orderData.deliveryType = deliveryType
        orderData.address = object["address"]
        if deliveryType == "click_and_collect"{
            orderData.address = selectedBranch.address
        }
        orderData.name = object["name"]
        orderData.totalPrice = object["total_price"]
        orderData.totalProducts = object["total_products"]
        orderData.userId = object["id"]
        orderData.town = object["town"]
        orderData.productQunatities = object["productQunatites"]
        orderData.productIds = object["productIds"]
        orderData.orderDate = Date()
        
        do{
            try context?.save()
        }catch{
            print("Error saving data. Error:",error)
        }
    }
// MARK: - Fetch
    func fetch(Predicate:NSPredicate)->[Order]{
        var order:[Order] = []
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Order")
        fetchRequest.predicate = Predicate
        do{
            order = try context!.fetch(fetchRequest) as! [Order]
        }catch{
            print("Error fetching data. Error:",error)
        }
        return order
    }
    
//    MARK: - Delete Object
    func delete(objectID:NSManagedObjectID){
        var objectIDs = [NSManagedObjectID]()
        objectIDs.append(objectID)
        let deleteRequest = NSBatchDeleteRequest(objectIDs: objectIDs)
        do {
            try context!.execute(deleteRequest)
        } catch let error as NSError {
          print("Could not delete  data. \(error), \(error.userInfo)")
        }
    }
    
//    MARK: - delete lastest entry
    func deleteLastestEntry(){
        let startDate = Date()
        let endDate = Calendar.current.date(byAdding: .hour, value: -24, to: startDate)!
        let user_info = UserDefaults.standard.value(forKey: "user_information") as! [String:AnyObject]
        let predicate = NSPredicate(format: "(orderDate >= %@) AND (orderDate <= %@) AND (userId == %@) ", endDate as NSDate, startDate as NSDate,user_info["user_id"] as! String)
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Order")
        fetchRequest.predicate = predicate
        do{
             let result = try context!.fetch(fetchRequest)
                    let lastEntry = result.count - 1
                    context?.delete(result[lastEntry])
                    try context?.save()
            
                    
            
        }catch let error as NSError{
            print(error)
        }
        
         

    }
}
