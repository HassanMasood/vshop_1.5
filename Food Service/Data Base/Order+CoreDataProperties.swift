//
//  Order+CoreDataProperties.swift
//  Food Service
//
//  Created by Hassan  on 11/03/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//
//

import Foundation
import CoreData


extension Order {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Order> {
        return NSFetchRequest<Order>(entityName: "Order")
    }

    @NSManaged public var address: String?
    @NSManaged public var name: String?
    @NSManaged public var orderDate: Date?
    @NSManaged public var orderID: Int16
    @NSManaged public var productIds: String?
    @NSManaged public var selectedDate: String?
    @NSManaged public var selectedTimeSlot: String?
    @NSManaged public var town: String?
    @NSManaged public var totalPrice: String?
    @NSManaged public var totalProducts: String?
    @NSManaged public var userId: String?
    @NSManaged public var productQunatities: String?
    @NSManaged public var deliveryType: String?

}
