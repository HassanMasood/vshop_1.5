//
//  DeliverySectionVc.swift
//  Food Service
//
//  Created by Hassan  on 21/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import SCLAlertView
import PKHUD

protocol DeliverySelectionDeledate: class {
    func dateSelected()
}

class DeliverySectionVc: CommonVc,UIPickerViewDataSource,UIPickerViewDelegate {
    
    
    @IBOutlet weak var pickerview_time: UIPickerView!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var btn_select_date: UIButton!
    @IBOutlet weak var btn_select_time_slot: UIButton!
    @IBOutlet weak var txt_date: UITextField!
    @IBOutlet weak var txt_time: UITextField!
    @IBOutlet weak var stack_buttons: UIStackView!
    @IBOutlet weak var stack_lbl_no_time_slots: UIStackView!
    @IBOutlet weak var stack_text_fields: UIStackView!
    @IBOutlet weak var lbl_slot_expiry: UILabel!
    
    
    var sender = UIViewController()
    weak var delegate : DeliverySelectionDeledate?

    //    MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        self.title = "Delivery Section"
        
        fetchDateTime()
        
    }
    
    //    MARK: - SetUP
    func setUp(){
        setupButtonUI(for:btn_submit)
    }
    //    MARK: - Button Submit
    @IBAction func Submit(_ sender: UIButton) {
        let scAlert = SCLAlertView()
        let textField = UITextField()
        textField.placeholder = "Please let us know about any parking or access restrictions to the delivery point."
        var txtView = scAlert.addTextView()
        scAlert.addButton("OK") {
            UserNote = txtView.text!
            if senderCheckOutVc {
                self.delegate?.dateSelected()
                self.dismiss(animated: true, completion: nil)
                senderCheckOutVc = false
            }else{
//                ----- segue perfromed by side menu ------
                if let Dra = self.drawer(){
                    Dra.showLeftSlider(isShow: false)
                    Dra.setMainWith(identifier: "gotoOrderPad2Vc")
                }

                
//            self.performSegue(withIdentifier: "gotoorderPad2", sender: self)
            }
        }
        if arr_dateNtime.isEmpty == false{
            
            scAlert.showCustom("Your Time Slot",subTitle: "Date:  \(arr_dateNtime[selectedDateIndex].dates)\nTime: \(arr_dateNtime[selectedDateIndex].StartTime[selectedStartTimeIndex]) - \(arr_dateNtime[selectedDateIndex].endTimeAfterSelection[selectedEndTimeIndex])\nSpecial delivery instructions:", color: .gray, closeButtonTitle: "Cancel", timeout: nil, circleIconImage: .actions, animationStyle: .topToBottom)
            
        }else{
            let date = Date()
            let dateFormatter = DateFormatter()
//            dateFormatter.f
        scAlert.showInfo("Special delivery instructions:",subTitle: "Special delivery instructions:",closeButtonTitle: "Cancel")
        }
        
    }
    
    
    
    //   MARK:- PickerView Delegate & DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if btn_select_date.isSelected{
            return 1
        }else if btn_select_time_slot.isSelected{
            return 3
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if btn_select_date.isSelected{
            return arr_dateNtime.count
        }else if btn_select_time_slot.isSelected{
            if component == 2{
                return arr_dateNtime[selectedDateIndex].endTimeAfterSelection.count
            }else if component == 0{
                return arr_dateNtime[selectedDateIndex].StartTime.count
            }else if component == 1 {
                return 1
            }
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if btn_select_date.isSelected{
            return arr_dateNtime[row].dates
        }else if btn_select_time_slot.isSelected {
            if component == 2{
                return arr_dateNtime[selectedDateIndex].endTimeAfterSelection[row]
            }else if component == 0{
                return arr_dateNtime[selectedDateIndex].StartTime[row]
            }else if component == 1 {
               return "To"
            }
        }
        return ""
    }
    
//    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
//        if component == 1 {
//         return   NSAttributedString(string: "TO", attributes: [NSAttributedString.Key.foregroundColor :UIColor.green ])
//        }
//        return NSAttributedString()
//    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if btn_select_time_slot.isSelected{
            updateTime_slots(row: row, component: component)
            pickerview_time.reloadAllComponents()
            DispatchQueue.main.async {
                selectedStartTimeIndex = pickerView.selectedRow(inComponent: 0)
                selectedEndTimeIndex = pickerView.selectedRow(inComponent: 2)
            }
            
            
        }
        else if btn_select_date.isSelected{
            selectedDateIndex = row
        }
        setUpDateTimeTextFields()
        
    }
    
    
    //    MARK: - Select time Slot
    @IBAction func selectTimeSlot(_ sender: Any) {
        btn_select_time_slot.isSelected = true
        btn_select_date.isSelected = false
        pickerview_time.reloadAllComponents()
    }
    
    
    
    //    MARK: - Select Date
    @IBAction func selectDate(_ sender: Any) {
        btn_select_time_slot.isSelected = false
        btn_select_date.isSelected = true
        pickerview_time.reloadAllComponents()
    }
    
    
    //    MARK:- Update Time Array
    func updateTime_slots(row:Int,component:Int){
        
        if component == 0 {
            arr_dateNtime[selectedDateIndex].endTimeAfterSelection = []
            for i in row..<arr_dateNtime[selectedDateIndex].endTime.count{
                arr_dateNtime[selectedDateIndex].endTimeAfterSelection.append(arr_dateNtime[selectedDateIndex].endTime[i])
            }
        }
        
        
    }
    
    //    MARK: - fetch date & time slots
    func fetchDateTime(){
        if internetConnectionIsOk(){
            let C_date = Date()
            let date_formater = DateFormatter()
            date_formater.dateFormat = "dd-MM-yyyy"
            let today = date_formater.string(from: C_date)
            //            print(today)
            HUD.show(.progress)
            var parameters = ["slot_date":today,"type":"future_slot_details","office_name":office_name] as [String:String]
            if deliveryType == "click_and_collect"{
                date_formater.dateFormat = "HH:mm"
                let time = date_formater.string(from: C_date)
                print(time)
                parameters = ["slot_date":today,"slot_time":time,"type":"click_collect_slots","office_name":office_name]  as [String:String]
            }
//            let parameters = ["slot_date":today,"type":"future_slot_details","office_name":office_name]
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameters).responseJSON(completionHandler: {(response) in
                HUD.hide()
//                print(response.result.value)
//                print(response.request?.url)
                switch response.result{
                case .success:
                    let dict = response.result.value as! [String:AnyObject]
                    if dict["message"] as! String == "Available Slots"{
                        let slots =  dict["DATA"]!["slots"] as! [[String : AnyObject]]
                        arr_dateNtime = []
                        for i in 0..<slots.count{
                            let date_time = dateNtime()
                            date_time.dates = slots[i]["date"] as! String
                             let time = slots[i]["available_slots"] as! [[String:AnyObject]]
                            for j in 0..<time.count{
                                date_time.available_timeSlots.append(time[j]["start_time"] as! String)
                            }
                            
                            arr_dateNtime.append(date_time)
                        }
                        self.updateTimes()
                        self.btn_select_date.isSelected = true
                        self.setUpDateTimeTextFields()
                        self.pickerview_time.reloadAllComponents()
                        self.calculateExpiryDate()
                    }else if dict["message"] as! String == "Any slot not available"{
//                        self.stack_buttons.isHidden = true
//                        self.stack_text_fields.isHidden = true
//                        self.stack_lbl_no_time_slots.isHidden = false
//                        self.lbl_slot_expiry.text = ""
//                        self.pickerview_time.isHidden = true
//                        self.btn_select_date.isEnabled = false
//                        self.btn_select_time_slot.isEnabled = false
                        
//                        --- side menu segue ---
                        if let Dra = self.drawer(){
                            Dra.showLeftSlider(isShow: false)
                            Dra.setMainWith(identifier: "gotoOrderPad2Vc")
                        }

                        
                    }
                    else{
                        SCLAlertView().showError(dict["message"] as! String)
                    }
                case .failure(let error):
                    SCLAlertView().showError(error.localizedDescription)
                }
            })
        }
    }
    
    //    MARK:- setUp Date Time TextFields
    func setUpDateTimeTextFields(){
        DispatchQueue.main.async {
            self.txt_date.text = arr_dateNtime[selectedDateIndex].dates
            let strtTime = arr_dateNtime[selectedDateIndex].StartTime[selectedStartTimeIndex]
            print("textfield")
            print(selectedEndTimeIndex)
            print(arr_dateNtime[selectedDateIndex].endTimeAfterSelection.count)
            let endTime = arr_dateNtime[selectedDateIndex].endTimeAfterSelection[selectedEndTimeIndex]
            self.txt_time.text = "\(strtTime) - \(endTime)"
            self.calculateExpiryDate()
        }
        
    }
    //   MARK: - Update Start_time and End_time
    func updateTimes(){
        ////    updates start and end time for each date
        for i in 0..<arr_dateNtime.count{
            for j in 0..<arr_dateNtime[i].available_timeSlots.count-3{
                arr_dateNtime[i].StartTime.append(arr_dateNtime[i].available_timeSlots[j])
            }
        }
        
        for j in 0..<arr_dateNtime.count{
            for k in 3..<arr_dateNtime[j].available_timeSlots.count{
                arr_dateNtime[j].endTime.append(arr_dateNtime[j].available_timeSlots[k])
                arr_dateNtime[j].endTimeAfterSelection.append(arr_dateNtime[j].available_timeSlots[k])
            }
        }
        
    }
    
    //    MARK: - Prepare For Segue
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let nav = segue.destination as? UINavigationController,
//            let vc = nav.topViewController as? OrderPad2Vc{
//            vc.barBackButtonIsEnabled = true
//            vc.sender = self
//
//        }
//    }
    
//    MARK: - Update expiry date
    func calculateExpiryDate(){
        if arr_dateNtime.isEmpty == false{
            if arr_dateNtime[selectedDateIndex].endTimeAfterSelection.isEmpty == false {
                let today = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                dateFormatter.locale = Locale(identifier: "en")
                let SD = dateFormatter.date(from: arr_dateNtime[selectedDateIndex].dates + " " + arr_dateNtime[selectedDateIndex].endTimeAfterSelection[selectedEndTimeIndex])
                let sDate = dateFormatter.string(from: SD!)
                let selectedDate = dateFormatter.date(from: sDate)
                print("Today From dateFormatter = " + dateFormatter.string(from: today))
                print(" selected date From date Fromatter = " + dateFormatter.string(from:selectedDate!))
                let cal = Calendar.current
                let components = cal.dateComponents([.minute], from: today, to: selectedDate!)
                var minutes  = components.minute!
                
                
                
                if minutes > 0 {
                    var dayString = ""
                    var hoursString = ""
                    var mintuesString = ""
                    let days =  minutes / 1440
                    minutes = minutes % 1440
                    let hours = minutes / 60
                    minutes = minutes % 60
                    if days > 0  {
                        dayString = String(days) + "d "
                    }
                    if hours > 0 {
                        hoursString = String(hours) + "h "
                    }
                    if minutes > 0 {
                        mintuesString = String(minutes) + "m "
                    }
                    lbl_slot_expiry.text = "This slot expires in \(dayString)\(hoursString)\(mintuesString)"
                }else{
                    lbl_slot_expiry.text = "Time slot expired"
                }
                
            }
        }
    }
    
//    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
//      return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
//    }
    
    
//    MARK:- uwind to delivery section
    @IBAction func unwindToDeliveryVc(_ sender: UIStoryboardSegue) {
    
    }
}
