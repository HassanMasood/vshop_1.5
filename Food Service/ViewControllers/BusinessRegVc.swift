//
//  BusinessRegVc.swift
//  Food Service
//
//  Created by Hassan  on 08/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit

class BusinessRegVc: CommonVc {

       @IBOutlet weak var view_Personalinfo: UIView!
       @IBOutlet weak var view_business_info: UIView!
       @IBOutlet weak var view_Address: UIView!
       @IBOutlet weak var view_phone1: UIView!
       @IBOutlet weak var view_Authentication: UIView! 
       @IBOutlet weak var txt_user_name: UITextField!
       @IBOutlet weak var txt_business_name: UITextField!
       @IBOutlet weak var txt_trading_name: UITextField!
       @IBOutlet weak var txt_business_type: UITextField!
       @IBOutlet weak var txt_country: UITextField!
       @IBOutlet weak var txt_address: UITextField!
       @IBOutlet weak var txt_post_code2: UITextField!
       @IBOutlet weak var txt_city: UITextField!
       @IBOutlet weak var txt_Num_code1: UITextField!
       @IBOutlet weak var txt_num_type1: UITextField!
       @IBOutlet weak var txt_mobile_num: UITextField!
       @IBOutlet weak var txt_email: UITextField!
       @IBOutlet weak var txt_email_confirm: UITextField!
       @IBOutlet weak var txt_password: UITextField!
       @IBOutlet weak var txt_password_confirm: UITextField!
       
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    

   func setupUI(){
    
   }

}
