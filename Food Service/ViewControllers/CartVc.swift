//
//  CartVc.swift
//  Food Service
//
//  Created by Hassan  on 05/05/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import SCLAlertView
import PKHUD
import AudioToolbox

@objc protocol CartVCDelegate: class{
    func CartVCBackButtonPressed()
}

class CartVc: CommonVC2,UITableViewDelegate,UITableViewDataSource,productCellDelegate,informationVCDelegate {
    
    
    
    var priceNproducts = [String:Any]()
    var selected_item_for_Information = 0

    var rowHeight : CGFloat = 267
    
    weak var delegate : CartVCDelegate?
    
    @IBOutlet weak var min_price: UILabel!
    @IBOutlet weak var btn_remove_cart: UIBarButtonItem!
    @IBOutlet weak var lbl_itemsCost: UILabel!
    @IBOutlet weak var tbl_products: UITableView!
    @IBOutlet weak var lbl_delivery_charges_title:UILabel!
    @IBOutlet weak var lbl_delivery_charges:UILabel!
    
    var deliveryCharges = 0;
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        self.title = "Cart"
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        selectedIndex = nil
//        tbl_products.beginUpdates()
//        tbl_products.endUpdates()
//        tbl_products.refresh()
        CartEnabled = true
//        InitiateCart()
        AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: lbl_itemsCost,of: cart)
//        Update_lbl_badge()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        delegate?.CartVCBackButtonPressed()
    }
//    MARK: - Product Cell delegate
    func quantityUpdated(tag: Int) {
        AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: lbl_itemsCost)
//        Update_lbl_badge()
        cart[tag].quantity = AppDelegate.sharedInstance.appCart[Row].quantity
        cart[tag].favorite = AppDelegate.sharedInstance.appCart[Row].favorite
        
    }
    
    func update_ProductID_For_AppCart(cell: UITableViewCell) {
        if let indexPath = tbl_products.indexPath(for: cell){
            
            tappedProduct = cart[indexPath.row]
        }
    }
    
    func deleteItemButtonPressed(cell: UITableViewCell) {
        let indexpath = tbl_products.indexPath(for: cell)
        var arr_indexpath = [IndexPath]()
        arr_indexpath.append(indexpath!)
//        ----- Removing Item from App Cart ------
        let pID = cart[indexpath!.row].id
        for i in 0..<AppDelegate.sharedInstance.appCart.count{
            if pID == AppDelegate.sharedInstance.appCart[i].id{
                
                AppDelegate.sharedInstance.appCart[i].quantity = "0"
                
                break
            }
            
        }
        
        
        AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: lbl_itemsCost)
//        Update_lbl_badge()
        
        cart.remove(at: indexpath!.row)
        tbl_products.deleteRows(at: arr_indexpath, with: .left)
        if cart.isEmpty{
            self.navigationController?.popViewController(animated: true)
        }else{
        tbl_products.refresh()
        tbl_products.reloadData()
        }
    }
//    MARK:- SetUP
    func setup(){
        min_price.text = minOrderPRICE
        btn_remove_cart.tintColor = .black
        
        if deliveryCharges <= 0{
            lbl_delivery_charges_title.text = "Free Delivery"
        }else{
            lbl_delivery_charges_title.text = "Delivery Charges:"
            lbl_delivery_charges.text = "Rs \(deliveryCharges)"
        }
        
    }
//    MARK: - TableView Data Sources
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cart.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "productcell") as! ProductCell
        cell.delegate = self
        cell.tag = indexPath.row
        
        
        cell.img_Product.image = UIImage(named: "loading")
        if let url = cart[indexPath.row].imageUrl {
            
            cell.img_Product.sd_setImage(with: url)

        }else{
            cell.img_Product.image = UIImage(named: "loading")
        }
        
        
        if cart[indexPath.row].favorite == "yes"{
            cell.btn_fav.tintColor = UIColor.init(hex: themeColour)
            cell.btn_fav.isSelected = true
        }else{
            cell.btn_fav.tintColor = UIColor.lightGray
            cell.btn_fav.isSelected = false
        }
        
        
        if cart[indexPath.row].quantity != "0" {
            cell.lbl_quantity.isHidden = false
            cell.txt_quantity.text = cart[indexPath.row].quantity
            cell.lbl_quantity.text = cart[indexPath.row].quantity
        }else{
            cell.lbl_quantity.isHidden = true
            cell.txt_quantity.text = "0"
        }
        
        
        if cart[indexPath.row].discountEndDate != nil {
            
        }
        
        if let discount = cart[indexPath.row].discount,
            Double(discount) ?? 0 > 0{
            cell.lbl_discount.text = "\(discount)% OFF "
            cell.lbl_discount.isHidden = false
        }else{
            cell.lbl_discount.isHidden = true
        }
        
        cell.lbl_product_id.text = cart[indexPath.row].id
        cell.lbl_product_title.text = cart[indexPath.row].title
        cell.lbl_product_description.text = cart[indexPath.row].Description
        
        cell.lbl_product_our_price.text = Currency + " " + (cart[indexPath.row].Ourprice ?? "0")
        
                if let orignalPrice = cart[indexPath.row].orignalPrice ,
            Int(orignalPrice) ?? 0 > 0 {
            cell.view_orignal_price.isHidden = false
            cell.lbl_product_orignal_price.text = Currency + " " + (cart[indexPath.row].orignalPrice!)
        }else{
            cell.view_orignal_price.isHidden = true
        }

        

        
        
        
        return cell
    }

    

    //    MARK: - Table View Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tappedProduct = cart[indexPath.row]
        selected_item_for_Information = indexPath.row
        self.performSegue(withIdentifier: "gotoInfoVc", sender: self)
        AudioServicesPlaySystemSound(1519)
        self.tbl_products.scrollToRow(at: indexPath, at: .none , animated: true)

//        changeRowHeight(at: indexPath)
//        tbl_products.scrollToRow(at: indexPath, at: .none , animated: true)
//        updateRow()


    }
    
    
    
//   //    MARK: - Change Row Height
//     func changeRowHeight(at indexPath:IndexPath){
//         AudioServicesPlaySystemSound(1519)
//             ProductId = cart[indexPath.row].id!
//
//
//         if selectedIndex == indexPath{
//             rowHeight = 200
//             tbl_products.beginUpdates()
////             tbl_products.reloadRows(at: [selectedIndex!], with: .none)
//             tbl_products.endUpdates()
//             selectedIndex = nil
//             rowHeight = 267
//             return
//         }
//         selectedIndex = indexPath
//         tbl_products.beginUpdates()
////         tbl_products.reloadRows(at: [selectedIndex!], with: .none)
//         tbl_products.endUpdates()
//
//
//     }
//    MARK: - Remove Cart
    @IBAction func remove_cart(_ sender: Any) {
                let Appearance = SCLAlertView.SCLAppearance()
                let alert = SCLAlertView(appearance: Appearance)
                alert.addButton("YES") {
                    cart = []
                    for i in 0..<AppDelegate.sharedInstance.appCart.count{
                        AppDelegate.sharedInstance.appCart[i].quantity = "0"
                    }
                    self.navigationController?.popViewController(animated: true)
        }

        alert.showCustom(AppName,subTitle: "Are you sure you want to remove cart", color: .gray,closeButtonTitle: "NO",circleIconImage: .remove)
    }

    //    MARK: - Initiate Cart
     func InitiateCart(){
           cart = []
           for i in 0..<AppDelegate.sharedInstance.appCart.count{
               if Int(AppDelegate.sharedInstance.appCart[i].quantity)! > 0 {
                   cart.append(AppDelegate.sharedInstance.appCart[i])
               }
           }
        tbl_products.reloadData()
       }
    
    //    MARK:- Button Check Out
    @IBAction func btn_check_out(_ sender: Any) {
        let priceNproducts = AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: nil)
//        let user_info = UserDefaults.standard.value(forKey: "user_information") as! [String:AnyObject]
        let price = priceNproducts["total_price"] as! String
//        let total_items = priceNproducts["total_items"] as! String
//        let total_products = priceNproducts["total_products"] as! String
//        let productIds = priceNproducts["productIds"] as! String
//        let productQunatites = priceNproducts["productQunatites"] as! String
//        let DBobject = ["total_price":price,"total_items":total_items,"total_products":total_products,"id":user_info["user_id"] as! String ,"town" : user_info["town"] as! String,"address":user_info["address"] as! String,"productIds":productIds,"productQunatites":productQunatites,"name":user_info["name"] as! String]
//        checkOutAndSave(oderInfo: DBobject)
        
        if let PriceInDouble = Double(price) {
        
            
        CheckMinPrice(totalAmount:PriceInDouble,completion: {(allowCheckOut)
              in
            if allowCheckOut{
                self.performSegue(withIdentifier: "gotoCheckOut", sender: self)
            }
            
        })
    }
    }
    
    
    func CheckMinPrice(totalAmount:Double,completion:@escaping (Bool) -> () ){
        
        if internetConnectionIsOk(){
            let parameter = ["type":"min_price","office_name":office_name] as [String:String]
            HUD.show(.progress)
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: {(response)in
                HUD.hide()
                print(response.result.value)
                switch response.result{
                case .success:
                    let dict = response.result.value as! [String:AnyObject]
                    if dict["message"] as! String == "success"{
                        let minPrice = dict["DATA"]!["min_price"] as! [AnyObject]
                        
                        if (minPrice[0]["minimum_price"] as! NSString).doubleValue > totalAmount {
                            appDelegate.showerror(str: "Minimum Order price is \(Currency)\(minPrice[0]["minimum_price"] as! String)")
                        }
                        else  {
                            DispatchQueue.main.async {
                                completion(true)

                            }
                            
                        }
                        
                    }else{
                        appDelegate.showerror(str: dict["DATA"]?["message"] as? String)
                    }
                    
                case .failure(let error):
                    appDelegate.showerror(str: error.localizedDescription)
                }
                
            })
        }
        
    
    }
    
    
    
    
    //    MARK: - Allow Check Out and save
    func checkOutAndSave(oderInfo:[String:String]){
        if internetConnectionIsOk(){
            let parameter = ["type":"min_price","office_name":office_name] as [String:String]
            HUD.show(.progress)
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: {(response)in
                HUD.hide()
                print(response.result.value)
                switch response.result{
                case .success:
                    let dict = response.result.value as! [String:AnyObject]
                    if dict["message"] as! String == "success"{
                        let minPrice = dict["DATA"]!["min_price"] as! [AnyObject]
                        let ComparablePrice = (oderInfo["total_price"]! as NSString).doubleValue
                        if (minPrice[0]["minimum_price"] as! NSString).doubleValue > ComparablePrice {
                            appDelegate.showerror(str: "Minimum Order price is \(Currency)\(minPrice[0]["minimum_price"] as! String)")
                        }
                        else  {
                            DataBaseHelper.sharedInstance.save(object: oderInfo)
                            DispatchQueue.main.async {
                                self.performSegue(withIdentifier: "gotoCheckOut", sender: self)
                            }
                            
                        }
                        
                    }else{
                        appDelegate.showerror(str: dict["DATA"]?["message"] as? String)
                    }
                    
                case .failure(let error):
                    appDelegate.showerror(str: error.localizedDescription)
                }
                
            })
        }
    }
//    MARK: - information VC delegate
    func InfoVc_ProductQuantity_Updated(quantity: String) {
        cart[selected_item_for_Information].quantity = quantity
    }
    
    //    MARK: - Prepare For Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        CartEnabled = false
        if segue.identifier == "gotoCheckOut"{
            let destination = segue.destination as! CheckOutVc
            destination.PriceNProducts = AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: nil)
            destination.checkOutCart = cart
        }else{
            let destination = segue.destination as! InformationVc
            destination.delegate = self
            destination.productData = AppDelegate.sharedInstance.appCart[Row]
            destination.PriceNProducts = AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: nil)
        guard let navigationController = self.navigationController else { return }
        var navigationArray = navigationController.viewControllers // To get all UIViewController stack as Array
            for i in 0..<navigationArray.count {
                if navigationArray[i].title == "Information"{
                    navigationArray.remove(at: i)
                    self.navigationController?.viewControllers = navigationArray
                    break
                }
            }
        }
    }
    
    
}


extension UITableView{
    func refresh(){
        selectedIndex = nil
        self.beginUpdates()
        self.endUpdates()
    }
}
