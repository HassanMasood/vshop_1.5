//
//  PreviousPurchases.swift
//  Food Service
//
//  Created by Hassan  on 03/04/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import Foundation


class Purchases {
    
    static var CurrentOrder = Purchases()
    
    var callerName = String()
    var deliveryCharges = String()
    var destination = String()
    var orderId = String()
    var notes = String()
    var mobile = String()
    var pickup = String()
    var totalPrice = String()
    var totalItems = String()
    var totalProducts = String()
    var order_date = String()
    var paid = Bool()
    var Productquantities = [String]()
    var Productids = [String]()
    var cart = [ProductData]()
    
    
}

