//
//  ProcductCellData.swift
//  Food Service
//
//  Created by Hassan  on 27/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import Foundation
import UIKit

class ProductData {
    var imageUrl : URL? = nil
    var otherImgUrl : String? = nil
    var discount : String? = nil
    var Ourprice  : String? = nil
    var info : String? = nil
    var Stock : String? = nil
    var title : String? = nil
    var id : String? = nil
    var Description : String? = nil
    var quantity = "0"
    var favorite = "no"
    var discountEndDate : String? = nil
    var orignalPrice : String? = nil
    var sold : String? = nil
    var rating : String? = nil
    var details : String? = nil
    
    
}
