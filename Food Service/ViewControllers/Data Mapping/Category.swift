//
//  Category.swift
//  Food Service
//
//  Created by Hassan  on 25/05/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import Foundation

class Category {
     var id = ""
    var name = ""
    var img_url : URL? = nil
}
