//
//  ProductCatagoriesVc.swift
//  Food Service
//
//  Created by Hassan  on 21/04/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import PKHUD
import SCLAlertView
class ProductCatagoriesVc: CommonVc , UITableViewDelegate , UITableViewDataSource ,OrderPad2Delegate {
    
    
    
    

    var arr_categories = [Category](){
        didSet{
//            categoryTbl.reloadData()
        }
    }
    
    var categoryID : String = "0"
    
    
    @IBOutlet weak var categoryTbl: UITableView!
    @IBOutlet weak var lbl_itemsCost : UILabel!
    @IBOutlet weak var lbl_minPrice : UILabel!
    
    @IBOutlet weak var btn_previousCategories: UIButton!
    
    var isParentCategory = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if arr_categories.isEmpty{
        fetchProductCatagory(category: categoryID)
//        }
        lbl_minPrice.text = minOrderPRICE
        addCustomizedBackBtn()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: lbl_itemsCost)
        Update_lbl_badge()
    }
//    MARK: - Table View Data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return  arr_categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell") as! CategoryCell
        cell.lbl_name.text = arr_categories[indexPath.row].name
        if arr_categories[indexPath.row].img_url != nil{
            cell.img_category.af_setImage(withURL: arr_categories[indexPath.row].img_url!,placeholderImage:UIImage(named: "loading"))
        }else{
            cell.img_category.image = nil
        }
        return cell
    }
    
//    MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isParentCategory{
            fetchProductCatagory(category: arr_categories[indexPath.row].id)
        }else{
            self.performSegue(withIdentifier: "gotoOrderpad2", sender: arr_categories[indexPath.row])
        }
    }
    
// MARK: - Button Side Menu
    @IBAction func sideMenu(_ sender: Any) {
        
            if let drawer = self.drawer() ,
                let manager = drawer.getManager(direction: .left){
                let value = !manager.isShow
                drawer.showLeftSlider(isShow: value)
            }
        
    }
    
//    MARK: -
    func orderPad2Dismissed() {
        AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: lbl_itemsCost)
        Update_lbl_badge()
    }
//    MARK: - Fetch Producd Catagories
    func fetchProductCatagory(category: String? = "0"){
        
        if internetConnectionIsOk(){
            let paramter = ["type":"category_list","office_name":office_name, "parent_category" : category!] as [String:String]
            HUD.show(.progress)
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: paramter).responseJSON(completionHandler: {(response)
                in
                HUD.hide()
                print(response.result.value)
                print(response.request?.url)
                switch response.result{
                case .success:
                    
                    let dict = response.result.value as! [String:AnyObject]
                    let data = dict["DATA"] as! [String:AnyObject]
                    if dict["message"] as! String == "Available Category"  {
                        self.arr_categories = []
                        let categories = data["categories"] as! [[String:AnyObject]]
                        for i in 0..<categories.count{
                            let c = Category()
                            c.id = categories[i]["category_id"] as? String ?? ""
                            c.name = categories[i]["category_name"] as? String ?? ""
                            if var img_url = categories[i]["category_image"] as? String {
                                img_url = img_url.replaceURL()
                                c.img_url = URL(string: img_url)
                            }
                            
                            self.arr_categories.append(c)
                        }
                        
                        self.isParentCategory = self.check_Category_type(Category: category!)
                        
                        if self.arr_categories.isEmpty == true {
                            self.categoryTbl.isHidden = true
                        }else{
                            
                            self.categoryTbl.isHidden = false
                        }
                        self.categoryTbl.reloadData()
                    }else{
                        AppDelegate.sharedInstance.showPopUP(message: "Sorry! Products not available yet." , sender: self)
                        self.fetchProductCatagory(category: "0")
                    }
                    
                case .failure(let error):
                    
                    appDelegate.showerror(str: error.localizedDescription)
                    
                }
            })
        }
    }
//    MARK: - Button Previous Categories pressed
    @IBAction func previousCategories_btn_pressed(){
        fetchProductCatagory()
        if isParentCategory{
            
        }else{
            
            
        }

    }
    
    func check_Category_type(Category:String)->Bool{
///  parent category = 0 and sub Category can have any id
         
        if Category == "0"{
            btn_previousCategories.isHidden = true
            return true
        }else{
            btn_previousCategories.isHidden = false
            return false
        }
        
        
    }
    

    func showPopUP(message:String){
        let alert = UIAlertController(title: office_name, message: message, preferredStyle: .alert)
        self.present(alert, animated: true) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let nav = segue.destination as? UINavigationController,
//            let destination = nav.topViewController as? OrderPad2Vc{
//            destination.sender = self
//            destination.category = sender as! Category
//            destination.delegate = self
//
//        }
        if let destination = segue.destination as? OrderPad2Vc{
            destination.sender = self
            destination.category = sender as! Category
            destination.delegate = self
            
        }
    }

}
