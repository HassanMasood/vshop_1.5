//
//  MyOrdersVc.swift
//  Food Service
//
//  Created by Hassan  on 11/03/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import SCLAlertView
import PKHUD
import CoreData
import MMDrawController



class MyOrdersVc: CommonVc,UITableViewDelegate,UITableViewDataSource,PurchasescellDelegate,PendingCellDelegate,CartVCDelegate {

    
        
    
    
    
    
    @IBOutlet weak var lbl_order_not_available: UILabel!
    @IBOutlet weak var seg_orders: UISegmentedControl!
    @IBOutlet weak var tlb_orders: UITableView!
    var arr_pendingOrder = [Order]()
    var arr_purchases = [Purchases]()
    
//    Segue data to CartVc
//    var productIDs = [String]()
//    var productQuantites = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if AppDelegate.sharedInstance.userLoggedIn{/
            fetchPendingOrders()
//        }else{
//            loginResquest(VC: self , nextViewController: nil)
//        }
        
    }
    
    //    MARK: - Table View Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if seg_orders.selectedSegmentIndex == 0 {
//            return arr_pendingOrder.count
//        }else if seg_orders.selectedSegmentIndex == 1 {
            return arr_purchases.count
//        }else if seg_orders.selectedSegmentIndex == 2 {
//            return arr_purchases.count
//        }
//        return 0
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        /// ----- cell for pending orders from local DB --------
        
        if seg_orders.selectedSegmentIndex == 0 || seg_orders.selectedSegmentIndex == 1  {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as! PendingOrdersCell
            cell.delegate = self
            cell.tag = indexPath.row
          
            cell.lbl_ID.text = arr_purchases[indexPath.row].orderId
            cell.lbl_name.text = arr_purchases[indexPath.row].callerName
            cell.lbl_address.text = "Drop Off : " + arr_purchases[indexPath.row].destination
            cell.lbl_order_date.text = arr_purchases[indexPath.row].order_date
            cell.lbl_mobile.text = arr_purchases[indexPath.row].mobile
            cell.lbl_total_price.text = Currency + " " + arr_purchases[indexPath.row].totalPrice
            cell.lbl_pickup_address.text = "Pick Up : " + arr_purchases[indexPath.row].pickup
            
            if arr_purchases[indexPath.row].paid {
                cell.btn_payNow.isHidden = true
            }else{
                cell.btn_payNow.isHidden = false
            }
            
            if seg_orders.selectedSegmentIndex == 0{
                cell.btn_delete.isHidden = false
            }else {
                cell.btn_delete.isHidden = true
            }
            
            cell.layoutIfNeeded()
            return cell
        }else{
        
        
        /// ----- cell for all kind of orders fetched from WS -----
        
            let cell  = tableView.dequeueReusableCell(withIdentifier: "previousPurchasesCell") as! PurchasesCell
            cell.delegate = self
            cell.lbl_orderId.text = arr_purchases[indexPath.row].orderId
            cell.lbl_callerName.text = arr_purchases[indexPath.row].callerName
            cell.lbl_mobile.text = arr_purchases[indexPath.row].mobile
            cell.lbl_notes.text = arr_purchases[indexPath.row].notes
            cell.lbl_notes.flashScrollIndicators()
//            if seg_orders.selectedSegmentIndex == 2{
//                cell.stack_buttons.isHidden = true
//            }else {
//                cell.stack_buttons.isHidden = false
//            }
            cell.lbl_totalItems.text = arr_purchases[indexPath.row].totalItems
            cell.lbl_totalPrice.text = Currency + " " + arr_purchases[indexPath.row].totalPrice 
            cell.lbl_totalProducts.text = arr_purchases[indexPath.row].totalProducts
            
            cell.lbl_destination.text = arr_purchases[indexPath.row].destination
            
            cell.layoutIfNeeded()
            return cell
            
        }
    }
    //  MARK: - tableView delegates
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if seg_orders.selectedSegmentIndex == 0 {
//            return 264
//        }else {
//            return 345
//        }
//    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
            case .delete:
                tableView.beginUpdates()
                tableView.deleteRows(at:[indexPath], with: .right)
                tableView.endUpdates()
            default:
                break
            }
        }
    
    
    //    MARK: - Fetch pending orders
    func fetchPendingOrders(){
        
///     ------ fetch pending orders from local DB ------
        
//        let startDate = Date()
//        let endDate = Calendar.current.date(byAdding: .hour, value: -24, to: startDate)!
//        let user_info = UserDefaults.standard.value(forKey: "user_information") as! [String:AnyObject]
//        let predicate = NSPredicate(format: "(orderDate >= %@) AND (orderDate <= %@) AND (userId == %@) ", endDate as NSDate, startDate as NSDate,user_info["user_id"] as! String)
//        arr_pendingOrder = DataBaseHelper.sharedInstance.fetch(Predicate: predicate)
//        arr_pendingOrder = arr_pendingOrder.reversed()
//
//        tlb_orders.reloadData()
        
        
        /// ----- fetch order from WS -----
        
        arr_purchases = []
        if internetConnectionIsOk(){
            
            let user_info = UserDefaults.standard.value(forKey: "user_information") as! [String:AnyObject]
            
            let parameter = ["office_name" : office_name , "type" : "cust_order_list" , "customer_id":user_info["user_id"] as! String , "status" : "pending" ] as [String:String]
            HUD.show(.progress)
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: {(response) in
                HUD.hide()
                print(response.result.value)
                print(response.request?.url)
                switch response.result{
                case .success:
                    let dict = response.result.value as! [String:AnyObject]
                    if dict["message"] as! String == "Available Orders"{
                        let P_orders = dict["DATA"]?["order"] as! [[String:AnyObject]]
                        for i in 0..<P_orders.count{
                            let p_purchases = Purchases()
                            p_purchases.callerName = P_orders[i]["caller"] as? String ?? ""
                            p_purchases.pickup = P_orders[i]["pickup"] as? String ?? ""
                            p_purchases.deliveryCharges = P_orders[i]["delivery_charges"] as? String ?? ""
                            p_purchases.notes = P_orders[i]["notes"] as? String ?? ""
                            p_purchases.destination = P_orders[i]["destination"] as? String ?? ""
                            p_purchases.orderId = P_orders[i]["job_id"] as? String ?? ""
                            p_purchases.mobile = P_orders[i]["mobile"] as! String
                            p_purchases.totalPrice = P_orders[i]["total_price"] as? String ?? ""
                            p_purchases.order_date = P_orders[i]["job_date"] as? String ?? ""
                            p_purchases.pickup = P_orders[i]["pickup"] as? String ?? ""
                            if P_orders[i]["paid"] as! String != "no" {
                                p_purchases.paid = true
                            }
                            
//                               -----total products and items-----
                            if let products = P_orders[i]["Order_item"] as? [[String:AnyObject]]{
                            var totalItems = 0
                            var productIds = [String]()
                            var quantities = [String]()
                            for i in 0..<products.count{
//                                let pd = products[i][]
                                let PD = ProductData()
                                PD.id = products[i]["product_id"] as? String
                                PD.info = products[i]["product_info"] as? String
                                PD.info = PD.info?.withoutHtml
                                PD.Description = products[i]["product_description"] as? String
                                PD.Description = PD.Description?.withoutHtml
                                PD.Stock = products[i]["product_stock"] as? String
                                PD.title = products[i]["product_title"] as? String
                                PD.favorite = products[i]["favorite"] as? String ?? "no"
                                let imgUrl = (products[i]["product_image"] as? String ?? "").replaceURL()
                                PD.imageUrl = URL(string:imgUrl)
                                PD.orignalPrice = products[i]["p_market_cutoff_price"] as? String
                                PD.discount = products[i]["p_discount_perc"] as? String
                                PD.Ourprice = products[i]["product_price"] as? String
                                PD.otherImgUrl = products[i]["other_img"] as? String
                                
                                if let rating = products[i]["product_rating"] as? Int{
                                    PD.rating = String(rating)
                                }
                                
                                if let int_sold = products[i]["product_sold"] as? Int{
                                    PD.sold = String(int_sold)
                                }
                                PD.quantity = products[i]["product_quantity"] as? String ?? "0"
                                p_purchases.cart.append(PD)
                                
                                totalItems += ((products[i]["product_quantity"])?.intValue)!
                                productIds.append(products[i]["product_id"] as! String )
                                quantities.append(products[i]["product_quantity"] as! String )
                            }
                            p_purchases.Productquantities = quantities
                            p_purchases.Productids = productIds
                            p_purchases.totalItems = String(totalItems)
                            p_purchases.totalProducts = String(products.count)
                            }
                            self.arr_purchases.append(p_purchases)
                        }
                        self.tlb_orders.reloadData()
                        
                    }else if  dict["message"] as! String == "Any Products not available" {
                        self.tlb_orders.reloadData()
                    }else{
                        SCLAlertView().showError(dict["message"] as! String)
                        self.tlb_orders.reloadData()
                    }
                case .failure(let error):
                    self.tlb_orders.reloadData()
                    SCLAlertView().showError(error.localizedDescription)
                }
            })
            
            
        }
        
        
        
    }
    //    MARK: - Side Menu
    
    @IBAction func sideMenu(_ sender: Any) {
        if let drawer = self.drawer() ,
            let manager = drawer.getManager(direction: .left){
            let value = !manager.isShow
            drawer.showLeftSlider(isShow: value)
        }
    }
    
    // MARK: - Fetch Previous Orders
    func fetchPreviousPurchases(){
        arr_purchases = []
        if internetConnectionIsOk(){
            
            let user_info = UserDefaults.standard.value(forKey: "user_information") as! [String:AnyObject]
            
            let parameter = ["office_name" : office_name , "type" : "cust_order_list" , "customer_id":user_info["user_id"] as! String , "status" : "confirmed" ] as [String:String]
            HUD.show(.progress)
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: {(response) in
                HUD.hide()
                print(response.result.value)
                print(response.request?.url)
                switch response.result{
                case .success:
                    let dict = response.result.value as! [String:AnyObject]
                    if dict["message"] as! String == "Available Orders"{
                        
                        
                        let P_orders = dict["DATA"]?["order"] as! [[String:AnyObject]]
                        for i in 0..<P_orders.count{
                            let p_purchases = Purchases()
                            p_purchases.callerName = P_orders[i]["caller"] as! String
                            p_purchases.pickup = P_orders[i]["pickup"] as! String
                            p_purchases.deliveryCharges = P_orders[i]["delivery_charges"] as! String
                            p_purchases.notes = P_orders[i]["notes"] as! String
                            p_purchases.destination = P_orders[i]["destination"] as! String
                            p_purchases.orderId = P_orders[i]["job_id"] as! String
                            p_purchases.mobile = P_orders[i]["mobile"] as! String
                            p_purchases.totalPrice = P_orders[i]["total_price"] as! String
                            p_purchases.order_date = P_orders[i]["job_date"] as! String
                            
                            if P_orders[i]["paid"] as! String != "no" {
                                p_purchases.paid = true
                            }
                            
                            
                            //                                 total products and items
                            if let products = P_orders[i]["Order_item"] as? [[String:AnyObject]]{
                            var totalItems = 0
                            var productIds = [String]()
                            var quantities = [String]()
                            for i in 0..<products.count{
                                totalItems += ((products[i]["product_quantity"])?.intValue)!
                                productIds.append(products[i]["product_id"] as! String )
                                quantities.append(products[i]["product_quantity"] as! String )
                                
                            }
                            p_purchases.Productquantities = quantities
                            p_purchases.Productids = productIds
                            p_purchases.totalItems = String(totalItems)
                            p_purchases.totalProducts = String(products.count)
                            }
                            self.arr_purchases.append(p_purchases)
                        }
                        self.tlb_orders.reloadData()
                        
                    }else if  dict["message"] as! String == "Any Products not available" {
                        self.tlb_orders.reloadData()
                    }else{
                        SCLAlertView().showError(dict["message"] as! String)
                        self.tlb_orders.reloadData()
                    }
                case .failure(let error):
                    self.tlb_orders.reloadData()
                    SCLAlertView().showError(error.localizedDescription)
                }
            })
            
            
        }
        
    }
    
    //    MARK: - Fetch Complete Orders
    func fetchCompletedOrders(){
        arr_purchases = []
        if internetConnectionIsOk(){
            
            let user_info = UserDefaults.standard.value(forKey: "user_information") as! [String:AnyObject]
//            let parameter = ["type":"completed_orders","office_name":office_name,"customer_id":user_info["user_id"] as! String]
            
            let parameter = ["type":"cust_order_list","office_name":office_name,"customer_id":user_info["user_id"] as! String , "status" : "completed"   ] as [String:String]

            HUD.show(.progress)
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: {(response) in
                HUD.hide()
                print(response.result.value)
                print(response.request?.url)
                switch response.result{
                case .success:
                    let dict = response.result.value as! [String:AnyObject]
                    if dict["message"] as! String == "Available Orders"{
                        let P_orders = dict["DATA"]?["orders"] as! [[String:AnyObject]]
                        print(P_orders)
                        for i in 0..<P_orders.count{
                            let p_purchases = Purchases()
                            p_purchases.callerName = P_orders[i]["caller"] as! String
                            p_purchases.pickup = P_orders[i]["pickup"] as! String
                            p_purchases.deliveryCharges = P_orders[i]["delivery_charges"] as! String
                            p_purchases.notes = P_orders[i]["notes"] as! String
                            p_purchases.destination = P_orders[i]["destination"] as! String
                            p_purchases.orderId = P_orders[i]["job_id"] as! String
                            p_purchases.mobile = P_orders[i]["mobile"] as! String
                            p_purchases.totalPrice = P_orders[i]["total_price"] as! String
                            
                            //                                 total products and items
                            let products = P_orders[i]["Order_item"] as! [[String:AnyObject]]
                            var totalItems = 0
                            for i in 0..<products.count{
                                totalItems += ((products[i]["product_quantity"])?.intValue)!
                            }
                            p_purchases.totalItems = String(totalItems)
                            p_purchases.totalProducts = String(products.count)
                            self.arr_purchases.append(p_purchases)
                        }
                        self.tlb_orders.reloadData()
                    }else if  dict["message"] as! String == "Any Products not available"{
                        self.tlb_orders.reloadData()
                    }else{
                        SCLAlertView().showError(dict["message"] as! String)
                        self.tlb_orders.reloadData()
                    }
                case .failure(let error):
                    SCLAlertView().showError(error.localizedDescription)
                    self.tlb_orders.reloadData()
                }
            })
        }
        self.tlb_orders.reloadData()
    }
    
    //    MARK: - Segment Orders
    
    @IBAction func Orders(_ sender: Any) {
        if AppDelegate.sharedInstance.userLoggedIn{
        if seg_orders.selectedSegmentIndex == 0 {
            fetchPendingOrders()
            
                
            }else if seg_orders.selectedSegmentIndex == 1{
            fetchPreviousPurchases()
            
            
            
        }else if seg_orders.selectedSegmentIndex == 2 {
            fetchCompletedOrders()
            
            
        }
        }
    }
    //    MARK: - Purchases cell Delegate
    func deleteButtonPressed(orderId: String , cell : PurchasesCell) {
//        if internetConnectionIsOk(){
//            HUD.show(.progress)
//            let parameter = ["type":"delete_order","office_name":office_name,"order_id":orderId]
//            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters:parameter).responseJSON(completionHandler: {(response)
//                in
//                HUD.hide()
//                //                        print(response.request?.url)
//                //                        print(response.result.value)
//                switch response.result{
//                case .success:
//                    let dict = response.result.value as! [String:AnyObject]
//                    if dict["message"]!["msg"] as! String == "Order deleted successfully" {
//                        guard let indexPath = self.tlb_orders.indexPath(for: cell)
//                                    else{
//                                        self.tlb_orders.reloadData()
//                                   return
//                               }
//                        self.arr_purchases.remove(at: indexPath.row)
//                        self.tableView(self.tlb_orders, commit: .delete, forRowAt: indexPath)
//                    }else {
//                        SCLAlertView().showError(dict["message"]!["msg"] as! String)
//                    }
//                case .failure(let error):
//                    SCLAlertView().showError(error.localizedDescription)
//                }
//            })
//        }
    }
    //    MARK: -  Pending cell Delegate
    
    func cancelButtonPressedPendingOrderCell(cell:PendingOrdersCell) {
        //        DataBaseHelper.sharedInstance.delete(objectID: objectID)
        if let indexPath = tlb_orders.indexPath(for: cell){
            //             else{
            //            tlb_orders.reloadData()
            //            return
            //        }
            
            
            if internetConnectionIsOk(){
                HUD.show(.progress)
                let parameter = ["type":"delete_order","office_name":office_name,"order_id":arr_purchases[indexPath.row].orderId] as [String:String]
                APIManager.sharedInstance.sessionManager.request(BaseURL,parameters:parameter).responseJSON(completionHandler: {(response)
                    in
                    HUD.hide()
//                                            print(response.request?.url)
//                                            print(response.result.value)
                    switch response.result{
                    case .success:
                        let dict = response.result.value as! [String:AnyObject]
                        if dict["message"]!["msg"] as! String == "Order deleted successfully" {
                            
                            self.arr_purchases.remove(at: indexPath.row)
                            self.tableView(self.tlb_orders, commit: .delete, forRowAt: indexPath)
                            
                            
                            
                            
                        }else {
                            SCLAlertView().showError(dict["message"]!["msg"] as! String)
                        }
                    case .failure(let error):
                        SCLAlertView().showError(error.localizedDescription)
                    }
                })
            }
        } else {
            appDelegate.showerror(str: "Unknown error!")
        }
        
        //        guard let indexPath = tlb_orders.indexPath(for: cell)
        //             else{
        //            tlb_orders.reloadData()
        //            return
        //        }
        
        
        
        
        
    }
// MARK: Button Change Order Pressed
  func EditButtonPressedPendingOrderCell(objectID: NSManagedObjectID, cell: PendingOrdersCell) {
    
    
    guard let indexPath = tlb_orders.indexPath(for: cell)
         else{
        tlb_orders.reloadData()
        return
    }
//    DataBaseHelper.sharedInstance.delete(objectID: objectID)
//    if arr_pendingOrder[indexPath.row].deliveryType == "click_and_collect"{
//    selectedBranch.address = arr_pendingOrder[indexPath.row].address ?? ""
//    deliveryType = "click_and_collect"
//    }
    
    
//    let productIDs = arr_pendingOrder[indexPath.row].productIds?.components(separatedBy: ",") ?? []
//    let productQuantites = arr_pendingOrder[indexPath.row].productQunatities?.components(separatedBy: ",") ?? []
    
/// -----To get products from app cart prodcd ids and Quantites are requried-----
//    let productIDs = arr_purchases[indexPath.row].Productids
//    let productQuantites = arr_purchases[indexPath.row].Productquantities
//    initiateCartForPendingOrder(productIDs: productIDs, productQuantities: productQuantites)
    
    
//    for i in arr_purchases[indexPath.row].
    
//    arr_pendingOrder.remove(at: indexPath.row)
//    tableView(tlb_orders, commit: .delete, forRowAt: indexPath)
    
//    segue performed from side menu vc
//    if let Dra = self.drawer(){
//               Dra.showLeftSlider(isShow: false)
//               Dra.setMainWith(identifier: "gotoOrderPad2Vc")
//           }
//    cart = arr_purchases[indexPath.row].cart
    
    updateAppcart(Cart: arr_purchases[indexPath.row].cart)
//    Purchases.CurrentOrder = arr_purchases[indexPath.row]
    openCart()
//    UpdateCostForMyOrders(cartData: cart)
//    openCartForMyorders(priceNproducts: UpdateCostForMyOrders(cartData: cart))
    
    
//    SCLAlertView().showCustom(AppName, subTitle: "Your order is updated as per availablity of the stock", color: .gray, closeButtonTitle: "Ok", timeout: nil, circleIconImage: .checkmark , animationStyle: .bottomToTop)
    
    }
    
    override func openCart() {
        
        InitiateCart()
        if cart.isEmpty {
            appDelegate.showerror(str: "Cart is Empty")
            return
        }
        
        if AppDelegate.sharedInstance.userLoggedIn{
            let story = UIStoryboard(name: "Main", bundle:nil)
            let CartVc = story.instantiateViewController(withIdentifier: "CartVc") as! CartVc
            self.navigationController?.pushViewController(CartVc, animated: true)
            CartVc.delegate = self
            CartVc.deliveryCharges = Int(Purchases.CurrentOrder.deliveryCharges) ?? 0
            
        }else{
            let story = UIStoryboard(name: "Main", bundle:nil)
            let cartVc = story.instantiateViewController(withIdentifier: "CartVc") as! CartVc
            loginResquest(VC: self , nextViewController: cartVc)
            
        }
        }
        
    //    MARK:- Cart VC Delegate
    func CartVCBackButtonPressed() {
        Purchases.CurrentOrder = Purchases()
        cart = []
        for i in AppDelegate.sharedInstance.appCart{
            i.quantity = "0"
        }
    }
    
    
    func paynowButtonPressedPendingOrderCell(cell: PendingOrdersCell) {
        
        guard let indexPath = tlb_orders.indexPath(for: cell)
             else{
            tlb_orders.reloadData()
            return
        }
        
        let productIDs = arr_purchases[indexPath.row].Productids
        let productQuantites = arr_purchases[indexPath.row].Productquantities
        initiateCartForPendingOrder(productIDs: productIDs, productQuantities: productQuantites)
        
        let story = UIStoryboard(name: "Main", bundle:nil)
        let checkOutViewController = story.instantiateViewController(withIdentifier: "checkOutVc") as! CheckOutVc
        
       checkOutViewController.PriceNProducts = AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: nil)
        checkOutViewController.checkOutCart = cart
        self.navigationController?.pushViewController(checkOutViewController, animated: true)
        
        
        
    }

    func UpdateCostForMyOrders(cartData:[ProductData])->[String:Any] {
                var noOfItems = 0
                var noOfproducts = 0
                var totalPrice = 0.00
                
        //       to save product ids and qunatity in a string to save in databse (comma seprated)
                var pIds = ","
                var pQunatities = ","
                
                for i in 0..<cart.count{
                    
                    if Int(cartData[i].quantity)! > 0 {
                        
                        noOfItems += Int(cartData[i].quantity)!
                        pIds += cartData[i].id! + ","
                        pQunatities += cartData[i].quantity + ","
                        let quantity = Double(cartData[i].quantity )!
                        var discount : Double = 0.00
                        
                        if let d = Double(cartData[i].discount ?? "0.00"),
                        d > 0.00 {
                            discount = d
                        }
    //                    print(discount)
    //                    totalPrice += (Double(appCart[i].price ?? "0.00")! - discount ) * quantity
    //                    print(totalPrice)
                        totalPrice += ( Double(cartData[i].Ourprice ?? "0.00")! - ( (Double(cartData[i].Ourprice ?? "0.00")! * discount ) / 100.0 ) ) * quantity
                        
                        noOfproducts += 1
                    }
                    
                }
    //            print(totalPrice)
                
              totalPrice = totalPrice.rounded(toPlaces : 2)
    //            print(totalPrice)
             var productIds = pIds.dropLast()
                var productQunatities = pQunatities.dropLast()
                 productIds = productIds.dropFirst()
                productQunatities = productQunatities.dropFirst()
                
                
                return ["total_price":String(totalPrice),"total_products":String(noOfproducts),"total_items":String(noOfItems),"productIds":String(productIds),"productQunatites":String(productQunatities)]
            }
    
    
    func updateAppcart(Cart:[ProductData]){
        HUD.show(.progress)
        
        ///       delete Cart
        for i in AppDelegate.sharedInstance.appCart{
            i.quantity = "0"
        }
        
        
        addProductToAppCart(products: Cart)
        cart = []


        for j in Cart{
        for i in AppDelegate.sharedInstance.appCart{
            if i.id == j.id{

                i.quantity = j.quantity

                if j.title == "parcel delivery" || j.id == "77"{
                    i.quantity = "1";
                    i.Stock = "1";
                    
                    j.quantity = "1";
                    j.Stock = "1";
                    
                    /// pickUp address      pickup phoneNumber      sender firstName sender lastname        sender Mobile      senderEmail               deliveryAddress               UserID              Vehicle           TotalPrice        JOb_time              JobDate
                    
//                    premiumDelivery.sharedInstance.pickup_street_address = Purchases.CurrentOrder.pickup
//                    premiumDelivery.sharedInstance.pickup_phone = Purchases.CurrentOrder.
//                    premiumDelivery.sharedInstance.destination = Purchases.CurrentOrder.destination
                    
                    
                }

                if Int(i.Stock ?? "0") ?? 0 <= Int(j.quantity) ?? 0{
                    i.Stock = j.quantity
                }

                
            }
        }
            
        }
        HUD.hide()
    }
    
//    MARK: - Check product availbility in appCart
    func addProductToAppCart(products:[ProductData]){
        

        for j in products{
            var productFound = false
            for i in AppDelegate.sharedInstance.appCart {
            if i.id == j.id{
                productFound = true
            }
            }
            if !productFound{
                AppDelegate.sharedInstance.appCart.append(j)
            }

        }

    }
    
    // MARK: - Initiate Cart with Product IDs
    func initiateCartForPendingOrder(productIDs : [String]? = nil ,productQuantities : [String]? = nil){
         if productIDs != nil {
             for i in AppDelegate.sharedInstance.appCart{
                        i.quantity = "0"
                    }
             cart = []
            for i in 0..<productIDs!.count{
                 for j in 0..<AppDelegate.sharedInstance.appCart.count{
                    if productIDs![i] == AppDelegate.sharedInstance.appCart[j].id{
                        AppDelegate.sharedInstance.appCart[j].quantity = productQuantities![i]
                     cart.append(AppDelegate.sharedInstance.appCart[j])
                     }
                 }
             }
         }
     }
    
    
     
}



