//
//  ResetPasswordVc.swift
//  Food Service
//
//  Created by Hassan  on 21/04/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import SCLAlertView
import PKHUD

class ResetPasswordVc: CommonVc {

    @IBOutlet weak var txt_email : UITextField!
    @IBOutlet weak var txt_old_password : UITextField!
    @IBOutlet weak var txt_new_password : UITextField!
    
    @IBOutlet weak var btn_cancel : UIButton!
    @IBOutlet weak var btn_change : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        addCustomBackButton()
        addCustomizedBackBtn()
        setUI()
        
        // Do any additional setup after loading the view.
    }
    //MARK:- SetUP UI
    func setUI(){
        
        if let dict: [String:String] = UserDefaults.standard.value(forKey: "user_information") as? [String : String]{
            txt_email.text = dict["email"]
        }
        
        setupButtonUI(for: btn_cancel)
        setupButtonUI(for: btn_change)
    }
   
//    MARK: - Button change password Pressed
    @IBAction func changePassword(_ sender: Any) {
        ChangePassword(Email: txt_email.text!, NewPassword: txt_new_password.text!, OldPassword: txt_old_password.text!)
    }
//    MARK: - Button Cancel pressed
    @IBAction func cancel(_ sender: Any){
//        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
        self.perform(#selector(backButton))
    }
//    MARK: - change Password
    func ChangePassword(Email:String,NewPassword:String,OldPassword:String){
        let check = validation()
        
        if check.isValid{
            
        if internetConnectionIsOk(){
            HUD.show(.progress)
            let parameter = ["office_name":office_name,"type":"reset_password","newpassword":NewPassword,"oldpassword":OldPassword,"email":Email] as [String:String]
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: {(response)
                in
                HUD.hide()
//                print(response.result.value)
                print(response.request?.url)
                switch response.result{
                case .success:
                    let dict = response.result.value as! [String:AnyObject]
                    if dict["DATA"] as! String == "Your password changed successfully"{
                        self.txt_email.text = ""
                        self.txt_new_password.text = ""
                        self.txt_old_password.text = ""
                        SCLAlertView().showSuccess(AppName,subTitle: "Your password changed successfully")
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
//                            self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
                            self.perform(#selector(self.backButton))
                        }

                    }else{
                        if dict["DATA"] as? String == "Email or old password not correct."{
                            appDelegate.showerror(str: "Please recheck email and old password.")
                        }else{
                            SCLAlertView().showError(AppName,subTitle:dict["DATA"] as! String)
                        }
                    }
                case .failure(let error):
                    SCLAlertView().showError(AppName,subTitle:error.localizedDescription)
                }
//
            })
        }
            
        }else{
            SCLAlertView().showError(AppName, subTitle: check.msg)
        }
    }
    
    
    
    //     MARK:- Validations
    func validation ()->(msg:String,isValid:Bool){

        if txt_email.text!.isEmpty{
            return ("Email can not be empty.",false)
        }
        if txt_old_password.text!.isEmpty{
            return ("Old password cannot be empty. ", false)
               }
        if txt_new_password.text!.isEmpty{
            return ("New Password cannot be empty.",false)
        }
        if isValidEmail(emailID: txt_email.text!) == false {
            return ("Please enter valid Email.",false)
        }
        
        return ("valid",true)
    }
    

    
    func isValidEmail(emailID:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailID)
    }
}

