//
//  SideMenuVc.swift
//  Food Service
//
//  Created by Hassan  on 08/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import MMDrawController

class SideMenuVc: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var tableView_Title: UITableView!
    
//    let titels_cell = ["Home","Product Categories","Order Pad","Special Offers","My Favourites","My Orders","Account","Setting","Logout" ]
    
//    let titels_cell = ["Home","Product Categories","Special Offers","My Favourites","My Orders","Account","Setting","Logout" ]
    
    var titels_cell = ["Home","Log-in","Change Password","Product Categories","My Favourites","My Orders","Help","About","Logout" ]
    
//    let imageArr = [#imageLiteral(resourceName: "home"),#imageLiteral(resourceName: "Category"),#imageLiteral(resourceName: "Order"),UIImage(named: "sale"),#imageLiteral(resourceName: "Fav"),#imageLiteral(resourceName: "Doc-1"),#imageLiteral(resourceName: "profile"),#imageLiteral(resourceName: "Setting"),UIImage(named: "logout")]
    
//    let imageArr = [#imageLiteral(resourceName: "home"),#imageLiteral(resourceName: "Category"),UIImage(named: "sale"),#imageLiteral(resourceName: "Fav"),#imageLiteral(resourceName: "Doc-1"),#imageLiteral(resourceName: "profile"),#imageLiteral(resourceName: "Setting"),UIImage(named: "logout")]
    
    var imageArr = [#imageLiteral(resourceName: "home"),#imageLiteral(resourceName: "profile"),UIImage(named: "password-reset"),#imageLiteral(resourceName: "Category"),#imageLiteral(resourceName: "Fav"),#imageLiteral(resourceName: "Doc-1"),#imageLiteral(resourceName: "help"),UIImage(named: "info"),UIImage(named: "logout")]
    
//    let segues = ["gotoHome","gotoProductCatagories","gotoOrderPad","gotoMyFavourites","gotoMyFavourites","gotoMyOrders","gotoProfile","gotoSetting","gotoLogout" ]
    var segues = ["gotoStartVC","gotoProfile","gotoChangePassword","gotoProductCatagories","gotoMyFavourites","gotoMyOrders","","gotoAbout","gotoLogout" ]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView_Title.delegate = self
        tableView_Title.dataSource = self
//        UpdateSegues()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UpdateSegues()
    }
    
    func UpdateSegues(){
        if AppDelegate.sharedInstance.userLoggedIn{
//            titels_cell[1] = "Profile"
            
            titels_cell = ["Home","Profile","Change Password","Product Categories","My Favourites","My Orders","Help","About","Logout" ]
            
            segues = ["gotoStartVC","gotoProfile","gotoChangePassword","gotoProductCatagories","gotoMyFavourites","gotoMyOrders","gotoHelp","gotoAbout","gotoLogout" ]

        }else{
            
//            titels_cell[1] = "Log-in"
            
            titels_cell = ["Home","Log-in","Product Categories","Help","About" ]
            
            segues = ["gotoStartVC","gotoProfile","gotoProductCatagories","gotoHelp","gotoAbout"]

        }
        
        
        
        
        tableView_Title.reloadData()
        
    }
    
    
// MARK: - TableView data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        titels_cell.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") , let lable = cell.viewWithTag(2) as? UILabel , let cellImage = cell.viewWithTag(1) as? UIImageView {
            cellImage.image = imageArr[indexPath.row]
            lable.text = titels_cell[indexPath.row]
            return cell
        }
        
        return UITableViewCell()
        
    }
    
// MARK: - TableView Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if segues[indexPath.row] == "gotoHelp"{
            return
            
        }
        
        favouriteIsEnabled = false
        specialOffersIsEnabled = false
        deliveryProductisEnabled = false
        
        var isSegueAllowed = true
        
        if segues[indexPath.row] == "gotoAbout"{
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            alert(Title: AppName, Message:"Version" + appVersion!, buttonTittle: "Done")
            return
        }
        
        if titels_cell[indexPath.row] == "My Favourites" {
            
            favouriteIsEnabled = true
            
        }else if titels_cell[indexPath.row] == "Special Offers"{
            
            specialOffersIsEnabled = true
            
        }else if segues[indexPath.row] == "gotoLogout" {
            //            self.dismiss(animated: true, completion: nil)
        }
        
        var segue = segues[indexPath.row]
        if segue == "gotoOrderPad"{
            if AppDelegate.sharedInstance.deliveryStatus == "0"{
                segue = "gotoOrderPad2Vc"
            }
            
        }else if segue == "gotoLogout"{
            
            UserDefaults.standard.set([],forKey:"user_information")
            AppDelegate.sharedInstance.userLoggedIn = false
            segue = "gotoStartVC"
            
        }
        
        if segue == "gotoProfile" || segue == "gotoMyOrders" {
            if !AppDelegate.sharedInstance.userLoggedIn{
                if let Dra = self.drawer(){
                    Dra.showLeftSlider(isShow: false)
                    let navCon = Dra.main as! UINavigationController
                    DispatchQueue.main.async {
                        
                        var nextVc : UIViewController? = nil
                        
                        if segue == "gotoProfile"{
                            let story = UIStoryboard(name: "Main", bundle:nil)
                            nextVc = story.instantiateViewController(withIdentifier: "AccountVC") as! AccountVc
                        }else{
                            let story = UIStoryboard(name: "Main", bundle:nil)
                            nextVc = story.instantiateViewController(withIdentifier: "MyOrdersVC") as! MyOrdersVc
                            
                        }
                        loginResquest(VC: navCon.topViewController!, nextViewController: nextVc!)
                    }
                }
                isSegueAllowed = false
            }
        }
        
        if isSegueAllowed{
            if let Dra = self.drawer(){
                let nav = Dra.main as? UINavigationController
                let vcs = nav!.viewControllers
                MMdrawerPreviousVC.sharedInstace.vc = (nav?.topViewController)!
                Dra.showLeftSlider(isShow: false)
                Dra.setMainWith(identifier: segue)
//                Dra.set
                
            }
        }
        UpdateSegues()
    }
    
    
    //  MARK: - Alert
    func alert(Title:String?, Message:String?,buttonTittle:String?){
        
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: .alert)
        let action = UIAlertAction(title: buttonTittle, style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action)
        self.present(alert , animated: true , completion: nil)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let destination = segue.destination as! OrderPad2Vc
////        destination.fav = true
//    }

    

}


