//
//  CheckOutVc.swift
//  Food Service
//
//  Created by Hassan  on 15/03/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import PKHUD
import SCLAlertView
import MMDrawController

protocol CheckOutVCDelegate : class{
    func myOrderButtonPressed()
}

class CheckOutVc: CommonVC2,UITableViewDelegate,UITableViewDataSource,DeliverySelectionDeledate {
    
    
    
    
    
    @IBOutlet weak var lbl_order_time: UILabel!
    @IBOutlet weak var btn_voucher: UIButton!
    @IBOutlet weak var btn_card: UIButton!
    @IBOutlet weak var lbl_min_price: UILabel!
    @IBOutlet weak var lbl_itemsCost: UILabel!
    @IBOutlet weak var btn_edit: UIButton!
    @IBOutlet weak var txt_view_destination: UITextView!
    @IBOutlet weak var lbl_total: UILabel!
    @IBOutlet weak var lbl_total_after_discount: UILabel!
    @IBOutlet weak var tbl_checkOut: UITableView!
    
    @IBOutlet weak var lbl_PremiumDelivery_pickup_address: UILabel!
    @IBOutlet weak var lbl_PremiumDelivery_delivery_address: UILabel!
    @IBOutlet weak var lbl_PremiumDelivery_mobile: UILabel!
    @IBOutlet weak var lbl_PremiumDelivery_vehicle: UILabel!
    @IBOutlet weak var lbl_PremiumDelivery_img: UIImageView!
    
    @IBOutlet weak var View_PremiumDelivery: UIView!
    
    weak var delegate : CheckOutVCDelegate?
    
    var checkOutCart = [ProductData]()
    var PriceNProducts = [String:Any]()
    var destination = String()
    var isPremiumDelivery = false
    var PaymentType = ""{
        didSet{
            if PaymentType == "Card"{
                if isPremiumDelivery{
                    createOrderPremiumDelivery()
                }else{
                    createOrder(paymentType: PaymentType )
                }
            }else if PaymentType == "Cash"{
                if isPremiumDelivery{
                    createOrderPremiumDelivery()
                }else{
                    createOrder(paymentType: PaymentType)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isPremiumDelivery{
            
//            btn_card.isHidden = true
            
        }
        setUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isPremiumDelivery{
            
            fetchDateTime()
         
        }
        addCustomBackButton()
    }
    
//    MARK: - Setup Premium Delivery UI
    func setupPremiumDeliveryUI(){
        
        let date = getSelectedDateTime()
        
        var timeSlot = ""
        if date.selectedStartTime != ""{
            timeSlot = " / \(date.selectedStartTime) - \(date.selectedEndTime)"
        }else{
            let C_date = Date()
              let df = DateFormatter()
              df.dateFormat = "HH:mm"
              let dateString = df.string(from: C_date)
            print(dateString)
        }
        
        premiumDelivery.sharedInstance.job_date = date.selectedDate
        premiumDelivery.sharedInstance.job_time = timeSlot
        
        let userInfo = UserDefaults.standard.value(forKey: "user_information") as! [String:AnyObject]
        lbl_order_time.text = "User-ID: \(userInfo["user_id"] as! String)\nDate/Time: \(date.selectedDate)\(timeSlot)"
        
        lbl_itemsCost.text = "Premium Delivery Cost: " + premiumDelivery.sharedInstance.total_price
        
//        txt_view_destination.text = "Pick Point: \(premiumDelivery.sharedInstance.pickup_point_name) , \(premiumDelivery.sharedInstance.pickup_street_address)\nPickup Mobile: \(premiumDelivery.sharedInstance.pickup_phone)\nPickup Email: \(premiumDelivery.sharedInstance.pickup_email)"
        
        lbl_PremiumDelivery_img.sd_setImage(with: checkOutCart[0].imageUrl , completed: nil)
        
        lbl_PremiumDelivery_pickup_address.text = premiumDelivery.sharedInstance.pickup_street_address
        lbl_PremiumDelivery_delivery_address.text = premiumDelivery.sharedInstance.destination
        
        lbl_PremiumDelivery_vehicle.text = premiumDelivery.sharedInstance.vehicle.type
        lbl_PremiumDelivery_mobile.text = premiumDelivery.sharedInstance.pickup_phone
        
        lbl_total.text = "\(Currency) \(premiumDelivery.sharedInstance.total_price)"
//        lbl_total.font = UIFont(name: lbl_total.font.fontName, size: 20)
        
//        self.txt_view_destination.flashScrollIndicators()
        txt_view_destination.isHidden = true
        View_PremiumDelivery.isHidden = false
        if premiumDelivery.sharedInstance.pickup_phone == "" || premiumDelivery.sharedInstance.pickup_phone.isEmpty{
            View_PremiumDelivery.isHidden = true
        }
    }
    // MARK: -   Setup UI
    func setUp(){
        
        lbl_min_price.text = minOrderPRICE

        setupButtonUI(for: btn_card)
        setupButtonUI(for: btn_voucher)
        setupButtonUI(for: btn_edit)
        txt_view_destination.flashScrollIndicators()

        /// Prime Delivery ID = 77 in server
        if cart.count == 1,
             cart[0].id == "77"{
            isPremiumDelivery = true
        }
        
        
        
        if isPremiumDelivery{
        
            FetchPremiumDeliveryData()
        }else{
            
            setUp_Order_cost_and_address()
        }
        
    }
    //    MARK: - table View data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checkOutCart.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "checkOutCell") as! CheckOutCell
        cell.lbl_product_id.text = checkOutCart[indexPath.row].id
        cell.lbl_product_title.text = checkOutCart[indexPath.row].title
        cell.lbl_product_price.text = Currency + " " + (checkOutCart[indexPath.row].Ourprice ?? "0")
        cell.lbl_quantity.text = checkOutCart[indexPath.row].quantity
        
        cell.img_Product.image = UIImage(named: "loading")
        if let url = checkOutCart[indexPath.row].imageUrl {
            
            cell.img_Product.sd_setImage(with: url)

        }else{
            cell.img_Product.image = UIImage(named: "loading")
        }

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125.5
    }
    
    //    MARK: - Update Labels
    func setUp_Order_cost_and_address(){
        
        
        lbl_itemsCost.text = "\(PriceNProducts["total_items"] as! String) items of \(PriceNProducts["total_products"] as! String) Products / \(Currency) \(PriceNProducts["total_price"] as! String)"
        
        let userInfo = UserDefaults.standard.value(forKey: "user_information") as! [String:AnyObject]
        destination = "\(userInfo["address"] as! String),\(userInfo["town"] as! String)"
        if deliveryType == "click_and_collect" {
            destination = selectedBranch.address
        }
        
        let Date = getSelectedDateTime()
        var timeSlot = ""
        if Date.selectedStartTime != ""{
            timeSlot = " / \(Date.selectedStartTime) - \(Date.selectedEndTime)"
        }
        
        lbl_order_time.text = "User-ID: \(userInfo["user_id"] as! String)\nDate/Time: \(Date.selectedDate)\(timeSlot)"
        
        txt_view_destination.text = "Deliver At: \(destination)"
        
        if deliveryType == "click_and_collect"{
            txt_view_destination.text = "Pickup From: \(destination)"
        }

        lbl_total.text = "\(Currency) \(PriceNProducts["total_price"] as! String)"
        
    }
    
    //    MARK: - Button Edit
    
    @IBAction func Edit(_ sender: Any) {
        
        let Appearance = SCLAlertView.SCLAppearance()
        let alert = SCLAlertView(appearance: Appearance)
        
        if !isPremiumDelivery{
        if deliveryType != "click_and_collect"{
            
            let txt = alert.addTextView()
            txt.text = destination
            alert.addButton("Change Address") {
                self.txt_view_destination.text = "Deliver at: " + txt.text
                self.txt_view_destination.flashScrollIndicators()
                self.destination = txt.text
            }
        }
        }
        
//        alert.addButton("Change Date and Time") {
//            self.performSegue(withIdentifier: "gotoDeliverySection", sender: self)
//        }
        alert.showEdit(AppName,closeButtonTitle: "Cancel",colorStyle: 999999)
    }
    
    //    MARK: - Button Cash
    @IBAction func cash(_ sender: Any) {
        
//        if isPremiumDelivery{
//            createOrderPremiumDelivery()
//            return
//        }
        
        let Appearance = SCLAlertView.SCLAppearance()
        let alert = SCLAlertView(appearance: Appearance)
        alert.addButton("Confirm") {
            self.PaymentType = "Cash"
        }
        alert.showEdit("Please Confirm Order",closeButtonTitle: "Cancel",colorStyle: 999999)
        
    }
    
//    MARK: - Button Card
    @IBAction func card(_ sender: Any) {
        let Appearance = SCLAlertView.SCLAppearance()
        let alert = SCLAlertView(appearance: Appearance)
        
        alert.addButton("Make payment") {
            self.PaymentType = "Card"
        }
        alert.showEdit("Please confirm Order",closeButtonTitle: "Cancel",colorStyle: 999999)
        
        
    }
    
//    MARK: - Delivery Section Delegate
    func dateSelected() {
        if isPremiumDelivery{
        setupPremiumDeliveryUI()
        }else{
        setUp_Order_cost_and_address()
        }
    }
//    MARK: - Payment through Card
    func paymentFromCard(jobID : String) {
       if internetConnectionIsOk() {
//            let userInfo = UserDefaults.standard.value(forKey: "user_information") as! [String:String]
//            let name = userInfo["name"]
//        if let arr_name = name?.components(separatedBy: " "),
//        arr_name.count == 2{
//            let firstName = arr_name[0]
//            let lastName = arr_name[1]
//        }
            
        
            HUD.show(.progress)
            
        
        
        var totalPrice = "0"
        
        if isPremiumDelivery{
            
            totalPrice = premiumDelivery.sharedInstance.total_price
            
            
        }else{
            if let p = PriceNProducts["total_price"] as? String{
                totalPrice = p
            }
        }
        
//        let param : [String:String] = [
//            "office_name":office_name , "type":"take_payment" , "job_id":jobID , "price":totalPrice , "first_name":firstName , "last_name":lastName , "contact_number":userInfo["mobile"]! , "email":userInfo["email"]! , "city":userInfo["town"]! , "address_line_one": userInfo["address"]! , "postal_code":userInfo["post_code"]! , "country":"Pakistan" , "process_currency":"RS" , "state":userInfo["state"]!
//        ]
        
        
        let paymentURL = CardPaymentURL + jobID
        print(paymentURL)
            APIManager.sharedInstance.sessionManager.request(paymentURL).responseJSON(completionHandler: {(response)
                in
                HUD.hide()
                

                guard let paymentUrl = response.request?.url
                    else{return}
                print(paymentURL)
                UIApplication.shared.open(paymentUrl, options: [:], completionHandler: nil)
                

            })
            
        }
    }
    
    
//    MARK: - creatr Order premium delivery
    func createOrderPremiumDelivery(){
        if internetConnectionIsOk(){
            HUD.show(.progress)
            
            
            
            let  parameter : [String:String] = [
        "type" : "create_deliveryorder" , "first_name" : premiumDelivery.sharedInstance.first_name , "last_name" : premiumDelivery.sharedInstance.last_name , "job_time" : premiumDelivery.sharedInstance.job_time, "job_date" : premiumDelivery.sharedInstance.job_date, "pickup_point_name" : premiumDelivery.sharedInstance.pickup_point_name , "pickup_street_address" : premiumDelivery.sharedInstance.pickup_street_address , "pickup_phone" : premiumDelivery.sharedInstance.pickup_phone ,"pickup_email" : premiumDelivery.sharedInstance.pickup_email , "email" : premiumDelivery.sharedInstance.email , "mobile" : premiumDelivery.sharedInstance.mobile , "destination": premiumDelivery.sharedInstance.destination , "total_price" : premiumDelivery.sharedInstance.total_price , "customer_id" : premiumDelivery.sharedInstance.user_id , "vehicle_type" : premiumDelivery.sharedInstance.vehicle.id
        ]
            
//            print(parameter)
            
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: {(response) in
                
                HUD.hide()
                
                print(response.request?.url)
                print(response.result.value)
                
                switch response.result {
                    
                case .success:
                    if let dict = response.result.value as? [String:AnyObject]{
                        
                        if dict["message"] as? String == "Job created successfully"{
                            let data = dict["DATA"] as! [String:AnyObject]
                            if self.PaymentType == "Card"{
                            if let OrderID = data["order"]?["order_id"] as? Int{
                             self.paymentFromCard(jobID: String(OrderID))
                            }
                            }
                            
                            DispatchQueue.main.async {
                                
                                let story = UIStoryboard(name: "Main", bundle:nil)
                                let MMdrawerVC = story.instantiateViewController(withIdentifier: "MMDrawerVc") as! MMdrawerVc
                                UIApplication.shared.windows.first?.rootViewController = MMdrawerVC
                                UIApplication.shared.windows.first?.makeKeyAndVisible()
                                if self.PaymentType == "Cash"{
                                    SCLAlertView().showSuccess("Order Placed Successfully",circleIconImage: .checkmark)
                                }else{
                                    SCLAlertView().showSuccess(AppName,subTitle: "Delivery Booked sucessfully \n Please make payment and check your Email for further information.\nThankyou!",circleIconImage: .checkmark)
                                }
                                
                            }

                            
                        }
                        
                    }else{
                        appDelegate.showerror(str: "ERROR: error creating order")
                    }
                    
                case .failure(let error):
                    appDelegate.showerror(str: error.localizedDescription)
                
                }
                
            })
        }
        
    }
    //    MARK: - Create Order
    func createOrder(paymentType:String){
        if internetConnectionIsOk(){
            let user_information = UserDefaults.standard.value(forKey: "user_information") as! [String:AnyObject]
            
            let Date = getSelectedDateTime()
            
            var parameter = [
                "type":"create_new_order" , "office_name":office_name , "destination":destination , "customer_id":user_information["user_id"] as! String, "job_date":Date.selectedDate , "job_time":"\(Date.selectedStartTime)-\(Date.selectedEndTime)" , "notes":UserNote,"product_id":PriceNProducts["productIds"] as? String ?? "" , "product_quantity":PriceNProducts["productQunatites"] as? String ?? "" , "total_price":PriceNProducts["total_price"] as? String ?? ""  , "delivery_type" : deliveryType , "payment_type" : paymentType , "pickup_point" : ""
            ] as [String:String]
            
            
            if PaymentType == "click_and_collect" {
                parameter.removeAll()
                 parameter = [
                    "type":"create_new_order" , "office_name":office_name , "destination":"" , "customer_id":user_information["user_id"] as! String, "job_date":Date.selectedDate , "job_time":"" , "notes":UserNote,"product_id":PriceNProducts["productIds"] as? String ?? "" , "product_quantity":PriceNProducts["productQunatites"] as? String ?? "" , "total_price":PriceNProducts["total_price"] as? String ?? "" , "delivery_type" : deliveryType , "payment_type" : paymentType , "pickup_point" : selectedBranch.address , "pickup_time" : "\(Date.selectedStartTime)-\(Date.selectedEndTime)"
                ] as [String:String]
                
            }
            
            
            HUD.show(.progress)
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: {(response)in
                debugPrint(response.request?.url)
                debugPrint(response.result.value)
                HUD.hide()
                switch response.result{
                    
                case .success:
                    
                    arr_dateNtime = []
                    
                    let dict = response.result.value as! [String:AnyObject]
                    if dict["message"] as? String == "Job created successfully"{
                        let data = dict["DATA"] as! [String:AnyObject]
                        if paymentType == "Card"{
                        if let OrderID = data["order"]?["order_id"] as? Int{
                         self.paymentFromCard(jobID: String(OrderID))
                        }
                        }
                        for i in 0..<AppDelegate.sharedInstance.appCart.count{
                            AppDelegate.sharedInstance.appCart[i].quantity = "0"
                        }
////                                        Order completed so should be removed from pending orders
//                        DataBaseHelper.sharedInstance.deleteLastestEntry()
                        
                        self.dismiss(animated: true, completion: nil)
                        
                        
                        DispatchQueue.main.async {
                            
                            let story = UIStoryboard(name: "Main", bundle:nil)
                            let MMdrawerVC = story.instantiateViewController(withIdentifier: "MMDrawerVc") as! MMdrawerVc
                            self.delegate = MMdrawerVC
                            UIApplication.shared.windows.first?.rootViewController = MMdrawerVC
                            UIApplication.shared.windows.first?.makeKeyAndVisible()
                            
                            
                            let alert = SCLAlertView()
                        
                            alert.addButton("My Orders",backgroundColor: .orange) {
                                
                                self.delegate?.myOrderButtonPressed()
//                                startVC.dele
                            
                            }
                            if paymentType == "Cash"{
                                
                               alert.showSuccess("Thank you, your order has been placed in our system, we shall get back to you soon.",circleIconImage: .checkmark)
                                
                            }else{
                                alert.showSuccess(AppName,subTitle: "Thank you, your order has been placed in our system, we shall get back to you soon.",circleIconImage: .checkmark)
                            }
                        }
                    }else{
                        SCLAlertView().showError(dict["message"] as! String)
                    }
                case .failure(let error):
                    SCLAlertView().showError(error.localizedDescription)
                }
            })
            
        }
    }
//    MARK: - Button Voucher
    @IBAction func applyVoucher(_ sender: Any) {
        if isPremiumDelivery{
            SCLAlertView().showNotice(AppName,subTitle: "Sorry! Voucher is not applicable on premium delivery")
            return
        }
        let alert = SCLAlertView()
        let txt = alert.addTextField()
        alert.addButton("Done") {
            
            self.getDiscount(voucherCode: txt.text!)
        }
        alert.showCustom(AppName, subTitle: "Enter Voucher Code", color: .gray, closeButtonTitle: "Cancel", timeout: .none, circleIconImage: .some(UIImage(named: "voucher")!), animationStyle: .bottomToTop)
        
    }
//    MARK: - Get Discount
    func getDiscount(voucherCode:String){
        if internetConnectionIsOk(){
            let userInfo = UserDefaults.standard.value(forKey: "user_information") as! [String:AnyObject]
            let param = [ "type":"apply_coupon_code" , "office_name":office_name , "couponcode":voucherCode , "email":userInfo["email"] as! String ] as [String:String]
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: param).responseJSON(completionHandler: {(response) in
                switch response.result{
                case .success:
                    print(response.result.value)
                    print(response.request?.url)
                    let dict = response.result.value as! [String:AnyObject]
                    if dict["message"] as! String == "coupon available"{
                        
                        let data = dict["DATA"] as! [String:AnyObject]
                        let discountValue = data["discount_value"] as! String
                        let total = Double(self.PriceNProducts["total_price"] as! String)
                        let d = Double(discountValue)
                        let afterDiscount = total! - d!
                        
                        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: self.PriceNProducts["total_price"]as!String + " " + Currency)
                        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                        self.lbl_total.attributedText = attributeString
                        self.lbl_total_after_discount.text = "\(afterDiscount) \(Currency)"
                        self.PriceNProducts["total_price"] = String(afterDiscount)
                        print(self.PriceNProducts)
                        
                    }else{
                        appDelegate.showerror(str: dict["message"] as! String)
                    }
                    
                case .failure(let error):
                    SCLAlertView().showError(error.localizedDescription)
                }
            })
        }
    }
    
    
//    MARK: - Fetch Premium Delivery Data
    func FetchPremiumDeliveryData(){
        if internetConnectionIsOk(){
           let  parameters = ["type":"cus_listing_products"  , "office_name":office_name ,"slideno":"delivery" ] as [String:String]
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameters).responseJSON(completionHandler: {(response)  in
                debugPrint(response.result.value!)
                debugPrint(response.request?.url)
                
                switch response.result{
                case .success:
                    
                    if let dict = response.result.value as? [String:AnyObject]{
                        
                        let data = dict["DATA"] as! [String:AnyObject]
                        let arr_product = data["products"] as! [[String:AnyObject]]
                        let PD = arr_product[0] as! [String:AnyObject]
                        
                        let product = ProductData()
                        product.Description = PD["product_description"] as? String
                        product.Description = product.Description?.withoutHtml
                        product.id = PD["product_id"] as? String
                        product.favorite = PD["favorite"] as? String ?? "no"
                        product.info = PD["product_info"] as? String
                        product.Ourprice = PD["p_sell_price"] as? String
                        product.orignalPrice = PD["p_market_cutoff_price"] as? String
                        product.Stock = PD["product_stock"] as? String
                        product.title = PD["product_title"] as? String
                        product.discount = PD["p_discount_perc"] as? String
                        product.quantity = "1"
                        if let imgUrl = PD["product_image"] as? String{
                        product.imageUrl = URL(string:imgUrl)
                        }
                        if let rating = PD["product_rating"] as? String{
                        product.rating = rating
                        }
                        
                        self.checkOutCart.append(product)
                        if let price = PD["p_sell_price"] as? String{
                            var totalPrice = Int(price) ?? 0
                            totalPrice = totalPrice +  (Int(premiumDelivery.sharedInstance.vehicle.price) ?? 0)
                            
                            premiumDelivery.sharedInstance.total_price = String(totalPrice)
                        }else{
                            appDelegate.showerror(str: "Error: unable to calculate price")
                        }
                        self.setupPremiumDeliveryUI()
                        self.tbl_checkOut.reloadData()

                    }else{
                        appDelegate.showerror(str: "ERROR: Problem fetching premium delivery data.")
                    }
                    
                case .failure(let error):
                    appDelegate.showerror(str: error.localizedDescription)
                }
                
  
                
            })
        }
    }
    
    
    //    MARK: - fetch date & time slots
         func fetchDateTime(){
             if internetConnectionIsOk(){
                 let C_date = Date()
                 let date_formater = DateFormatter()
                 date_formater.dateFormat = "dd-MM-yyyy"
                 let today = date_formater.string(from: C_date)
                 //            print(today)
                 HUD.show(.progress)
                 var parameters = ["slot_date":today,"type":"future_slot_details","office_name":office_name] as [String:String]
                 if deliveryType == "click_and_collect"{
                     date_formater.dateFormat = "HH:mm"
                     let time = date_formater.string(from: C_date)
                     print(time)
                     parameters = ["slot_date":today,"slot_time":time,"type":"click_collect_slots","office_name":office_name] as [String:String]
                 }
     //            let parameters = ["slot_date":today,"type":"future_slot_details","office_name":office_name]
                 APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameters).responseJSON(completionHandler: {(response) in
                     HUD.hide()
//                                     print(response.result.value)
//                                     print(response.request?.url)
                     switch response.result{
                     case .success:
                         let dict = response.result.value as! [String:AnyObject]
                         if dict["message"] as! String == "Available Slots"{
                             
                             self.performSegue(withIdentifier: "gotoDeliverySection", sender: self)
                            
                         }else if dict["message"] as! String == "Any slot not available"{
    
                             
     //                        --- side menu segue ---
                        

                             
                         }
                         else{
                             SCLAlertView().showError(dict["message"] as! String)
                         }
                     case .failure(let error):
                         SCLAlertView().showError(error.localizedDescription)
                     }
                 })
             }
         }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoDeliverySection"{
            let destination = segue.destination as! DeliverySectionVc
            destination.delegate = self
            senderCheckOutVc = true
        }
    }
}

