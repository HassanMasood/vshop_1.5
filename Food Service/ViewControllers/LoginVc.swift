//
//  ViewController.swift
//  Food Service
//
//  Created by Hassan  on 06/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import SCLAlertView
import PKHUD
import SVProgressHUD
import Alamofire
import LocalAuthentication

protocol LoginVcDelegate : class{
    func loggedinSucessfully(NextViewController:UIViewController)
}

class LoginVc: CommonVC2,UITextFieldDelegate,PersonalVcDelegate {

    
    
    @IBOutlet weak var txt_EmailAddress: UITextField!
    @IBOutlet weak var txt_Password: UITextField!
    @IBOutlet weak var btn_remember_me: UIButton!
    @IBOutlet weak var btn_login: UIButton!
    @IBOutlet weak var btn_register: UIButton!
    
    weak var delegate : LoginVcDelegate?
    var nextViewController = UIViewController()
    var TouchIDAuthen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        
//        self.navigationItem.hidesBackButton = true
//        let newBackButton = UIBarButtonItem(title: "Back" , style: .done, target: self, action: #selector(back))
//        self.navigationItem.leftBarButtonItem = newBackButton
        
        setup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
//    MARK: - Bar Back button
//    func barBackButton(){
//        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .done, target: nil, action:"pop2ViewControllers:")
//    }
    
    @objc func back(popViewControllers:Int){
//        let VCs = self.navigationController?.viewControllers
//        print(VCs?.count)
//        VCs?.dropLast()
//        print(VCs?.count)
//        if let v = VCs{
//        self.navigationController?.viewControllers = v
//        }else{
//            self.navigationController?.popViewController(animated: true)
//        }
//        if self.navigationController!.viewControllers.count > 3 {
        
        
        
        
//        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
//        self.navigationController!.popToViewController(viewControllers[viewControllers.count - popViewControllers + 1], animated: true)
//        }else{
        
//            self.navigationController!.popViewController(animated: true)
//        }
    }
    //   MARK: - SetUP
    func setup(){
        UIApplication.shared.isIdleTimerDisabled = true
//        setupButtonUI(for: btn_login)
//        setupButtonUI(for: btn_register)
        self.title = "Login"
        
        btn_login.layer.cornerRadius = btn_login.frame.size.height / 2
        btn_login.layer.shadowColor = btn_login.backgroundColor?.cgColor
        btn_login.layer.shadowRadius = 0.0
        btn_login.layer.masksToBounds = false
        
        if let dict = UserDefaults.standard.value(forKey: "Login_Info") as? [String:String] {
            if dict["touchID"] == "enabled" {
                authTochID()
            }else{
                if dict["rememberMe"] == "yes" {
                    txt_EmailAddress.text = dict["email"] as! String
                    txt_Password.text = dict["password"] as! String
                    btn_remember_me.isSelected = true
                    btn_remember_me.setImage(UIImage(named: "check"), for: .selected)
                }
            }
        }else{
            useTouchID()
        }
        
    }
    
    //    MARK: -  button Login
    @IBAction func login_btn_pressed(_ sender: Any) {
        login(TouchID: false)
        
    }
    
    //    MARK: - Remember Me
    @IBAction func remeber_me(_ sender: UIButton) {
        
        var touchID = "notSet"
        if let dict = UserDefaults.standard.value(forKey: "Login_Info") as? [String:String] {
            touchID = dict["touchID"] ?? "notSet"
        }
        
        if sender.isSelected{
            btn_remember_me.setImage(UIImage(named:"uncheck"), for: .normal)
            let dict1 = ["email":txt_EmailAddress.text!,"password":txt_Password.text!,"touchID":touchID,"rememberMe":"no"]
            UserDefaults.standard.set(dict1, forKey: "Login_Info")
        }else{
            btn_remember_me.setImage(UIImage(named:"check"), for: .selected)
            let dict2 = ["email":txt_EmailAddress.text!,"password":txt_Password.text!,"touchID":touchID,"rememberMe":"yes"]
            UserDefaults.standard.set(dict2, forKey: "Login_Info")
        }
        sender.isSelected = !sender.isSelected
    }
    
    //   MARK: - Validations
    
    func validation()->(msg:String,isvalid:Bool){
        if txt_EmailAddress.text!.isEmpty{
            return("Email Address can not be empty",false)
        }
        if txt_Password.text!.isEmpty{
            return ("Password can not be empty",false)
        }
        return ("valid",true)
    }
    
    //MARK:- TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
    // MARK: - Button Forgot Pasword
    @IBAction func forgotPassword(_ sender: Any) {
        if internetConnectionIsOk(){
            let alert = SCLAlertView()
            let txt = alert.addTextField()
            txt.placeholder = "Please Enter Your Email"
            txt.keyboardType = .emailAddress
            alert.addButton("OK", backgroundColor: UIColor(hex: themeColour), textColor: .white, showTimeout: nil) {
                self.sendEmail_forgotPassword(email: txt.text!)
            }
            
            alert.showCustom("Forgot Password", subTitle: nil, color: UIColor(hex: themeColour)!, closeButtonTitle: "Cancel", timeout: nil, colorTextButton: getUintColor(color: UIColor.white), circleIconImage: .actions, animationStyle: .bottomToTop)
            
//            alert.showEdit(AppName,subTitle: "Please Enter Your Email Address", closeButtonTitle: "Cancel", timeout: .none,colorStyle: 999999 ,animationStyle: .bottomToTop)
            //            633333 light blue
        }
    }
    //    MARK: - Send Email Forgot Password
    func sendEmail_forgotPassword(email:String!){
        HUD.show(.progress)
        let parameters = ["type":"forget_password","office_name":office_name,"email":email!] as [String : String]
        APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameters).responseJSON(completionHandler: {(response)
            in
            HUD.hide()
            //            print(response.result.value)
            //            print(response.request?.url)
            switch response.result{
            case .success:
                let dict = response.result.value as! [String:AnyObject]
                if dict["DATA"]?["message"] as! String == "No account exist"{
                    SCLAlertView().showError("No account found with this email address.\n Please recheck spelling and try again")
                }else if dict["DATA"]?["message"] as! String == "Email sent!"{
                    SCLAlertView().showSuccess(AppName, subTitle:"Password is sent to your email address.")
                }
            case .failure(let error):
                SCLAlertView().showError(AppName, subTitle:error.localizedDescription)
            }
        })
    }
    @IBAction func unwindToLoginVc(_sender:UIStoryboardSegue){
    }
    
    //    MARK: - Login
    func login(TouchID:Bool){
        if btn_remember_me.isSelected{
            var touchID = "disabled"
            if let dict = UserDefaults.standard.value(forKey: "Login_Info") as? [String:String]{
                touchID = dict["touchID"] ?? "disabled"
            }
            let dict1 = ["email":txt_EmailAddress.text!,"password":txt_Password.text!,"touchID":touchID,"rememberMe":"yes"]
            UserDefaults.standard.set(dict1, forKey: "Login_Info")
        }
        
        self.view.endEditing(true)
        
        var check = validation()
        if TouchID == true {
            if let dictInfo = UserDefaults.standard.value(forKey: "Login_Info")as?[String:String]{
                print(dictInfo)
                if dictInfo["email"] != "" && dictInfo["password"] != "" {
                    check.isvalid = true
                }
                
            }
            
        }
        if check.isvalid{
            if internetConnectionIsOk() == false {
                return
            }
            HUD.show(.progress)
            
            var parameter = ["type":"customer_login","office_name":office_name,"email":txt_EmailAddress.text!,"password":txt_Password.text!] as [String:String]
            if TouchID == true {
                if let dict = UserDefaults.standard.value(forKey: "Login_Info") as? [String:String]{
                    parameter = ["type":"customer_login","office_name":office_name,"email":dict["email"]!,"password":dict["password"]!]
                }
            }
            print(parameter)
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler:{(response) in
                HUD.hide()
                
                debugPrint(response.request?.url)
                debugPrint(response.result.value)
                switch response.result{
                    
                case .success:
                    let dict = response.result.value as! [String:AnyObject]
                    debugPrint(dict)
                    
                    if dict["RESULT"] as! String == "OK" && dict["DATA"]?["msg"] as! String != "Login Failed..." {
                        var user_information = dict["DATA"] as! [String:AnyObject]
                        if let user_id = dict["DATA"]!["customer_id"] as? String {
                            user_information["user_id"] = user_id as AnyObject
                        }
                        if let post_code = dict["DATA"]!["post_code"] as? String{
                            user_information["post_code"] = post_code as AnyObject
                        }
                        if let currencyType = dict["DATA"]!["currency_type"] as? String{
//                            Currency = currencyType
                        }
                        
                        if let name = dict["DATA"]!["name"] as? String{
                            user_information["name"] = name as AnyObject
                        }
                        
                        if let address = dict["DATA"]!["address"] as? String{
                            user_information["address"] = address as AnyObject
                        }
                        
                        if let mobile = dict["DATA"]!["mobile"] as? Double {
                            user_information["mobile"] = mobile as AnyObject
                        }
                        
                        if let email = dict["DATA"]!["email"] as? String{
                            user_information["email"] = email as AnyObject
                        }
                        if let town = dict["DATA"]!["town"] as? String{
                            user_information["town"] = town as AnyObject
                        }
                        if let paytime = dict["DATA"]!["paytime"] as? String{
                            user_information["paytime"] = paytime as AnyObject
                        }
                        if let state = dict["DATA"]!["state"] as? String{
                            user_information["state"] = state as AnyObject
                        }
                        if let title = dict["DATA"]!["title"] as? String{
                            user_information["title"] = title as AnyObject
                        }
                        let keysToRemove = user_information.keys.filter({ (key) -> Bool in
                            if (user_information[key] as? NSNull) != nil {
                                return true
                            }
                            return false
                        })
                        
                        for key in keysToRemove {
                            user_information[key] = "" as AnyObject
                        }
                        
                        UserDefaults.standard.set(user_information,forKey:"user_information")
                        print(user_information)
                        
                        
                        
                        
                        if let dict = UserDefaults.standard.value(forKey: "Login_Info") as? [String:String] {
                            //                            print(dict)
                            if dict["touchID"] == "notAvailable" || dict["touchID"] == "disabled" {
                                DispatchQueue.main.async(execute:{
//                                    self.performSegue(withIdentifier: "gotoHome", sender: self)

                                    AppDelegate.sharedInstance.userLoggedIn = true
                                    appDelegate.showSuccess(str: "logged in sucessfully")
                                    self.navigationController?.popViewController(animated: true)
                                    self.delegate?.loggedinSucessfully(NextViewController: self.nextViewController)
                                    
                                    
                                    
                                })
                            }else if dict["touchID"] == "enabled" {
                                if self.TouchIDAuthen{
                                    DispatchQueue.main.async(execute:{
//                                        self.performSegue(withIdentifier: "gotoHome", sender: self)
                                        
                                        AppDelegate.sharedInstance.userLoggedIn = true
                                        appDelegate.showSuccess(str: "logged in sucessfully")
                                        self.delegate?.loggedinSucessfully(NextViewController: self.nextViewController)
                                        self.navigationController?.popViewController(animated: true)
                                    })
                                }else{
                                    self.authTochID()
                                    
                                }
                                
                            }else{
                                self.useTouchID()
                            }
                        }
                        
                        
                        
                    }
                    else{
                        SCLAlertView().showError(AppName,subTitle:dict["DATA"]!["msg"] as? String)
                    }
                    
                    
                case .failure(let error):
                    appDelegate.showerror(str: error.localizedDescription)
                    
                }
                
            })
            
        }else{
            SCLAlertView().showError(AppName,subTitle: check.msg)
        }
    }
//    MARK: - PersonalReg Delegate
    func loginNewUser(allowLogin: Bool, password: String, email: String) {
        if allowLogin{
            txt_Password.text = password
            txt_EmailAddress.text = email
            login(TouchID: false)
        }
        
    }
    
    //    MARK: - Button Reset Pressed
    func reSetPassword(){
        let apperance = SCLAlertView.SCLAppearance()
        let alert = SCLAlertView(appearance: apperance)
        let txt_email = alert.addTextField()
        let txt_oldPassword = alert.addTextField()
        let txt_newPassword = alert.addTextField()
        txt_email.placeholder = "Email"
        txt_newPassword.placeholder = "New Password"
        txt_oldPassword.placeholder = "Old Password"
        alert.addButton("Change") {
            self.ChangePassword(Email: txt_email.text!, NewPassword: txt_newPassword.text!, OldPassword: txt_oldPassword.text!)
        }
        
        
        
        alert.showEdit(AppName, subTitle:"Reset Password", closeButtonTitle: "Cancel",colorStyle: 999999, animationStyle: .topToBottom)
        
    }
    //    MARK: - change password
    func ChangePassword(Email:String,NewPassword:String,OldPassword:String){
        if internetConnectionIsOk(){
            let parameter = ["type":"reset_password","newpassword":NewPassword,"oldpassword":OldPassword,"email":Email]  as [String:String]
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: {(response)
                in
                print(response.result.value)
                print(response.request?.url)
                
            })
        }
    }
    //    MARK:- Authenticate using Touch ID
    func authTochID(){
        let authContext = LAContext()
        let authReason = "Please Use Touch ID to login"
        var authError: NSError?
        
        if authContext.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &authError){
            authContext.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: authReason, reply: {(sucess,error)
                in
                if sucess{
                    DispatchQueue.global(qos: .background).async {
                        
                        // Background Thread
                        
                        DispatchQueue.main.async {
                            //                            print("Authenticated")
                            self.login(TouchID: true)
                            self.TouchIDAuthen = true
                        }
                    }
                }else{
                    if let error = error {
                        DispatchQueue.global(qos: .background).async {
                            
                            // Background Thread
                            
                            DispatchQueue.main.async {
                                self.reportTouchIdError(error: error as NSError)
                                if let dictInfo = UserDefaults.standard.value(forKey: "Login_Info") as? [String:String]{
                                    let dict = ["email":dictInfo["email"]!,"password":dictInfo["password"],"touchID":"disabled"]
                                    UserDefaults.standard.set(dict, forKey: "Login_Info")
                                }
                            }
                        }
                        
                    }
                }
            })
        }else{
            print(authError?.localizedDescription ?? "Unknow error")
        }
    }
    
    //    MARK: - Use Touch ID
    func useTouchID(){
        //        let dict = ["email":txt_EmailAddress.text!,"password":txt_Password.text!]
        //        UserDefaults.standard.set(dict, forKey: "Login_Info")
        if let dict = UserDefaults.standard.value(forKey: "Login_Info") as? [String:String]{
            if dict["touchID"] == "notAvailable" || dict["touchID"] == "disabled" {
                print(dict["touchID"]!)
                return
            }else if dict["touchID"] == "enabled" {
                
                authTochID()
                return
            }
        }else{
            let dict = ["email":"","password":"","touchID":"notSet","rememberMe":"no"]
            UserDefaults.standard.set(dict, forKey: "Login_Info")
            return
        }
        
        
        if checkAuth(){
            let alert = UIAlertController(title: AppName, message: "Would you like to use Touch ID to login?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Use Touch ID", style: UIAlertAction.Style.default, handler: {(action)
                in
                let dict = ["email":self.txt_EmailAddress.text!,"password":self.txt_Password.text!,"touchID":"enabled","rememberMe":"no"]
                UserDefaults.standard.set(dict, forKey: "Login_Info")
                self.login(TouchID: true)
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {(cancel)
                in
                if let dict1 = UserDefaults.standard.value(forKey: "Login_Info") as? [String:String]{
                    let dict = ["email":self.txt_EmailAddress.text!,"password":self.txt_Password.text!,"touchID":"disabled","rememberMe":dict1["rememberMe"]]
                    UserDefaults.standard.set(dict, forKey: "Login_Info")
                }
                
                
            }))
            present(alert, animated: true)
        }else{
            if let dictInfo = UserDefaults.standard.value(forKey: "Login_Info") as? [String:String]{
                let dict = ["email":txt_EmailAddress.text!,"password":self.txt_Password.text!,"touchID":"notAvailable","rememberMe":dictInfo["rememberMe"] ?? ""]
                UserDefaults.standard.set(dict, forKey: "Login_Info")
            }
        }
        
    }
    
    
    
    //    MARK: - Report Touch ID Error
    func reportTouchIdError(error:NSError){
        switch error.code {
        case LAError.authenticationFailed.rawValue:
            print("Authentication failed")
        case LAError.passcodeNotSet.rawValue:
            print("Passcode not set")
        case LAError.systemCancel.rawValue:
            print("authentication was canceled by the system")
        case LAError.biometryNotAvailable.rawValue:
            print("touch id is not available")
        case LAError.userCancel.rawValue:
            print("ooops")
            
        default:
            print(error.localizedDescription)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoPersonalRegVc"{
            if let nav = segue.destination as? UINavigationController{
                let destination = nav.topViewController as! PersonalRegVc
                destination.delegate = self
            }
        }
    }
    
    func getUintColor(color : UIColor)->UInt{
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        var colorAsUInt : UInt32 = 0
        if color.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {

            

            colorAsUInt += UInt32(red * 255.0) << 16 +
                           UInt32(green * 255.0) << 8 +
                           UInt32(blue * 255.0)
          
            
        }
        return UInt(colorAsUInt)
    }
}

