//
//  PersonalRegVc.swift
//  Food Service
//
//  Created by Hassan  on 08/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import SCLAlertView
import Reachability
import PKHUD

@objc protocol PersonalVcDelegate: class {
    @objc optional func updateButtonPressed()
    @objc optional func loginNewUser(allowLogin:Bool , password : String , email : String)
}

class PersonalRegVc: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    
    
    
    @IBOutlet weak var view_Personalinfo: UIView!
    @IBOutlet weak var view_Address: UIView!
    @IBOutlet weak var view_phone1: UIView!
    @IBOutlet weak var view_Authentication: UIView!
    
    
    @IBOutlet weak var txt_first_name: UITextField!
    @IBOutlet weak var txt_last_name: UITextField!
    @IBOutlet weak var txt_address: UITextField!
    @IBOutlet weak var txt_town: UITextField!
    @IBOutlet weak var txt_post_code: UITextField!
    @IBOutlet weak var txt_mobile_num: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    
    @IBOutlet weak var btn_register: UIButton!
    @IBOutlet weak var btn_business: UIButton!
    @IBOutlet weak var btn_personal: UIButton!
    
    @IBOutlet weak var pickerView_postCode: UIPickerView!
    
    @IBOutlet weak var stack_pickerView: UIStackView!
    @IBOutlet weak var stack_reg_type: UIStackView!
    
    var registrationType = "personal"
    var postCodeAdresses = [String]()
    
    var sender = UIViewController()
    
    weak var delegate : PersonalVcDelegate?
    //    MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        internetConnectionIsOk()
        super.viewDidLoad()
        setupUI()
    }
    
    // MARK: - Setup
    
    func setupUI(){
        txt_email.delegate = self
        setupButtonUI(for: btn_personal)
        setupButtonUI(for: btn_business)
        
        if sender.title == "Account" {
            stack_reg_type.isHidden = true
            btn_register.setTitle("Update", for: .normal)
            
            if let userInfo = UserDefaults.standard.value(forKey: "user_information") as? [String:String]{
            
//            let name = userInfo["name"]
//            let arr_name = name?.components(separatedBy: " ")
            
//                txt_first_name.text = arr_name![0]
//                txt_last_name.text = arr_name![1]
                
                txt_first_name.text = userInfo["name"]
                
                txt_town.text = userInfo["town"]
                
                txt_email.text = userInfo["email"]
                
                txt_password.isHidden = true
                
                txt_post_code.text = userInfo["post_code"]
                
                txt_address.text = userInfo["address"]
                
                txt_mobile_num.text = userInfo["mobile"]
                
                
            }
        }
        
    }
//    MARK: - Button Personal and Business
    @IBAction func RegistrationType(_ sender: UIButton) {
        let Unselectedcolor = UIColor.init(hex: "6ACADF")
        let selectedcolor = UIColor.init(hex: "6FAAFA")
        
        if sender.tag == 0{
            registrationType = "personal"
            btn_business.backgroundColor = Unselectedcolor
            btn_personal.backgroundColor = selectedcolor
        }else{
            registrationType = "business"
            btn_business.backgroundColor = selectedcolor
            btn_personal.backgroundColor = Unselectedcolor
        }
    }
    
    //    MARK: - Picker View Data Source & Delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return postCodeAdresses.count
    }
    
    /// Title for row
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel
        
        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 400))
        }
        
        label.text = postCodeAdresses[row]
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.sizeToFit()
        
        return label
        
    }
    
    //    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    //        postCodeAdresses[row]
    //
    //    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 80.0
    }
    
    //    MARK: - hide with animation
    func hideViewWithFade(_ view: UIView) {
        if view.isHidden {
            view.alpha = 0.0
        }
        
        view.isHidden = false
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .transitionCrossDissolve, animations: {
            view.alpha = view.alpha == 1.0 ? 0.0 : 1.0
        }, completion: { _ in
            view.isHidden = !Bool(truncating: view.alpha as NSNumber)
        })
    }
    // MARK: - Register
    
    @IBAction func registerUpdate(_ Sender:UIButton) {
        if sender.title == "Account"{
            updateUserProfile()
        }else{
            register_user()
        }
        
    }
    
    
//    MARK: - Update user Profile
    func updateUserProfile(){
       
        if internetConnectionIsOk() {
        let check = validation()
        
            if check.isValid{
                HUD.show(.progress)
                let param = [ "office_name":office_name , "type":"edit_user_profile" , "email": txt_email.text! , "name": txt_first_name.text! + " " + txt_last_name.text! , "mobile": txt_mobile_num.text! , "address": txt_address.text! , "city": txt_town.text! , "post_code":txt_post_code.text! ,"title":""]  as [String:String]
                APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: param).responseJSON(completionHandler: {(response) in
                    HUD.hide()
                    print(response.request?.url)
                    print(response.result.value)
                    switch response.result{
                    case .success:
                        let dict = response.result.value as! [String:AnyObject]
                        if let msg = dict["message"]?["msg"] as? String{
                            if msg != "user profile"{
                                appDelegate.showerror(str:msg)
                            }else{
                                
                                appDelegate.showSuccess(str: "Updated Sucessfully")
                                self.updateUserInfo(dict: dict["DATA"]!["user_profile"] as! [String:AnyObject])
                                self.delegate?.updateButtonPressed!()
                                self.dismiss(animated: true, completion: nil)
                               
                            }
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            appDelegate.showerror(str: error.localizedDescription)
                        }
                    }
                })
                
            }else{
                SCLAlertView().showError(AppName,subTitle: check.msg)
            }
    }
       

    }
//    MARK: - Register user
    func register_user(){
        let check =  validation()
        
        if check.isValid{
            
            if   internetConnectionIsOk(){
                
                let parameter = ["type":"customer_register","name":txt_first_name.text! + " " + txt_last_name.text! ,"office_name":office_name , "password":txt_password.text! , "address":txt_address.text! , "town":txt_town.text! ,"postcode":txt_post_code.text! , "mobile":txt_mobile_num.text! , "email":txt_email.text! , "register_type": registrationType  ] as [String:String]
                
                HUD.show(.progress)
                APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: { (response) in
                    switch response.result{
                    case .success:
                        
                        let data = response.result.value as! [String:AnyObject]
                        debugPrint(data)
                        if data["DATA"]?["msg"] as! String == "Successfully registered into the system."{
                            SCLAlertView().showSuccess(AppName,subTitle: "You are registered.")
                            self.delegate?.loginNewUser?(allowLogin: true, password: self.txt_password.text!, email: self.txt_email.text!)
                            self.performSegue(withIdentifier: "unwindToLoginVc", sender: self)
                        }
                        
                        if data["DATA"]?["msg"] as! String == "Problem while registering into our system."{
                            SCLAlertView().showError(AppName,subTitle:"Server error")
                        }
                        
                        if data["DATA"]?["msg"] as! String == "Email address already exist in our system."{
                            SCLAlertView().showError(AppName,subTitle:"Email address already registered.")
                        }else if data["DATA"]?["msg"] as! String == "Email address or mobile already exist in our system."{
                            SCLAlertView().showError(AppName,subTitle:"Email address or mobile already registered.")
                        }
                        HUD.hide()
                    case .failure(let error):
                        HUD.hide()
                        DispatchQueue.main.async(execute: {
                            SCLAlertView().showError(AppName,subTitle: error.localizedDescription)
                        })
                    }
                })
            }
        }else{
            SCLAlertView().showError(AppName, subTitle: check.msg)
        }
        
    }
    
    //     MARK:- Validations
    func validation ()->(msg:String,isValid:Bool){
        if txt_first_name.text!.isEmpty{
            return ("First Name can not be empty.",false)
        }
//        if txt_last_name.text!.isEmpty{
//            return ("Last Name can not be empty.",false)
//        }
        if txt_address.text!.isEmpty{
            return ("Address can not be empty.",false)
        }
//        if txt_town.text!.isEmpty{
//            return ("town can not be empty.",false)
//        }
//        if txt_post_code.text!.isEmpty{
//            return ("Post code can not be empty.",false)
//        }
        if txt_mobile_num.text!.isEmpty{
            return ("Phone number can not be empty.",false)
        }
        if txt_email.text!.isEmpty{
            return ("Email can not be empty.",false)
        }
        if sender.title != "Account"{
            if txt_password.text!.isEmpty{
                return ("Password cannot be empty.",false)
            }
        }
        if isValidEmail(emailID: txt_email.text!) == false {
            return ("Please enter valid Email.",false)
        }
        
        return ("valid",true)
    }
    
    
    
    func isValidEmail(emailID:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailID)
    }
    
    //    MARK: - Text Field Delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        if isValidEmail(emailID: txt_email.text!) == false {
            txt_email.textColor = UIColor.red
        }
        if textField == txt_post_code {
            getPostCode()
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txt_email.textColor = UIColor.black
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //    MARK: - Button Done and cancel pressed
    @IBAction func selectAddress(_ sender: UIButton) {
        txt_address.text = ""
        let CommaSepratedAdress = postCodeAdresses[pickerView_postCode.selectedRow(inComponent: 0)].components(separatedBy: ",")
        if sender.tag == 0 {
            
            for i in 0..<CommaSepratedAdress.count-2{
                txt_address.text! += CommaSepratedAdress[i]
            }
            
            for i in 0..<CommaSepratedAdress.count-1{
                txt_town.text = CommaSepratedAdress[i]
            }
            
        }else if sender.tag == 1{
            hideViewWithFade(stack_pickerView)
        }
    }
    
    //    MARK: - Get Post code
    func getPostCode(){
        if internetConnectionIsOk(){
            HUD.show(.progress)
            let parameter = ["office_name":office_name,"type":"get_address_io","address":txt_post_code.text!]
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: {(response)
                in
                HUD.hide()
                print(response.result.value)
                print(response.request?.url)
                switch response.result{
                case .success:
                    if let dict = response.result.value as? [String:AnyObject]{
                        if dict["RESULT"] as! String == "OK"{
                            if let data = dict["DATA"] as? [String] {
                            self.postCodeAdresses = data
                            print(self.postCodeAdresses)
                            self.pickerView_postCode.reloadAllComponents()
                            if self.stack_pickerView.isHidden == true{
                                self.hideViewWithFade(self.stack_pickerView)
                            }
                        }
                        }else{
                            print("error")
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            })
            
        }
    }
    
    
    //    MARK: - Update User Info
    func updateUserInfo(dict:[String:AnyObject]){
        
        debugPrint(dict)
        
        
        var user_information = dict
        print(user_information)
                if let user_id = dict["customer_id"] as? String {
                    user_information["user_id"] = user_id as AnyObject
                }
                if let post_code = dict["post_code"] as? String{
                    user_information["post_code"] = post_code as AnyObject
                }
                
                if let name = dict["name"] as? String{
                    user_information["name"] = name as AnyObject
                }
                
                if let address = dict["address"] as? String{
                    user_information["address"] = address as AnyObject
                }
                
                if let mobile = dict["mobile"] as? Double {
                    user_information["mobile"] = mobile as AnyObject
                }
                
                if let email = dict["email"] as? String{
                    user_information["email"] = email as AnyObject
                }
                if let town = dict["town"] as? String{
                    user_information["town"] = town as AnyObject
                }
                if let paytime = dict["paytime"] as? String{
                    user_information["paytime"] = paytime as AnyObject
                }
                if let title = dict["title"] as? String{
                    user_information["title"] = title as AnyObject
                }
                let keysToRemove = user_information.keys.filter({ (key) -> Bool in
                    if (user_information[key] as? NSNull) != nil {
                        return true
                    }
                    return false
                })
                
                for key in keysToRemove {
                    user_information[key] = "" as AnyObject
                }

                UserDefaults.standard.set(user_information,forKey:"user_information")
                 print(user_information)
    
    }

    
}


extension UIColor {

    // MARK: - Initialization

    convenience init?(hex: String) {
        var hexSanitized = hex.trimmingCharacters(in: .whitespacesAndNewlines)
        hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")

        var rgb: UInt32 = 0

        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 1.0

        let length = hexSanitized.count

        guard Scanner(string: hexSanitized).scanHexInt32(&rgb) else { return nil }

        if length == 6 {
            r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
            g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
            b = CGFloat(rgb & 0x0000FF) / 255.0

        } else if length == 8 {
            r = CGFloat((rgb & 0xFF000000) >> 24) / 255.0
            g = CGFloat((rgb & 0x00FF0000) >> 16) / 255.0
            b = CGFloat((rgb & 0x0000FF00) >> 8) / 255.0
            a = CGFloat(rgb & 0x000000FF) / 255.0

        } else {
            return nil
        }

        self.init(red: r, green: g, blue: b, alpha: a)
    }

    // MARK: - Computed Properties

    var toHex: String? {
        return toHex()
    }

    // MARK: - From UIColor to String

    func toHex(alpha: Bool = false) -> String? {
        guard let components = cgColor.components, components.count >= 3 else {
            return nil
        }

        let r = Float(components[0])
        let g = Float(components[1])
        let b = Float(components[2])
        var a = Float(1.0)

        if components.count >= 4 {
            a = Float(components[3])
        }

        if alpha {
            return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
        } else {
            return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
        }
    }

}

    

