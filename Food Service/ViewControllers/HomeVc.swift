//
//  HomeVc.swift
//  Food Service
//
//  Created by Hassan  on 07/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import PKHUD
import SCLAlertView
import AlamofireImage
class HomeVc: CommonVc,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    
    
    
    
    
//    @IBOutlet weak var btn_PlaceOrder: UIButton!
    @IBOutlet weak var btn_premium_delivery : UIButton!
    @IBOutlet weak var img_delivery : UIImageView!
    @IBOutlet weak var collection_view_promo_images: UICollectionView!
    @IBOutlet weak var collection_view_category_images: UICollectionView!
    @IBOutlet weak var view_delivery_back_ground: UIView!
    
        
    struct promotional_data{
        var banner_img : URL
        var slide_no : String
    }

//    func loggedinSucessfully(NextViewController: UIViewController) {
//
//
//
//    }
    
    var arr_slider_data: [promotional_data] = []
    var arr_category_data : [Category] = []
    var bannerNumber = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    
///            self.getDeliveryStatus()
        
            self.fetchPromoImgs()
            self.fetchProductCatagory()
        
///            self.fetchPremiumDeliveryData()
        
            self.fetchProductsForAppCart()
        
///            AppDelegate.sharedInstance.getMinOrderPrice(lable: nil)
        
//            self.scheduledTimerWithTimeInterval()
        
    
    }
    
    func setup(){
        
//            AddshadowTo(view: img_delivery)
        
        
    }
    
    
    //    MARK: - Collection View Data Source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collection_view_promo_images{
            return arr_slider_data.count
        }
        return arr_category_data.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collection_view_promo_images{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "promoCell", for: indexPath) as! PromotionalCell
            let Width = Int(UIScreen.main.bounds.width)
            let ImgView: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: Width, height: Width / 2))
            ImgView.af_setImage(withURL: arr_slider_data[indexPath.row].banner_img)
            ImgView.contentMode = .scaleToFill
            cell.addSubview(ImgView)
            
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCollectionViewCell", for: indexPath) as! CategoryCollectionViewCell
        
        if let imgUrl = arr_category_data[indexPath.row].img_url{
            cell.image_View_product.af_setImage(withURL: imgUrl,placeholderImage: #imageLiteral(resourceName: "loading"))
        }else{
            cell.image_View_product.image = UIImage(named: "loading")
        }
        cell.lbl_title.text = arr_category_data[indexPath.row].name
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collection_view_promo_images{

            performSegue(withIdentifier: "gotoOrderPad2VC", sender: arr_slider_data[indexPath.row].slide_no)

        }
        else{
//        performSegue(withIdentifier: "gotoOrderPad2VC", sender: arr_category_data[indexPath.row])
            if arr_category_data[indexPath.row].id != "0"{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let ProductCategoryVc = storyboard.instantiateViewController(identifier: "ProductCatagoriesVc") as! ProductCatagoriesVc
                ProductCategoryVc.categoryID = arr_category_data[indexPath.row].id
                self.navigationController?.pushViewController(ProductCategoryVc, animated: true)
            }
//            fetchProductCatagory(parentCategory: arr_category_data[indexPath.row].id)
        }
        
        
        
    }
    
    //    MARK: - Collection View Delegate Flow Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collection_view_promo_images{
            let size = UIScreen.main.bounds
            return CGSize(width: size.width , height: CGFloat(220))
        }
//        return CGSize(width: view.frame.size.width/2, height: view.frame.size.width/2)
        return CGSize(width: 40, height: 100)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //    MARK:- Timeer to change slid
    func scheduledTimerWithTimeInterval(){
        _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector:Selector("changerBanner"), userInfo: nil, repeats: true)
        
    }
    
    //    MARK: - Change slide
    @objc func changerBanner(){
        
        if arr_slider_data.isEmpty{return}
        
        
        
        var indexpths : [NSIndexPath] = []
        for i in 0..<collection_view_promo_images.numberOfSections{
            for j in 0..<collection_view_promo_images.numberOfItems(inSection: i){
                indexpths.append(NSIndexPath(item: j, section: i))
            }
        }
        
        let cells = collection_view_promo_images.visibleCells
        let current_index = collection_view_promo_images.indexPath(for: cells[0])
        bannerNumber = current_index!.row
        
        if bannerNumber == (arr_slider_data.count - 1) {
            bannerNumber = -1
        }
        bannerNumber += 1
        
        collection_view_promo_images.scrollToItem(at: indexpths[bannerNumber] as IndexPath, at: .left, animated: true)
        
    }
    //    MARK: - Fetch Promotional Images
    func fetchPromoImgs(){
        //        if internetConnectionIsOk(){
        //            (CASE : slider_images ,
        //            Demo = <YOUR LINK>/vshopcustomer.php?office_name=&type=slider_images )
        HUD.show(.progress)
        //            let param = ["type":"slider_images" , "office_name":office_name,"imagetype":"popular_category"]
        if !arr_slider_data.isEmpty{return}
        let param = ["type":"slider_images" ,"imagetype":"popular_category"] as [String:String]
        APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: param).responseJSON(completionHandler: {(response) in
//            HUD.hide()
            print(response.request?.url)
            print(response.result.value)
            switch response.result{
                
            case .success:
                let dict = response.result.value as! [String:AnyObject]
                print(dict)
                if dict["message"] as! String == "slider images" {
                    if let data = dict["DATA"] as? [[String:AnyObject]]{
                    for i in 0..<data.count{
                        if var StringUrl =  data[i]["image"] as? String{
                            print(StringUrl)
                           StringUrl = StringUrl.replaceURL()
                            print(StringUrl)
                        if let url = URL(string: StringUrl){
                        self.arr_slider_data.append(promotional_data(banner_img: url , slide_no: String(i + 1)))
                        }
                        }
                    }
                    }
                    self.collection_view_promo_images.reloadData()
                }else{
                    appDelegate.showerror(str: dict["message"] as! String)
                }
            case .failure(let error):
                //                    appDelegate.showerror(str: error.localizedDescription)
                print("error fetching products")
                
            }
        })
        //        }
    }
//    MARK: - Fetch Premium Delivery Image
    func fetchPremiumDeliveryData(){
        HUD.show(.progress)
        
        let param = ["type":"slider_images", "office_name":office_name] as [String:String]
        APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: param).responseJSON(completionHandler: {(response) in
            HUD.hide()

            
            print(response.request?.url)
            print(response.result.value)
            switch response.result{
            
            case .success:
                let dict = response.result.value as! [String:AnyObject]
                let data =  dict["DATA"] as! [[String:AnyObject]]
                if let imgString = data[0]["delivery_service"] as? String {
                    if let ImgUrl = URL(string: imgString.replacingOccurrences(of: ".com", with: ".lk")){
                        self.img_delivery.af_setImage(withURL: ImgUrl)
                    }else{
                        print("ERROR: unable to load delivery image")
                    }
                }else{
                    AppDelegate.sharedInstance.showerror(str: dict["message"] as? String)
                }
                
            case .failure(let error):
                print(error)
            }
        })
    }
    
//    MARK: - Premium Delivery tapped
//    @IBAction func btn_delivery_tapped() {
//        if let Drawer = drawer() ,
//            let manager = drawer()?.getManager(direction: .left){
//            deliveryProductisEnabled = true
//            Drawer.setMainWith(identifier: "gotoOrderPad2Vc")
//        }
//        if AppDelegate.sharedInstance.userLoggedIn{
//        self.performSegue(withIdentifier: "gotoPremiumDelivery", sender: self)
//
//        }else{
//            loginResquest(VC: self)
//        }
//
//    }
    // MARK: - Side Menu
    @IBAction func Menu() {
        if let drawer = self.drawer() ,
            let manager = drawer.getManager(direction: .left){
            let value = !manager.isShow
            drawer.showLeftSlider(isShow: value)
        }
    }
    
    //    MARK: - Button My Orders
//    @IBAction func myOrders(_ sender: Any) {
//        //        segue performed from side menu vc
//        if let Dra = self.drawer(){
//            Dra.showLeftSlider(isShow: false)
//            Dra.setMainWith(identifier: "gotoMyOrders")
//        }
//    }
    
    
    @IBAction func unwindToHomeVc(_ sender: UIStoryboardSegue) {
        
    }
    
    
    //    http://vshoplanka.com/taxi_app/WebServices/vshopcustomer.php?type=cus_listing_products&category_id=0&customer_id=256&slideno=best_seller
    
    
    //    func fetchProducts(){
    ////        if internetConnectionIsOk(){
    ////            let userInfo = UserDefaults.value(forKey: "user_information") as! [String:String]
    //            let param = ["office_name":office_name,"type":"cus_listing_products","customer_id":"256","category_id":"0","slideno":"best_seller"]
    //            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: param).responseJSON(completionHandler: {(response)
    //                in
    //                print(response.request?.url)
    //                print(response.result.value)
    //
    //                switch response.result {
    //                case .success:
    //                    print("")
    //                case .failure(let error):
    //                    AppDelegate.sharedInstance.showerror(str: error.localizedDescription)
    //                }
    //            })
    ////        }
    //    }
    
    //    MARK: - Fetch Producd Catagories
    func fetchProductCatagory(parentCategory: String? = "0"){
        if internetConnectionIsOk(){
//            if !arr_category_data.isEmpty{return}
            let paramter = ["type":"category_list","office_name":office_name , "parent_category" : parentCategory! ]  as [String:String]
            HUD.show(.progress)
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: paramter).responseJSON(completionHandler: {(response)
                in
                HUD.hide()
                print(response.result.value)
                print(response.request?.url)
                switch response.result{
                case .success:
                    
                    let dict = response.result.value as! [String:AnyObject]
                    let data = dict["DATA"] as! [String:AnyObject]
                    if dict["message"] as! String == "Available Category"  {
//                        self.arr_category_data = []
                        let categories = data["categories"] as! [[String:AnyObject]]
                        for i in 0..<categories.count{
                            let c = Category()
                            c.id = categories[i]["category_id"] as? String ?? ""
                            c.name = categories[i]["category_name"] as? String ?? ""
                            if var img_url = categories[i]["category_image"] as? String {
                               img_url = img_url.replacingOccurrences(of: ".com", with: ".lk")
                                c.img_url = URL(string: img_url)
                            }
                            
                            self.arr_category_data.append(c)
                            
                        }
                        if parentCategory != "0"{
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let ProductCategoryVc = storyboard.instantiateViewController(identifier: "ProductCatagoriesVc") as! ProductCatagoriesVc
                            ProductCategoryVc.arr_categories = self.arr_category_data
                            self.navigationController?.pushViewController(ProductCategoryVc, animated: true)
                        }else{
                        self.collection_view_category_images.reloadData()
                        }
                    }else{
                        AppDelegate.sharedInstance.showPopUP(message: "Sorry! Products not available yet." , sender: self)
                    }
                    
                case .failure(let error):
                    
                    appDelegate.showerror(str: error.localizedDescription)
                    
                }
            })
        }
    }
    
//    func fetchProducts(){
//        //        if AppDelegate.sharedInstance.appCart.isEmpty == false {return}
//        //                    if internetConnectionIsOk(){
//
//        //                        let userInfo = UserDefaults.standard.value(forKey: "user_information") as! [String:AnyObject]
//        //                        let parameters = ["type":"listing_products","category_id": "","office_name":office_name,"customer_id":userInfo["user_id"] as! String]
//        let param = ["office_name":office_name,"type":"cus_listing_products","slideno":"best_seller"]
//
//        HUD.show(.progress)
//        APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: param).responseJSON(completionHandler:{ (response) in
//            HUD.hide()
//            switch response.result{
//            case .success:
//                print(response.request?.url)
//                print(response.result.value)
//                let  dict = response.result.value as! [String:AnyObject]
//                //                                var msgResponse = ""
//                let message = dict["message"] as! String
//                //                                 msgResponse = message[0] as! String
//                if  message == "Available products" {
//                    if let data = dict["DATA"]?["products"] as? [[String : AnyObject]]{
//                        for i in 0..<data.count{
//                            let PD = ProductData()
//                            if let ProductID = data[i]["product_id"] as? String {
//                                PD.id = ProductID
//                                PD.info = data[i]["product_info"] as? String
//                                PD.info = PD.info?.withoutHtml
//                                PD.Description = data[i]["product_description"] as? String
//                                PD.info = PD.info?.withoutHtml
//                                PD.price = data[i]["product_price"] as? String
//                                PD.Stock = data[i]["product_stock"] as? String
//                                PD.discount = data[i]["product_discount"] as? String
//                                PD.title = data[i]["product_title"] as? String
//                                PD.favorite = (data[i]["favorite"] as? String)!
//                                let imgUrl = data[i]["product_image"] as? String ?? ""
//                                PD.imageUrl = URL(string:imgUrl)
//                                self.arr_category_data.append(PD)
//                            }
//                        }
//                        self.collection_view_category_images.reloadData()
//                    }
//                }else{
//                    DispatchQueue.main.async {
//                        appDelegate.showerror(str: message)
//                    }
//                }
//            case .failure(let error):
//                SCLAlertView().showError(error.localizedDescription)
//            }
//        })
//        //                    }
//    }
    
    //    MARK: - GET Delivery status
    func getDeliveryStatus(){
        if AppDelegate.sharedInstance.deliveryStatus == "0"{}else{return}
                HUD.show(.progress)
        //            if internetConnectionIsOk(){
        let param = ["office_name":office_name,"type":"delivery_status"] as [String:String]
        
        APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: param).responseJSON(completionHandler: {(response)
            in
            //                HUD.hide()
            print(response.request?.url)
            print(response.result.value)
            switch response.result{
            case .success:
                let dict = response.result.value as! [String:AnyObject]
                if dict["message"] as! String == "success"{
                    let data = dict["DATA"] as! [[String:AnyObject]]
                    AppDelegate.sharedInstance.deliveryStatus = data[0]["delivery_status"] as! String
                    AppDelegate.sharedInstance.deliveryStatus = "3"
                }
            case .failure(let error):
                print(error)
            }
        })
        //            }
    }
    
    
    //    MARK: - Fetch Products For App Cart
    func fetchProductsForAppCart(){
//        return
        
        
        if internetConnectionIsOk(){
            
            if !AppDelegate.sharedInstance.appCart.isEmpty{return}
            HUD.show(.progress)
            var userInfo = [String:AnyObject]()
            if let userinfo = UserDefaults.standard.value(forKey: "user_information") as? [String:AnyObject]{
                userInfo = userinfo
                            }
                let parameters = ["type":"listing_products","category_id": "","office_name":office_name,"customer_id":userInfo["user_id"] as? String ?? ""]  as [String:String]
//                HUD.show(.progress)
                
                APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameters).responseJSON(completionHandler:{ (response) in
                    HUD.hide()
                    
                    
                    switch response.result{
                    case .success:
                        print(response.request?.url)
                        print(response.result.value)
                        let  dict = response.result.value as! [String:AnyObject]
                        var msgResponse = ""
                        let message = dict["message"] as! [AnyObject]
                        msgResponse = message[0] as! String
                        if  msgResponse == "Available products" {
                            if let data = dict["DATA"]?["products"] as? [[String : AnyObject]]{
                                for i in 0..<data.count{
                                    let PD = ProductData()
                                    if let ProductID = data[i]["product_id"] as? String {
                                        PD.id = ProductID
                                        
                                        
                                        PD.info = data[i]["product_info"] as? String
                                        /// changing HTML to attributed string is taking too long
//                                        PD.info = PD.info?.withoutHtml
                                        
                                        PD.Description = data[i]["product_description"] as? String
                                        /// changing HTML to attributed string is taking too long
//                                        PD.Description = PD.Description?.withoutHtml
                                        
                                        
                                        PD.Stock = data[i]["product_stock"] as? String
                                        PD.title = data[i]["product_title"] as? String
                                        PD.favorite = (data[i]["favorite"] as? String)!
                                        if let imgURL = data[i]["product_image"] as? String {
                                        PD.imageUrl = URL(string:(imgURL.replaceURL()))
                                        }
                                        PD.orignalPrice = data[i]["p_market_cutoff_price"] as? String
                                        PD.discount = data[i]["p_discount_perc"] as? String
                                        PD.Ourprice = data[i]["p_sell_price"] as? String
                                        PD.otherImgUrl = data[i]["other_img"] as? String
                                        
                                        if let rating = data[i]["product_rating"] as? Int{
                                            PD.rating = String(rating)
                                        }
                                        
                                        if let int_sold = data[i]["product_sold"] as? Int{
                                            PD.sold = String(int_sold)
                                        }
                                        
                                        AppDelegate.sharedInstance.appCart.append(PD)
                                    }
                                }
                            }
                            
                        }else{
                            DispatchQueue.main.async {
                                appDelegate.showerror(str:dict["message"] as? String)
                            }
                        }
                    case .failure(let error):
                        //                    SCLAlertView().showError(error.localizedDescription)
                        print(error)
                    }
                })
//            }
        }
    }
    
//    func showPopUP(message:String){
//        let alert = UIAlertController(title: office_name, message: message, preferredStyle: .alert)
//
//        let subview :UIView = alert.view.subviews.first! as UIView
//        let alertContentView = subview.subviews.first! as UIView
//
//        alertContentView.backgroundColor = UIColor.init(hex: themeColour)
//
//        self.present(alert, animated: true) {
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
//                alert.dismiss(animated: true, completion: nil)
//            }
//        }
//
//    }
 
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoOrderPad2VC"{
            
            let destination = segue.destination as! OrderPad2Vc
            
            if let category = sender as? Category{
                destination.category = category
                
            }
            
            if let slide_no = sender as? String{
                destination.slideNo  = slide_no
                destination.fetchPromotionalProductsEnable = true
            }
            
        }
    }
    
}



