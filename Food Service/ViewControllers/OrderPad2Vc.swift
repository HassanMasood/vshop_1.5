//
//  OrderPad2Vc.swift
//  Food Service
//
//  Created by Hassan  on 24/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//


import UIKit
import Alamofire
import AlamofireImage
import PKHUD
import SCLAlertView
import AudioToolbox
import Reachability
protocol OrderPad2Delegate : class {
    func orderPad2Dismissed()
}

class OrderPad2Vc: CommonVc,UITableViewDataSource, UITableViewDelegate , productCellDelegate , informationVCDelegate {

    
    
    
    
    
    
    weak var delegate : OrderPad2Delegate?
    var selected_item_for_Information = 0
//    var arr_searchedProducts = [ProductData]()
    var arr_Products = [ProductData]()
    var rowHeight : CGFloat = 200
    var sender = UIViewController()
    var slideNo = String()
    var category = Category()
    var searchProductsEanble = false
    var fetchPromotionalProductsEnable = false
    
    
    @IBOutlet weak var tbl_products: UITableView!
//    @IBOutlet weak var btn_cart: UIButton!
    @IBOutlet weak var lbl_itemsCost: UILabel!
    @IBOutlet weak var lbl_category_name : UILabel!
//    @IBOutlet weak var btn_searchBar: UIBarButtonItem!
    @IBOutlet weak var btn_menu: UIBarButtonItem!
    @IBOutlet weak var stack_category: UIView!
    @IBOutlet weak var lbl_min_price: UILabel!
    @IBOutlet weak var img_category: UIImageView!
//    @IBOutlet weak var search_bar: UISearchBar!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        fetchProducts()
        SetUp()
        addCustomizedBackBtn()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        tbl_products.refresh()
//        getProductQunatites()
        InitiateCart()
        DispatchQueue.main.async {
                for i in self.arr_Products{
                    i.quantity = getQunatityFromAppCart(ProductID: i.id!)
                    
            }
            self.tbl_products.reloadData()
                
        }
        
        AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: lbl_itemsCost)
        Update_lbl_badge()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        print("dismissed")
        delegate?.orderPad2Dismissed()
        
    }
    //    MARK: - SetUP
    func SetUp(){
        AppDelegate.sharedInstance.getMinOrderPrice(lable: lbl_min_price)
        if sender.restorationIdentifier == "ProductCatagoriesVc"{
        if category.name != "" {
//                     stack_category.isHidden = false
                     img_category.sd_setImage(with: category.img_url, completed: nil)
                     lbl_category_name.text = category.name
                }
        }

//        updateViewTitel()
        
    }
    
    
    
    
    // MARK: - Fetch Products
    func fetchProducts(){

        if internetConnectionIsOk(){
            var fetchRequest = "listing_products"
            var type = "products"
            
            
            if favouriteIsEnabled == true {
                fetchRequest = "favourite_product_list"
                type = "favourite"
            }else if specialOffersIsEnabled {
                fetchRequest = "special_offers"
            }else if fetchPromotionalProductsEnable{
                fetchRequest = "cus_listing_products"
            }else if category.name != ""{
                fetchRequest = "cus_listing_products"
            }
            
            
                        
            var userInfo = [String:AnyObject]()
            if let userinfo = UserDefaults.standard.value(forKey: "user_information") as? [String:AnyObject]{
                userInfo = userinfo
                AppDelegate.sharedInstance.userLoggedIn = true
            }
            var parameters = ["type":fetchRequest,"category_id":category.id, "office_name":office_name,"customer_id":userInfo["user_id"]  as? String ?? ""] as [String:String]
//            print(parameters)
            if fetchPromotionalProductsEnable {
                parameters = ["type":fetchRequest  , "office_name":office_name ,"slideno":slideNo ] as [String:String]
            }
            HUD.show(.progress)
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameters).responseJSON(completionHandler:{ (response) in
                                
                
                self.arr_Products = []
                switch response.result{
                case .success:
                    
                    print(parameters)
                    print(response.request?.url)
                    print(response.result.value)
                    let  dict = response.result.value as! [String:AnyObject]
                    var msgResponse = ""
                    if fetchRequest == "listing_products" || fetchRequest == "special_offers" || fetchRequest == "cus_listing_products" {
                        if let message = dict["message"] as? [AnyObject]{
                            msgResponse = message[0] as! String
                        }else {
                            msgResponse = dict["message"] as? String ?? "Server Error"
                        }
                    }else{
                        msgResponse = dict["message"] as! String
                    }
                    if  msgResponse == "Available products" || msgResponse == "Available favourites" {
                        if let data = dict["DATA"]?[type] as? [[String : AnyObject]]{
                            if fetchRequest == "listing_products"{
                                
                               /* AppDelegate.sharedInstance.appCart fetchs data from "listing_products" */
                                
                                self.arr_Products = AppDelegate.sharedInstance.appCart
                                
                            }else{
                            //                            print(AppDelegate.sharedInstance.appCart)
                            for i in 0..<data.count{
                                let PD = ProductData()
                                if let productID = data[i]["product_id"] as? String{
                                    PD.id = productID
                                    PD.quantity = getQunatityFromAppCart(ProductID: PD.id!)

                                    PD.info = data[i]["product_info"] as? String
                                    PD.info = PD.info?.withoutHtml
                                    PD.Description = data[i]["product_description"] as? String
                                    PD.Description = PD.Description?.withoutHtml
                                    PD.Stock = data[i]["product_stock"] as? String
                                    PD.title = data[i]["product_title"] as? String
                                    PD.orignalPrice = data[i]["p_market_cutoff_price"] as? String
                                    PD.discount = data[i]["p_discount_perc"] as? String
                                    PD.Ourprice = data[i]["p_sell_price"] as? String
                                    
                                    if let rating = data[i]["product_rating"] as? Int{
                                        PD.rating = String(rating)
                                    }
                                    
                                    if let int_sold = data[i]["product_sold"] as? Int{
                                        PD.sold = String(int_sold)
                                    }

                                    
                                    if fetchRequest == "favourite_product_list" {
                                        PD.favorite = "yes"
                                    }else{
                                     PD.favorite = data[i]["favorite"] as? String ?? "no"
                                    }
                                    
                                     if fetchRequest == "special_offers" {
                                        PD.discountEndDate = data[i]["discount_enddate"] as? String ?? nil
                                    }
                                    
                                    if var imgUrl = data[i]["product_image"] as? String{
                                      imgUrl = imgUrl.replaceURL()
                                    PD.imageUrl = URL(string:imgUrl)
                                    }
                                     PD.otherImgUrl = data[i]["other_img"] as? String
                                        
                                    
                                    self.arr_Products.append(PD)
                                    
                                }
                                
                                
                                }
                            }
                            self.tbl_products.isHidden = false
                            self.tbl_products.reloadData()
//                            self.getProductQunatites()
                            
                        }
                    }else{
                        DispatchQueue.main.async {
                            if dict["message"] as? String == "Any products not available" {
                                self.tbl_products.isHidden = true
                            }else{
                                appDelegate.showerror(str:dict["message"] as? String)
                            }
                        }
                    }
                case .failure(let error):
                    SCLAlertView().showError(error.localizedDescription)
                }
            })
        }
        HUD.hide()
        
    }
    //    MARK: - View Title
    func updateViewTitel(){
        if favouriteIsEnabled == true {
            self.title = "Your Favourites"
        }else if searchProductsEanble{
            
        }else if sender.title == "Categories"{
            self.title = category.name
        }else if specialOffersIsEnabled{
            self.title = "Special Offers"
        }
        else {
            self.title = "Order Pad"
        }
        
    }
    //  MARK: - TableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        if searchProductsEanble == true{
//            return arr_searchedProducts.count
//        }
        
        return arr_Products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_products.dequeueReusableCell(withIdentifier: "productcell") as! ProductCell
        var product = [ProductData]()
        cell.delegate = self
        cell.tag = indexPath.row
        
            product = arr_Products
        
//        Image
        cell.img_Product.image = UIImage(named: "loading")
        if let url = product[indexPath.row].imageUrl {
            
            cell.img_Product.sd_setImage(with: url)

        }else{
            cell.img_Product.image = UIImage(named: "loading")
        }

//        Fav
        if product[indexPath.row].favorite == "yes"{
            cell.btn_fav.tintColor = UIColor.init(hex: themeColour)
            cell.btn_fav.isSelected = true
        }else{
            cell.btn_fav.tintColor = UIColor.lightGray
            cell.btn_fav.isSelected = false
        }
        
        
//        Quantity
        if product[indexPath.row].quantity != "0" && product[indexPath.row].quantity != "" {
            print(product[indexPath.row].quantity)
            cell.lbl_quantity.isHidden = false
            cell.txt_quantity.text = product[indexPath.row].quantity
            cell.lbl_quantity.text = product[indexPath.row].quantity
        }else{
            cell.lbl_quantity.isHidden = true
            cell.txt_quantity.text = "0"
        }
        
//        Offer Ends-in
        if product[indexPath.row].discountEndDate != nil {
            cell.lbl_discountDate.text =  "Offer Ends\n" + product[indexPath.row].discountEndDate! 
        }else{
            cell.lbl_discountDate.text = ""
        }
        
//        Discount
        if let discount = product[indexPath.row].discount,
            Double(discount) ?? 0 > 0{
            cell.lbl_discount.text = "\(discount)% OFF "
            cell.lbl_discount.isHidden = false
        }else{
            cell.lbl_discount.isHidden = true
        }
        
//        ID
        cell.lbl_product_id.text = product[indexPath.row].id
        
//        Title
        cell.lbl_product_title.text = product[indexPath.row].title
        
        
//        Description
        cell.lbl_product_description.text = product[indexPath.row].Description
        
//        currency
        cell.lbl_product_our_price.text = Currency + " " + (product[indexPath.row].Ourprice ?? "0")
        
//        Orignal Price
        if let orignalPrice = product[indexPath.row].orignalPrice ,
            Int(orignalPrice) ?? 0 > 0 {
            cell.view_orignal_price.isHidden = false
            cell.lbl_product_orignal_price.text = Currency + " " + (product[indexPath.row].orignalPrice!)
        }else{
            cell.view_orignal_price.isHidden = true
        }

        return cell
    }
    
    
    
    //    MARK: - Table View Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if selectedIndex == indexPath{
//            return rowHeight
//        }
        return 200
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selected_item_for_Information = indexPath.row
        tappedProduct = arr_Products[indexPath.row]
        self.performSegue(withIdentifier: "gotoInfoVc", sender: indexPath)
        AudioServicesPlaySystemSound(1519)
//        self.tbl_products.scrollToRow(at: indexPath, at: .none , animated: true)
    }
    
    //    MARK: - Product cell delegate
    func quantityUpdated(tag: Int) {
        AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: lbl_itemsCost)
        Update_lbl_badge()
//        if searchProductsEanble{
//            arr_searchedProducts[tag].quantity = AppDelegate.sharedInstance.appCart[Row].quantity
//            arr_searchedProducts[tag].favorite = AppDelegate.sharedInstance.appCart[Row].favorite
//        }else{
            arr_Products[tag].quantity = AppDelegate.sharedInstance.appCart[Row].quantity
            arr_Products[tag].favorite = AppDelegate.sharedInstance.appCart[Row].favorite
//        }
        
    }
    
    func update_ProductID_For_AppCart(cell: UITableViewCell) {
        
        if let indexPath = tbl_products.indexPath(for: cell){

            tappedProduct = arr_Products[indexPath.row]
        
        }
//        updateRow()
        
       
    }
    
    
//        MARK: - Change Row Height
//    func changeRowHeight(at indexPath:IndexPath){
//
//        if searchProductsEanble == true{
//            ProductId = arr_searchedProducts[indexPath.row].id!
//        }
//        else{
//            ProductId = arr_Products[indexPath.row].id!
//        }
//
//        if selectedIndex == indexPath{
//            rowHeight = 188
//            tbl_products.beginUpdates()
//            tbl_products.endUpdates()
//            selectedIndex = nil
//            rowHeight = 267
//            return
//        }else{
//            selectedIndex = indexPath
//            tbl_products.beginUpdates()
//            tbl_products.endUpdates()
//        }
//    }
    
    //    MARK: - Initiate Cart
//    func InitiateCart(){
//        cart = []
//        for i in 0..<AppDelegate.sharedInstance.appCart.count{
//            if Int(AppDelegate.sharedInstance.appCart[i].quantity)! > 0 {
//                cart.append(AppDelegate.sharedInstance.appCart[i])
//            }
//        }
//    }
    //    MARK: - Button Cart
//    @IBAction func Cart(_ sender: Any) {
//        InitiateCart()
//        if cart.isEmpty {
//            appDelegate.showerror(str: "Cart is Empty")
//            return
//        }
//
//        if AppDelegate.sharedInstance.userLoggedIn{
//            performSegue(withIdentifier: "gotoCartVc", sender: self)
//        }else{
//
//            loginResquest(VC: self)
//
//        }
//
//
//    }
    
    //    MARK: - Button search bar
    //@IBAction func searchBar(_ sender: Any) {
       // search_bar.isHidden = !search_bar.isHidden
   // }
    
    //  MARK: - Side MENU
    @IBAction func menu() {
        if sender.title == "Delivery Section" || sender.title == "Categories" {
            //            let vc : UIViewController = self.storyboard?.instantiateViewController(identifier: "deliverySectionVc") as! DeliverySectionVc
            self.dismiss(animated: true, completion: nil)
            //            self.present(vc, animated: true, completion: nil)
        }else{
            if let drawer = self.drawer() ,
                let manager = drawer.getManager(direction: .left){
                let value = !manager.isShow
                drawer.showLeftSlider(isShow: value)
            }
        }
    }
    
    //    MARK: - Allow Check Out and save
    func checkOutAndSave(oderInfo:[String:String]){
        if internetConnectionIsOk(){
            let parameter = ["type":"min_price","office_name":office_name] as [String:String]
            HUD.show(.progress)
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: {(response)in
                HUD.hide()
                print(response.result.value)
                switch response.result{
                case .success:
                    let dict = response.result.value as! [String:AnyObject]
                    if dict["message"] as! String == "success"{
                        let minPrice = dict["DATA"]!["min_price"] as! [AnyObject]
                        let ComparablePrice = (oderInfo["total_price"]! as NSString).doubleValue
                        if (minPrice[0]["minimum_price"] as! NSString).doubleValue > ComparablePrice {
                            appDelegate.showerror(str: "Minimum Order price is \(Currency) \(minPrice[0]["minimum_price"] as! String)")
                        }
                        else  {
                            DataBaseHelper.sharedInstance.save(object: oderInfo)
                            DispatchQueue.main.async {
                                self.performSegue(withIdentifier: "gotoCheckOut", sender: self)
                            }
                            
                        }
                        
                    }else{
                        appDelegate.showerror(str: dict["DATA"]?["message"] as? String)
                    }
                    
                case .failure(let error):
                    appDelegate.showerror(str: error.localizedDescription)
                }
                
            })
        }
    }
//    MARK: - Get Product quntites
    func getProductQunatites(){
        DispatchQueue.main.async {
                for i in self.arr_Products{
                    i.quantity = getQunatityFromAppCart(ProductID: i.id!)
                    
            }
            self.tbl_products.reloadData()
                
        }
    }
    
//    //    MARK: - Fetch Products For App Cart
//    func fetchProductsForAppCart(){
//            
//            
//            
//            if internetConnectionIsOk(){
//                var userInfo = [String:AnyObject]()
//                if let userinfo = UserDefaults.standard.value(forKey: "user_information") as? [String:AnyObject]{
//                    userInfo = userinfo
//    //            }
//                let parameters = ["type":"listing_products","category_id": "","office_name":office_name,"customer_id":userInfo["user_id"] as? String ?? ""]
//                                        HUD.show(.progress)
//                
//                APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameters).responseJSON(completionHandler:{ (response) in
//                                                    HUD.hide()
//                    
//                
//                    switch response.result{
//                    case .success:
//                        //                                print(response.request?.url)
//                        //                                print(response.result.value)
//                        let  dict = response.result.value as! [String:AnyObject]
//                        var msgResponse = ""
//                        let message = dict["message"] as! [AnyObject]
//                        msgResponse = message[0] as! String
//                        if  msgResponse == "Available products" {
//                            if let data = dict["DATA"]?["products"] as? [[String : AnyObject]]{
//                                for i in 0..<data.count{
//                                    let PD = ProductData()
//                                    if let ProductID = data[i]["product_id"] as? String {
//                                        PD.id = ProductID
//                                        PD.info = data[i]["product_info"] as? String
//                                        PD.Description = data[i]["product_description"] as? String
//                                        PD.price = data[i]["product_price"] as? String
//                                        PD.Stock = data[i]["product_stock"] as? String
//                                        PD.discount = data[i]["product_discount"] as? String
//                                        PD.title = data[i]["product_title"] as? String
//                                        PD.favorite = (data[i]["favorite"] as? String)!
//                                        let imgUrl = data[i]["product_image"] as? String ?? ""
//                                        PD.imageUrl = URL(string:imgUrl)
//                                        AppDelegate.sharedInstance.appCart.append(PD)
//                                    }
//                                }
//                                
//                            }
//                            self.fetchProducts()
//                        }else{
//                            DispatchQueue.main.async {
//                                appDelegate.showerror(str:dict["message"] as? String)
//                            }
//                        }
//                    case .failure(let error):
//    //                    SCLAlertView().showError(error.localizedDescription)
//                        print(error)
//                    }
//                })
//            }
//        }
//        }
    
    
        
//   //     MARK: - Serach Bar Delegate
//    override func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        self.title = "Search Results:\(searchText)"
//
//        APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: ["office_name":"product","type":"search_product","search_text":searchText]).responseJSON(completionHandler: {(response) in
//            print(response.request?.url)
//            print(response.result.value)
//
//            self.arr_searchedProducts = []
//            switch response.result{
//            case .success:
//
//                let  dict = response.result.value as! [String:AnyObject]
//                if dict["message"] as! String != "No Product Found"{
//                    if let data = dict["DATA"]?["products"] as? [[String : AnyObject]]{
//                        for i in 0..<data.count{
//                            let PD = ProductData()
//                            if let productID = data[i]["product_id"] as? String{
//                                PD.id = productID
//                                PD.info = data[i]["product_info"] as? String
//                                PD.Description = data[i]["product_description"] as? String
//                                PD.price = data[i]["product_price"] as? String
//                                PD.Stock = data[i]["product_stock"] as? String
//                                PD.discount = data[i]["product_discount"] as? String
//                                PD.title = data[i]["product_title"] as? String
//                                PD.quantity = getQunatityFromAppCart(ProductID: PD.id!)
//                                //                            PD.productIndexInTable = i
//                                PD.favorite = "no"
//                                let imgUrl = data[i]["product_image"] as? String ?? ""
//                                PD.imageUrl = URL(string:imgUrl)
//
//                                self.arr_searchedProducts.append(PD)
//                            }
//                        }
//                        self.searchProductsEanble = true
//                        self.tbl_products.reloadData()
//                    }
//                }else if dict["message"] as! String == "No Product Found"{
//
//                }else {
//                    AppDelegate.sharedInstance.showerror(str: dict["message"] as? String)
//                }
//
//            case .failure(let error):
//                SCLAlertView().showError(error.localizedDescription)
//            }
//
//        })
//    }
//
//    override func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        searchBar.endEditing(true)
//        searchBar.isHidden = true
//        searchProductsEanble = false
//        arr_searchedProducts = []
//        fetchProducts()
//        updateViewTitel()
//
//    }
   func addBarBackButton(){
        if sender.title == "Categories"{
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action:#selector(backButton))
        }
    }
    
//    MARK: - Reachability
    @objc private func reachabilityChanged( notification: NSNotification )
    {
        guard let reachability = notification.object as? Reachability else
        {
            return
        }
        
        
        
        if reachability.connection == .wifi
        {
            print("Reachable via WiFi")
        }
        else if reachability.connection == .cellular
        {
            print("Reachable via Cellular")
        }
        else
        {
            print("not reachable")
        }
    }
//    MARK: - Information VC delegate
        func InfoVc_ProductQuantity_Updated(quantity: String) {
            
            arr_Products[selected_item_for_Information].quantity = quantity
            
        }
        //    MARK: - Prepare for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoInfoVc"{
            let destination = segue.destination as! InformationVc
            destination.delegate = self
//            destination.productData = AppDelegate.sharedInstance.appCart[Row]
            let indxpth = sender as! IndexPath
            destination.productData = arr_Products[indxpth.row]
            destination.PriceNProducts = AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: nil)
        }else if segue.identifier == "gotoCheckOut"{
            let destination = segue.destination as! CheckOutVc
            InitiateCart()
            destination.PriceNProducts = AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: nil)
            destination.checkOutCart = cart
        }else if segue.identifier == "gotoCartVc"{
            CartEnabled = true
        }
    }
}


