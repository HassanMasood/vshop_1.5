//
//  SettingVc.swift
//  Food Service
//
//  Created by Hassan  on 09/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import SCLAlertView

class SettingVc: CommonVc {

    @IBOutlet weak var switch_touch_id: UISwitch!
    @IBOutlet weak var btn_update_choice: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
    }
    
//MARK: - SetUP UI
    func setupUI(){
        setupButtonUI(for: btn_update_choice)
        if let dict = UserDefaults.standard.value(forKey: "Login_Info") as? [String:String]{
            if dict["touchID"] == "notAvailable" || dict["touchID"] == "disabled"{
                switch_touch_id.setOn(false, animated: true)
                
            }
            
        }
    }
    
//    MARK: - Button Side Menu
    @IBAction func sideMenu(_ sender: Any) {
        if let drawer = self.drawer() ,
            let manager = drawer.getManager(direction: .left){
            let value = !manager.isShow
            drawer.showLeftSlider(isShow: value)
        }
        
    }
    
//    MARK: - Switch Touch ID
    @IBAction func touchID(_ sender: UISwitch) {
        
        var loginInfo = [String:String]()
        if let dict = UserDefaults.standard.value(forKey: "Login_Info") as? [String:String]{
            loginInfo = dict
            debugPrint(loginInfo)
            print("aasasas")
        }
        
        if switch_touch_id.isOn {
            if checkAuth(){
                AppDelegate.sharedInstance.showSuccess(str: "Touch ID Enabled")
                let dict2 = ["email":loginInfo["email"],"password":loginInfo["password"],"touchID":"enabled","rememberMe":loginInfo["rememberMe"]]
                UserDefaults.standard.set(dict2, forKey: "Login_Info")
            }else{
                appDelegate.showerror(str: "Touch ID not available")
                let dict2 = ["email":loginInfo["email"],"password":loginInfo["password"],"touchID":"notAvailable","rememberMe":loginInfo["rememberMe"]]
                UserDefaults.standard.set(dict2, forKey: "Login_Info")
                switch_touch_id.setOn(false, animated: true)
            }
            
        }else{
            SCLAlertView().showCustom(AppName,subTitle: "Touch ID disabled!",color: .gray)
            let dict2 = ["email":loginInfo["email"],"password":loginInfo["password"],"touchID":"disabled","rememberMe":loginInfo["rememberMe"]]
            
            UserDefaults.standard.set(dict2, forKey: "Login_Info")
            print(UserDefaults.standard.value(forKey: "Login_Info"))
        }
    }
    
}
