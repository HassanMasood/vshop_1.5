//
//  RegisterVc.swift
//  Food Service
//
//  Created by Hassan  on 08/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit

class RegisterVc: CommonVc {

    @IBOutlet weak var btn_Personal: UIButton!
    @IBOutlet weak var btn_Business: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    func setupUI(){
        setupButtonUI(for: btn_Business)
        setupButtonUI(for: btn_Personal)
    }

}
