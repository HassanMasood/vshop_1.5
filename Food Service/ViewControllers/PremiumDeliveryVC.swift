//
//  PremiumDeliveryVC.swift
//  Food Service
//
//  Created by Hassan  on 05/10/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import SCLAlertView
//import iOSDropDown
import PKHUD
import DropDown

class PremiumDeliveryVC: CommonVC2,UITextViewDelegate,UITextFieldDelegate{
    
    @IBOutlet weak var lbl_placeHolder : UILabel!
    @IBOutlet weak var txt_view_note : UITextView!
    @IBOutlet weak var txt_name : UITextField!
    @IBOutlet weak var txt_mobile : UITextField!
    @IBOutlet weak var txt_pickup_address : UITextField!
    @IBOutlet weak var txt_delivery_address : UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_pickUp_email : UITextField!
    @IBOutlet weak var txt_pickUp_mobile : UITextField!
    @IBOutlet weak var txt_pickUp_point_name : UITextField!
    
    @IBOutlet weak var btn_dropDown_pickUp_address : UIButton!
    @IBOutlet weak var btn_dropDown_delivery_address : UIButton!
    
    @IBOutlet weak var txt_vehicle : UITextField!
    
    let pickUp_address_DropDown = DropDown()
    let delivery_address_DropDown = DropDown()
    let vehicle_dropDown = DropDown()
    

    
    var vehicle = [Vehicle]()
    
/// Boolean value to stop getting more prediction from Google Places , afer your selects his desired address
    var shouldGetPredictions : Bool = true
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        addCustomBackButton()
        //        dropDown_Vehicle.optionArray = ["Bike","Car","Truck"]
        if AppDelegate.sharedInstance.userLoggedIn{
            setUp()
            
        }else{
            loginResquest(VC: self , nextViewController: nil)
        }
        setupDropDown()
        didSelectRow_DropDown()
        //        dropDown_Vehicle.isSearchEnable = false
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if AppDelegate.sharedInstance.userLoggedIn{
            setUp()            
        }
        fetchVehicles()
    }
    
    //    MARK: - Text Filed Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.layer.borderWidth = 0
        txt_vehicle.layer.borderWidth = 0
        txt_pickup_address.layer.borderWidth = 0
        
        
        
    }
    
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
//        if textField == txt_pickUp_mobile || textField == txt_mobile{
//            if !isValidPhoneNumber(phoneNumber: textField.text!) {
//                textField.layer.borderColor = UIColor.red.cgColor
//                textField.layer.borderWidth = 1
//            }else{
//                textField.layer.borderWidth = 0
//            }
//
//        }
        
    
        if textField == txt_pickup_address || textField == txt_delivery_address{
            if textField.text!.count > 2{
                getAddressPredictions(address: textField.text!, dropDown: (textField == txt_pickup_address ? pickUp_address_DropDown : delivery_address_DropDown))
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == txt_email {
            if isValidEmail(emailID: textField.text!) == false {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1
            }
        }else if textField == txt_pickUp_email {
            if isValidEmail(emailID: txt_pickUp_email.text!) == false {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1
            }
            
        }
        
    }
    
    func DropDownDidSelectRow(){
        //        dropDown_Vehicle.didSelect { (type, index, id) in
        //
        //            for i in self.vehicle {
        //                if type == i.type{
        //                    premiumDelivery.sharedInstance.vehicle = i
        //                }
        //            }
        //        }
        
        
        
        
    }
    
    //    MARK: - Set up
    func setUp(){
        
        if let dict = UserDefaults.standard.value(forKey: "user_information") as? [String:AnyObject]{
            txt_name.text = dict["name"] as? String ?? ""
            txt_mobile.text = dict["mobile"] as? String ?? ""
            txt_delivery_address.text = (dict["address"] as? String ?? "") + "," + (dict["town"] as? String ?? "")
            txt_email.text = dict["email"] as? String ?? ""
            premiumDelivery.sharedInstance.user_id = dict["user_id"] as! String
        }
        
        //        txt_pickup_address.selectedRowColor = .white
        //        txt_pickup_address.arrowColor = .white
        //        txt_pickup_address.rowHeight = 50
        //        txt_pickup_address.checkMarkEnabled = false
    }
    
    
    // MARK: - Button Order Now Tapped
    @IBAction func btn_order_now_tapped(){
        if AppDelegate.sharedInstance.userLoggedIn{
            
            let check = validation()
            if check.isValid{
                UpdatePremiumDeliveryData()
                self.performSegue(withIdentifier: "gotoCheckOutVc", sender: self)
                
            }else{
                appDelegate.showerror(str: check.error)
                
            }
        }else{
            loginResquest(VC: self,nextViewController: nil)
        }
    }
    
    //    MARK: - Update Premium Delivery Data
    func UpdatePremiumDeliveryData(){
        
        if let user_first_lst_name = txt_name.text?.components(separatedBy: " "),
            user_first_lst_name.count == 2{
            premiumDelivery.sharedInstance.first_name = user_first_lst_name[0]
            premiumDelivery.sharedInstance.last_name = user_first_lst_name[1]
        }else{
            premiumDelivery.sharedInstance.first_name = txt_name.text!
            premiumDelivery.sharedInstance.last_name = txt_name.text!
        }
        premiumDelivery.sharedInstance.mobile = txt_mobile.text!
        premiumDelivery.sharedInstance.destination = txt_delivery_address.text!
        //        premiumDelivery.sharedInstance.pickup_point_name = txt_pickup_address.text!
        premiumDelivery.sharedInstance.pickup_point_name = ""
        premiumDelivery.sharedInstance.email = txt_email.text!
        //        premiumDelivery.sharedInstance.pickup_email = txt_pickUp_email.text!
        premiumDelivery.sharedInstance.pickup_email = ""
        premiumDelivery.sharedInstance.pickup_street_address = txt_pickup_address.text!
        premiumDelivery.sharedInstance.pickup_phone = txt_pickUp_mobile.text!
    }
    
    
    //    ------- validation -----------
    func validation()->(error:String,isValid:Bool){
        
        
        if txt_name.text!.isEmpty{
            txt_name.layer.borderColor = UIColor.red.cgColor
            txt_name.layer.borderWidth = 1
            return ("Name can not be empty.",false)
        }
        else if txt_mobile.text!.isEmpty{
            txt_mobile.layer.borderColor = UIColor.red.cgColor
            txt_mobile.layer.borderWidth = 1
            return ("Mobile can not be empty.",false)
        }
        else if txt_email.text!.isEmpty{
            txt_email.layer.borderColor = UIColor.red.cgColor
            txt_email.layer.borderWidth = 1
            return ("Email can not be empty.",false)
        }
        else if txt_pickup_address.text!.isEmpty{
            txt_pickup_address.layer.borderColor = UIColor.red.cgColor
            txt_pickup_address.layer.borderWidth = 1
            return ("Pickup address can not be empty.",false)
        }
        else if txt_delivery_address.text!.isEmpty{
            txt_delivery_address.layer.borderColor = UIColor.red.cgColor
            txt_delivery_address.layer.borderWidth = 1
            return ("Pickup address can not be empty.",false)
        }
        else if txt_vehicle.text!.isEmpty{
            txt_vehicle.layer.borderColor = UIColor.red.cgColor
            txt_vehicle.layer.borderWidth = 1
            return ("Please select vehicle.",false)
        }
        else if txt_pickUp_mobile.text!.isEmpty{
            txt_pickUp_mobile.layer.borderColor = UIColor.red.cgColor
            txt_pickUp_mobile.layer.borderWidth = 1
            return ("Pickup mobile can not be empty.",false)
        }
        //        if txt_name.text!.components(separatedBy: " ").count < 2{
        //            txt_name.layer.borderColor = UIColor.red.cgColor
        //            txt_name.layer.borderWidth = 1
        //            return ("Please enter your last name.",false)
        //        }
        if isValidEmail(emailID: txt_email.text!) == false {
            txt_email.layer.borderColor = UIColor.red.cgColor
            txt_email.layer.borderWidth = 1
            return ("Please enter valid email address.",false)
        }
        
        
        /// phone number validation
//        if isValidPhoneNumber(phoneNumber: txt_mobile.text!){
//            txt_mobile.layer.borderColor = UIColor.red.cgColor
//            txt_mobile.layer.borderWidth = 1
//            return ("Please enter valid mobile number.",false)
//        }
//        if isValidPhoneNumber(phoneNumber: txt_pickUp_mobile.text!){
//            txt_pickUp_mobile.layer.borderColor = UIColor.red.cgColor
//            txt_pickUp_mobile.layer.borderWidth = 1
//            return ("Please enter valid mobile number.",false)
//        }
        
        
        //         if txt_pickUp_point_name.text!.isEmpty{
        //            txt_pickUp_point_name.layer.borderColor = UIColor.red.cgColor
        //            txt_pickUp_point_name.layer.borderWidth = 1
        //            return ("Pickup point can not be empty.",false)
        //        }
        
        
        
        
        //        else if txt_pickUp_email.text!.isEmpty{
        //            txt_pickUp_email.layer.borderColor = UIColor.red.cgColor
        //            txt_pickUp_email.layer.borderWidth = 1
        //            return ("Pickup email can not be empty.",false)
        //        }
        
        
        
        
        //        else if isValidEmail(emailID: txt_pickUp_email.text!) == false {
        //                txt_pickUp_email.layer.borderColor = UIColor.red.cgColor
        //                txt_pickUp_email.layer.borderWidth = 1
        //            return ("Please enter valid email address.",false)
        //            }
        
        
        
        
        return ("",true)
    }
    //   ----  Check Email Validation ----
    func isValidEmail(emailID:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailID)
    }
    //    ---- check Phone number ----
    func isValidPhoneNumber(phoneNumber:String) -> Bool{
        let PhoneRegEx = "\\A[0-9]{10}\\z"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PhoneRegEx)
        return phoneTest.evaluate(with: phoneNumber)
    }
    //    MARK:- Text View Note Delegate
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            lbl_placeHolder.isHidden = false
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        lbl_placeHolder.isHidden = true
        
        
    }
    
    
    //    MARK: - fetch Vehicles
    func fetchVehicles(){
        if internetConnectionIsOk() {
            //            http://vshoplanka.com/taxi_app/WebServices/vshopcustomer.php?office_name=vshop&type=vehicle_type
            
            let param = ["office_name":office_name, "type":"vehicle_type"] as [String:String]
            
            
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: param).responseJSON(completionHandler: {(response) in
                
                print(response.request?.url)
                print(response.result.value)
                
                switch response.result{
                case .success:
                    
                    if let dict = response.result.value as? [String:AnyObject]{
                        let data = dict["DATA"] as! [[String:AnyObject]]
                        if data[0]["msg"] as! String == "Vehicle Type"{
                            
                            self.vehicle = []
                            //                            self.dropDown_Vehicle.optionArray = []
                            self.vehicle_dropDown.dataSource = []
                            
                            for i in 1..<data.count{
                                var vehicleData = Vehicle()
                                vehicleData.type = data[i]["type"] as! String
                                vehicleData.id = data[i]["id"] as! String
                                vehicleData.price = data[i]["standard_delivery_charge"] as! String
                                self.vehicle.append(vehicleData)
                            }
                            
                            for i in self.vehicle{
                                //                                self.dropDown_Vehicle.optionArray.append(i.type)
                                self.vehicle_dropDown.dataSource.append(i.type)
                                
                                
                            }
                            
                            
                        }else{
                            appDelegate.showerror(str: data[0]["msg"] as? String ?? "Sever Error")
                        }
                        
                    }
                    
                case .failure(let error):
                    appDelegate.showerror(str: error.localizedDescription)
                }
                
                //                {"RESULT":"OK","DATA":[{"msg":"Vehicle Type"},{"id":"5","type":"Motor Bike","standard_delivery_charge":"0"},{"id":"6","type":"Van","standard_delivery_charge":"0"},{"id":"7","type":"Truck","standard_delivery_charge":"0"},{"id":"8","type":"Three Wheeler","standard_delivery_charge":"0"}]}
                
            })
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoCheckOutVc"{
            let dest = segue.destination as! CheckOutVc
            dest.isPremiumDelivery = true
        }
    }
    
//    MARK: - Google Places
    func getAddressPredictions(address:String,dropDown:DropDown){
        let Address = address.replacingOccurrences(of: " ", with: "")
        print(Address)
        let LocationBaseURL = "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyCblnmLosUiZfHXZIKldbzIC9hroN80GvY&inputtype=textquery&components=country:" + countryCode + "&input=" + Address
        
        dropDown.dataSource = []
        APIManager.sharedInstance.sessionManager.request(LocationBaseURL).responseJSON(completionHandler: {(response)
            in
            
            switch response.result{
            case .success:
                if let dict = response.result.value as? [String:AnyObject]{
                    //                    self.txt_pickup_address.optionArray = [String]()
                    if dict["status"] as! String == "OK"{
                        let placesInfo = dict["predictions"] as! [[String:AnyObject]]
                        for i in 0..<placesInfo.count{
                            if let placeName = placesInfo[i]["description"] as? String{
                                dropDown.dataSource.append(placeName)
                            }
                        }
                        
                        if self.shouldGetPredictions{
                        dropDown.show()
                        }
                        self.shouldGetPredictions = true
                        
                    }else{
                        
                        if self.shouldGetPredictions == false{
                            self.shouldGetPredictions = true
                        }
                        
                    }
                }
                
                
            case .failure(let error):
                print("Error fetching places ::> \(error) " )
                dropDown.hide()
            }
        })
        
        dropDown.hide()
    }
    
    @IBAction func vehicle_dropDown_pressed(_ sender:UIButton){
        vehicle_dropDown.show()
    }
    
    
    
    func setupDropDown(){
        
        pickUp_address_DropDown.anchorView = txt_pickup_address
        pickUp_address_DropDown.topOffset = CGPoint(x:0,y: -(txt_pickup_address.bounds.height))
        pickUp_address_DropDown.direction = .top
        pickUp_address_DropDown.selectionBackgroundColor = UIColor.init(hex: themeColour)!
        
        
        
        delivery_address_DropDown.anchorView = txt_delivery_address
        delivery_address_DropDown.topOffset = CGPoint(x:0,y: -(txt_delivery_address.bounds.height))
        delivery_address_DropDown.direction = .top
        delivery_address_DropDown.selectionBackgroundColor = UIColor.init(hex: themeColour)!
        
        
        vehicle_dropDown.anchorView = txt_vehicle
        vehicle_dropDown.bottomOffset = CGPoint(x:0,y: txt_vehicle.bounds.height)
        vehicle_dropDown.direction = .bottom
        vehicle_dropDown.selectionBackgroundColor = UIColor.init(hex: themeColour)!
    }
    
    
    func didSelectRow_DropDown(){
        pickUp_address_DropDown.selectionAction = { (index, item) in
            //            print("\(item) + \(index)")
            self.txt_pickup_address.text = item
            self.shouldGetPredictions = false
        }
        delivery_address_DropDown.selectionAction = { (index, item) in
            //            print("\(item) + \(index)")
            self.txt_delivery_address.text = item
            self.shouldGetPredictions = false
        }
        vehicle_dropDown.selectionAction = { (index, item) in
            //            print("\(item) + \(index)")
            self.txt_vehicle.text = item
            for i in self.vehicle {
                if item == i.type{
                    premiumDelivery.sharedInstance.vehicle = i
                }
            }
            
        }
    }
    
    
}



