//
//  InformationVc.swift
//  Food Service
//
//  Created by Hassan  on 04/03/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import SCLAlertView
import PKHUD
import AudioToolbox
import SDWebImage

protocol informationVCDelegate : class {
    func InfoVc_ProductQuantity_Updated(quantity:String)
}


class InformationVc: CommonVc, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource , productCellDelegate , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {

    
    weak var delegate : informationVCDelegate?
    
//    @IBOutlet weak var lbl_Id: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
//    @IBOutlet weak var img_product: UIImageView!
    @IBOutlet weak var lbl_details: UILabel!
    @IBOutlet weak var lbl_orignal_price: UILabel!
    @IBOutlet weak var lbl_our_price: UILabel!
    @IBOutlet weak var btn_plus: UIButton!
    @IBOutlet weak var btn_minus: UIButton!
    @IBOutlet weak var btn_fav: UIButton!
    @IBOutlet weak var txt_quantity: UITextField!
    @IBOutlet weak var lbl_quantity: UILabel!
    @IBOutlet weak var lbl_itemsCost: UILabel!
    @IBOutlet weak var lbl_min_price: UILabel!
    @IBOutlet weak var lbl_stock : UILabel!
    @IBOutlet weak var lbl_discount : UILabel!
    @IBOutlet weak var tbl_recommend_products: UITableView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var scroll_product_details: UIScrollView!
    @IBOutlet weak var view_details: UIView!
    @IBOutlet weak var view_product: UIView!
    @IBOutlet weak var view_products_not_available: UIView!
    @IBOutlet weak var view_product_total_price: UIView!
    @IBOutlet weak var lbl_info: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var collection_view_imgs: UICollectionView!
    
    @IBOutlet weak var stack_images: UIStackView!
    @IBOutlet weak var img_view_1: UIImageView!
    @IBOutlet weak var img_view_2: UIImageView!
    @IBOutlet weak var img_view_3: UIImageView!
    @IBOutlet weak var btn_image_1: UIButton!
    @IBOutlet weak var btn_image_2: UIButton!
    @IBOutlet weak var btn_image_3: UIButton!
    @IBOutlet weak var view_image_1: UIView!
    @IBOutlet weak var view_image_2: UIView!
    @IBOutlet weak var view_image_3: UIView!
    
    @IBOutlet weak var btn_addToCart : UIButton!
    @IBOutlet weak var btn_buyNow : UIButton!
    @IBOutlet weak var lbl_product_total_Price : UILabel!
    
    
    
//    @IBOutlet weak var View_orignal_price: UIView!
    
    var arr_downloaded_product_images = [UIImage](){
        didSet{
            if arr_downloaded_product_images.count == 1 {
                img_view_1.image = arr_downloaded_product_images[0]
                
                view_image_1.isHidden = true
                SetBorder(for: view_image_1)
            }else if arr_downloaded_product_images.count == 2{
                img_view_2.image = arr_downloaded_product_images[1]
                SetBorder(for: view_image_2)
                view_image_1.isHidden = false
                view_image_2.isHidden = false
            }else if arr_downloaded_product_images.count == 3{
                img_view_3.image = arr_downloaded_product_images[2]
                
                view_image_3.isHidden = false
                SetBorder(for: view_image_3)
            }
            collection_view_imgs.reloadData()
        }
    }
    
    var rowHeight : CGFloat = 267
    var productData = ProductData()
    var PriceNProducts = [String:Any]()
    var arr_product_img_url = [URL]()
//    var recomendedProducts = [ProductData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Information"
        txt_quantity.delegate = self
//        setupUI()
        
        fetchProductDetails()
        CartEnabled = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupUI()
        CartEnabled = false
        fetchRecommendedPrdoducts()
        tbl_recommend_products.refresh()
        AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: lbl_itemsCost)
        Update_lbl_badge()
    }
    
    //    MARK:- Product cell Delegate
    func quantityUpdated(tag: Int) {
        AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: lbl_itemsCost)
//        Update_lbl_badge()
        if recommendedProductsIsEnabled{
        recomendedProducts[tag].quantity = AppDelegate.sharedInstance.appCart[Row].quantity
        recomendedProducts[tag].favorite = AppDelegate.sharedInstance.appCart[Row].favorite
    }
    }
    
    func update_ProductID_For_AppCart(cell: UITableViewCell) {
           if let indexPath = tbl_recommend_products.indexPath(for: cell){
                      tappedProduct = recomendedProducts[indexPath.row]

                  }
       }
    
    func productCellInfoButtonPressed(tag: Int) {
        
    }
    //    MARK: - Setup UI
    func setupUI(){
        
        tappedProduct = productData
//        updateRow()
        setupButtonUI(for: btn_addToCart)
        setupButtonUI(for: btn_buyNow)
        
        btn_buyNow.layer.borderColor = UIColor(hex: themeColour)?.cgColor
        btn_buyNow.layer.borderWidth = 1
        
        lbl_quantity.text = AppDelegate.sharedInstance.appCart[Row].quantity
        txt_quantity.text = AppDelegate.sharedInstance.appCart[Row].quantity
        lbl_our_price.text =  Currency + " " + (productData.Ourprice!)
        
        
//        lbl_Id.text = productData.id
        if lbl_quantity.text == "0"{
            lbl_quantity.isHidden = true
        }
        lbl_details.text = productData.info
        lbl_title.text = productData.title
        
        lbl_quantity.layer.cornerRadius = lbl_quantity.frame.size.height / 2
        lbl_quantity.layer.masksToBounds = true
        
        lbl_discount.layer.cornerRadius = lbl_quantity.frame.size.height / 2
        lbl_discount.layer.masksToBounds = true
        
        lbl_product_total_Price.layer.cornerRadius = lbl_product_total_Price.frame.size.height / 2
        lbl_quantity.layer.masksToBounds = true
        
        if let OPrice = productData.orignalPrice,
            OPrice != "0" {
            lbl_orignal_price.text =   Currency + " " + OPrice
        }else{
            lbl_orignal_price.isHidden = true
//            stack_orignal_price.isHidden = true
        }
        
        if let discout = productData.discount,
            Double(discout) ?? 0 > 0{
            lbl_discount.text = "  " + discout + "% OFF  "
        }else{
            lbl_discount.isHidden = true
        }
        
        
       if let stock = productData.Stock ,
        Int(stock) ?? 0 > 0{
        print(stock)
        lbl_stock.text = "Available"
        lbl_stock.textColor = .systemGreen
       }
        
//        if let url = productData.imageUrl{
//            img_product.af_setImage(withURL: url)
//        }else{
//            img_product.image = UIImage(named: "image")
//        }
        
        
        if productData.favorite == "yes"{
            btn_fav.isSelected = true
        }else{
            btn_fav.isSelected = false
            btn_fav.tintColor = UIColor.lightGray
        }
        AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: lbl_itemsCost)
//        Update_lbl_badge()
        
//        lbl_itemsCost.text = "\(PriceNProducts["total_items"] as! String) items of \(PriceNProducts["total_products"] as! String) Products / \(Currency)\(PriceNProducts["total_price"] as! String)"
        lbl_min_price.text = minOrderPRICE

        updateProdctPriceLbl()
        
        setUPProductImages()
        
              view_product.isHidden = false
              scroll_product_details.isHidden = false
              recommendedProductsIsEnabled = false
              view_details.isHidden = true
              tbl_recommend_products.isHidden = true
        
        
    }
    
    // MARK: - Button add to basket pressed
    @IBAction func btn_addTopCart_pressed(){
        
        var qunatity = Int(lbl_quantity.text!) ?? 0
        
//        if qunatity! > 0{
//            updateProductQunatity(sender: nil, Quantity: String(qunatity!))
//        }else{
//            updateProductQunatity(sender: nil, Quantity: "1")
        
//        }
        qunatity += 1
        updateProductQunatity(sender: nil, Quantity: String(qunatity))
        updateProdctPriceLbl()
        
    }
//    MARK: - Button Buy Now
    @IBAction func btn_buyNow_pressed(){
        InitiateCart()
        if cart.isEmpty{
            return
        }
        self.performSegue(withIdentifier: "gotoCartVc", sender: self)
        
    }
//    MARK: - Get Product Total Price
    func updateProdctPriceLbl(){
        
        let quantity = Double(lbl_quantity.text!) ?? 0.00
        var totalPrice = 0.00
        if let discount = Double(productData.discount!),
            discount > 0 {
            
            totalPrice += ( Double(productData.Ourprice ?? "0.00")! - ( (Double(productData.Ourprice ?? "0.00")! * discount ) / 100.0 ) ) * quantity
        }else{
            totalPrice = ( Double(productData.Ourprice ?? "0.00")! ) * quantity
        }
       totalPrice = totalPrice.rounded(toPlaces: 2)
        if totalPrice > 0 {
//            lbl_product_total_Price.isHidden = false
            view_product_total_price.isHidden = false
        lbl_product_total_Price.text =  Currency + " " + String(totalPrice) 
        }else{
            view_product_total_price.isHidden = true
        }
        
        if quantity == 0 {
            updateProductQunatity(sender: nil, Quantity: "0")
        }
        
    }
    
    
//    MARK: - setup Product Images
    func setUPProductImages(){
        
        
        
        var arr_images = [URL]()

        if let url = productData.imageUrl {
        arr_images.append(url)
        }
        
        if productData.otherImgUrl != nil{
            
            if let images = productData.otherImgUrl?.components(separatedBy: ","){
                
//                print("images comma seprated\(images)")
                
                for i in images{
                    if let image_url = URL(string:i){
                        arr_images.append(image_url)
                    }
                }
//                print("total urls we get = \(arr_images)")
            }
        }
            
      

        arr_downloaded_product_images = []
        for i in arr_images{
            SDWebImageManager.shared.loadImage(with: i, options: .highPriority, progress: .none) { (image, data, error, type, true, url) in
                if error == nil{
                    if image != nil {
                    self.arr_downloaded_product_images.append(image!)
                    }
                }
            }
        }
   
 
        
    }
    
//    MARK: - set Border
    func SetBorder (for view:UIView ){
    
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.orange.cgColor
        
        
    }
    //   MARK: - Button Fav
    @IBAction func fav(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        updateFav(sender)
    }
    
    
    //    MARK: - Table View Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recomendedProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productcell") as! ProductCell
        
        cell.delegate = self
        cell.tag = indexPath.row
        
        cell.img_Product.image = UIImage(named: "loading")
        if let url = recomendedProducts[indexPath.row].imageUrl {
            
            cell.img_Product.sd_setImage(with: url)

        }else{
            cell.img_Product.image = UIImage(named: "loading")
        }
  
        if recomendedProducts[indexPath.row].favorite == "yes"{
            cell.btn_fav.tintColor = UIColor.init(hex: themeColour)
            cell.btn_fav.isSelected = true
        }else{
            cell.btn_fav.tintColor = UIColor.lightGray
            cell.btn_fav.isSelected = false
        }
        
        if let discount = recomendedProducts[indexPath.row].discount,
            Double(discount) ?? 0 > 0{
            cell.lbl_discount.text = "\(discount)% OFF "
            cell.lbl_discount.isHidden = false
        }else{
            cell.lbl_discount.isHidden = true
        }

        
        
        if recomendedProducts[indexPath.row].quantity != "0" {
            cell.lbl_quantity.isHidden = false
            cell.txt_quantity.text = recomendedProducts[indexPath.row].quantity
            cell.lbl_quantity.text = recomendedProducts[indexPath.row].quantity
        }else{
            cell.lbl_quantity.isHidden = true
            cell.txt_quantity.text = "0"
        }

        cell.lbl_product_id.text = recomendedProducts[indexPath.row].id
        cell.lbl_product_title.text = recomendedProducts[indexPath.row].title
        cell.lbl_product_description.text = recomendedProducts[indexPath.row].Description
        
        
        cell.lbl_product_our_price.text = Currency + " " + (recomendedProducts[indexPath.row].Ourprice ?? "0")
        
        if let orignalPrice = recomendedProducts[indexPath.row].orignalPrice ,
            Int(orignalPrice) ?? 0 > 0 {
            cell.view_orignal_price.isHidden = false
            cell.lbl_product_orignal_price.text = Currency + " " + (recomendedProducts[indexPath.row].orignalPrice!)
        }else{
            cell.view_orignal_price.isHidden = true
        }

        
        return cell
        
    }
    
    //    MARK: - Table View Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AudioServicesPlaySystemSound(1519)
        segment.selectedSegmentIndex = 0
        tappedProduct = recomendedProducts[indexPath.row]
        productData = recomendedProducts[indexPath.row]
        fetchProductDetails()
        fetchRecommendedPrdoducts()
        setupUI()
        
        

//        tbl_recommend_products.scrollToRow(at: indexPath, at: .none , animated: true)

    }
    //    MARK: - Change Row Height
//    func changeRowHeight(at indexPath:IndexPath){
//
//           AudioServicesPlaySystemSound(1519)
//            ProductId = recomendedProducts[indexPath.row].id!
//
//        if selectedIndex == indexPath{
//            rowHeight = 200
//            tbl_recommend_products.beginUpdates()
////            tbl_recommend_products.reloadRows(at: [selectedIndex!], with: .none)
//            tbl_recommend_products.endUpdates()
//            selectedIndex = nil
//            rowHeight = 267
//            return
//        }
//        selectedIndex = indexPath
//        tbl_recommend_products.beginUpdates()
////        tbl_recommend_products.reloadRows(at: [selectedIndex!], with: .none)
//        tbl_recommend_products.endUpdates()
//
//    }
    //    MARK: - Update Fav
    func updateFav(_ sender:UIButton){
        if AppDelegate.sharedInstance.userLoggedIn{
        var fav_tpye = ""
        if sender.isSelected == true {
            fav_tpye = "yes"
        }else{
            fav_tpye = "no"
        }
        
        let dict = UserDefaults.standard.value(forKey:"user_information") as? [String:AnyObject]
            let parameter : [String:String] = ["type":"save_favourite_product","favourite_type":fav_tpye,"customer_id":dict!["user_id"] as? String ?? "" ,"product_id":productData.id!,"office_name":office_name] 
        APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: { (response) in
            switch response.result{
            case .success:
                let data = response.result.value as! [String:AnyObject]
                if fav_tpye == "yes"{
                    if data["message"] as! String == "saved"{
                        self.btn_fav.tintColor = UIColor.init(hex: themeColour)
                        appDelegate.showSuccess(str: "Saved in favourites")
                        AppDelegate.sharedInstance.appCart[Row].favorite = "yes"
                    }else{
                        SCLAlertView().showError(data["message"] as! String)
                    }
                }else if fav_tpye == "no"{
                    self.btn_fav.tintColor = UIColor.lightGray
                    AppDelegate.sharedInstance.appCart[Row].favorite = "no"
                }
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        }else{
            loginResquest(VC: self , nextViewController: nil )
        }
    }
    
    
//    MARK: - Collection View Data Source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arr_downloaded_product_images.count
//        return arr_product_img_url.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "promoCell", for: indexPath) as! PromotionalCell
        let imgView : UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
//        imgView.sd_setImage(with: arr_product_img_url[indexPath.row])
        imgView.image = nil
        imgView.image = arr_downloaded_product_images[indexPath.row]
        cell.addSubview(imgView)
//        cell.promo_img_view.af_setImage(withURL: arr_product_img_url[indexPath.row])
        
        return cell
    }
    //    MARK: - Collection View Delegate
    
//    scrollviewdidend
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat(300), height: CGFloat(300))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    //    MARK: - Buttons Plus & Minus Pressed
    @IBAction func update_qunatity(_ sender: UIButton) {
        
//        updateProductQunatity(sender: sender, Quantity: nil)
        
//        var quantity = Int(AppDelegate.sharedInstance.appCart[Row].quantity) ?? 0
        var quantity = Int(lbl_quantity.text!) ?? 0
                
                    if sender.tag == 0 {
                        quantity += 1
                    }else{
                        if quantity > 0{
                            quantity -= 1
                        }
                    }
                
                if quantity > 0{
                    // check Product available Stock
                    if let Stock = AppDelegate.sharedInstance.appCart[Row].Stock{
                        let  intStock = Int(Stock)
                        if (intStock! + 1) > quantity{
//                            AppDelegate.sharedInstance.appCart[Row].quantity = String(quantity)
        //                    updateAppCart(productID: arr_Products[Row].id!, qunatity: arr_Products[Row].quantity)
        //                    print(arr_Products[Row].quantity)
                            updateProductQunatity(sender: nil, Quantity: String(quantity))
                            lbl_quantity.text = String(quantity)
                            txt_quantity.text = String(quantity)
                            lbl_quantity.isHidden = false
                        }else{
                            if  intStock == 0 || intStock! < 0  {
                                appDelegate.showerror(str: "Product out of stock today")
                            }else{
                                appDelegate.showerror(str: "Maximum allowed quantity is \(intStock!) today")
                            }
                        }
                    }
                }else{
                    
                    lbl_quantity.text = "0"
                    lbl_quantity.isHidden = true
                    txt_quantity.text = "0"
                    AppDelegate.sharedInstance.appCart[Row].quantity = "0"
                }
             updateProdctPriceLbl()
//                AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: lbl_itemsCost)
//                delegate?.InfoVc_ProductQuantity_Updated(quantity: AppDelegate.sharedInstance.appCart[Row].quantity)
//                Update_lbl_badge()
        
//        updateProductQunatity(sender: nil, Quantity: lbl_quantity.text)
        
        
        
    }
    //    MARK: - Initiate Cart
    override func InitiateCart(){
        cart = []
        for i in 0..<AppDelegate.sharedInstance.appCart.count{
            if Int(AppDelegate.sharedInstance.appCart[i].quantity) ?? 0 > 0 {
                cart.append(AppDelegate.sharedInstance.appCart[i])
            }
        }
    }
    //    MARK: - Button Cart
    @IBAction func Cart(_ sender: Any) {
        InitiateCart()
        if cart.isEmpty {
            appDelegate.showerror(str: "Cart is Empty")
            return
        }
        performSegue(withIdentifier: "gotoCartVc", sender: self)
    }
    //    MARK: - Update Product Quantity
    func updateProductQunatity(sender:AnyObject?,Quantity:String?){
//        ProductId = productData.id!
//        updateRow()
        var quantity = Int(AppDelegate.sharedInstance.appCart[Row].quantity) ?? 0
        
        if sender != nil{
            if sender?.tag == 0 {
                quantity += 1
            }else{
                if quantity > 0{
                    quantity -= 1
                }
            }
        }else{
            if Quantity != ""{
                quantity = Int(Quantity!)!
            }else{
                quantity = 0
            }
        }
        if quantity > 0{
            // check Product available Stock
            if let Stock = AppDelegate.sharedInstance.appCart[Row].Stock{
                let  intStock = Int(Stock)
                if (intStock! + 1) > quantity{
                    AppDelegate.sharedInstance.appCart[Row].quantity = String(quantity)
//                    updateAppCart(productID: arr_Products[Row].id!, qunatity: arr_Products[Row].quantity)
//                    print(arr_Products[Row].quantity)
                    
                    lbl_quantity.text = String(quantity)
                    txt_quantity.text = String(quantity)
                    lbl_quantity.isHidden = false
                }else{
                    if  intStock == 0 || intStock! < 0  {
                        appDelegate.showerror(str: "Product out of stock today")
                    }else{
                        appDelegate.showerror(str: "Maximum allowed quantity is \(intStock!) today")
                    }
                }
            }
        }else{
            lbl_quantity.text = "0"
            lbl_quantity.isHidden = true
            txt_quantity.text = "0"
            AppDelegate.sharedInstance.appCart[Row].quantity = "0"
        }
        AppDelegate.sharedInstance.UpdateCost(lbl_itemsCost: lbl_itemsCost)
        delegate?.InfoVc_ProductQuantity_Updated(quantity: AppDelegate.sharedInstance.appCart[Row].quantity)
        Update_lbl_badge()
    }
    //    MARK: - TextField delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let Appearance = SCLAlertView.SCLAppearance()
        let productQuntityAlert = SCLAlertView(appearance: Appearance)
        let txt = productQuntityAlert.addTextField()
        txt.keyboardType = UIKeyboardType.numberPad
        productQuntityAlert.addButton("Done") {

            self.lbl_quantity.text = txt.text!
              self.updateProductQunatity(sender: nil,Quantity: txt.text!)
            self.updateProdctPriceLbl()
        }
        productQuntityAlert.showEdit(AppName,subTitle:"Change Product Quantity",closeButtonTitle: "Cancel",colorStyle: 999999)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            txt.becomeFirstResponder()
        }
        
    }
    
    
    //    MARK: - Fetch Product detail Info
    func fetchProductDetails(){
        
            HUD.show(.progress)
        
        
        let param = ["type":"product_detail_info" , "office_name": office_name , "product_id":productData.id!, "customer_id" : "" ] as [String:String]
        print(param)
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: param).responseJSON(completionHandler: {(response)
                in
                HUD.hide()
//                print(response.result.value)
//                print(response.request?.url)
                var details = [[String:AnyObject]]()
                switch response.result{
                case .success:
                    if let dict = response.result.value as? [String:AnyObject]{
                        if dict["message"] as! String == "Details Found"{
                        let data = dict["DATA"] as! [String:AnyObject]
                        details = data["products"] as! [[String:AnyObject]]
                            
                        self.lbl_info.text = details[0]["product_info"] as? String
                            self.lbl_info.text = self.lbl_info.text?.withoutHtml
                            
                            self.lbl_details.text = details[0]["product_detail"] as? String
                            self.lbl_details.text = self.lbl_details.text?.withoutHtml
                            
                            self.lbl_description.text = details[0]["product_description"] as? String
                            self.lbl_description.attributedText = self.lbl_description.text?.html2AttributedString
                            
//                            prod
                            
//                        self.lbl_disclaimer.text = details[0]["product_desclaimer"] as? String
                        }
                                }
                case  .failure(let error):
                    print(error.localizedDescription)
                }
            })
        
    }
////    MARK: - Fetch Product imgs
//    func fetchProductImgs(){
//        if internetConnectionIsOk(){
//            HUD.show(.progress)
//            let param : [String:String] = ["":""]
//        }
//    }
    //    MARK: - Fetch Recommended Products
    func fetchRecommendedPrdoducts(){
        if internetConnectionIsOk(){
            HUD.show(.progress)
            let param : [String:String] = ["product_id":productData.id!,"type":"order_detail_recomm","office_name":office_name]
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: param).responseJSON(completionHandler: {(response)
                in
                HUD.hide()
                switch response.result{
                case .success:
                    recomendedProducts = []
                    print(response.request?.url)
                    print(response.result.value)
                    let  dict = response.result.value as! [String:AnyObject]
                    if  dict["message"] as! String == "Available products"{
                        if let data = dict["DATA"]?["linked_products"] as? [[String : AnyObject]]{
                            //                            print(AppDelegate.sharedInstance.appCart)
                            for i in 0..<data.count{
                                let PD = ProductData()
                                
                                if let productID = data[i]["product_id"] as? String{
                                    PD.id = productID
                                    PD.quantity = getQunatityFromAppCart(ProductID: PD.id!)
                                    PD.info = data[i]["product_info"] as? String
                                    PD.info = PD.info?.withoutHtml
                                    PD.Description = data[i]["product_description"] as? String
                                    PD.Description = PD.info?.withoutHtml
                                    PD.Stock = data[i]["product_stock"] as? String
                                    PD.title = data[i]["product_title"] as? String
                                    PD.orignalPrice = data[i]["p_market_cutoff_price"] as? String
                                    PD.discount = data[i]["p_discount_perc"] as? String
                                    PD.Ourprice = data[i]["p_sell_price"] as? String
                                    
                                    
                                    
                                    if let rating = data[i]["product_rating"] as? Int{
                                        PD.rating = String(rating)
                                    }
                                    
                                    if let int_sold = data[i]["product_sold"] as? Int{
                                        PD.sold = String(int_sold)
                                    }

                                    
                                    let imgUrl = data[i]["product_image"] as? String ?? ""
                                    PD.imageUrl = URL(string:imgUrl)
                                    PD.otherImgUrl = data[i]["other_img"] as? String
                                    
                                    
                                    recomendedProducts.append(PD)
                                }
                            }
                            
                            self.tbl_recommend_products.reloadData()
                        }
                    }else{
                        DispatchQueue.main.async {
                            appDelegate.showerror(str:dict["message"] as? String)
                        }
                    }
                case .failure(let error):
                    SCLAlertView().showError(error.localizedDescription)
                }
            })
        }
    }
    
//    MARK: -  Buttons Image View
    @IBAction func btn_imageView_tapped(sender:UIButton){
        
        var indexpths : [NSIndexPath] = []
        for i in 0..<collection_view_imgs.numberOfSections{
            for j in 0..<collection_view_imgs.numberOfItems(inSection: i){
                indexpths.append(NSIndexPath(item: j, section: i))
            }
        }
        
        var imageNumber = sender.tag
        
        repeat{
            
            if imageNumber > arr_downloaded_product_images.count{
                imageNumber -= 1
            }
            
        }while(imageNumber > arr_downloaded_product_images.count)
        
        if imageNumber == 1{
            
            view_image_1.layer.borderColor = UIColor.green.cgColor
            view_image_2.layer.borderColor = UIColor(hex: themeColour)?.cgColor
            view_image_3.layer.borderColor = UIColor(hex: themeColour)?.cgColor
            
        collection_view_imgs.scrollToItem(at: indexpths[0] as IndexPath, at: .left, animated: true)
        }else if imageNumber == 2{
            
            view_image_2.layer.borderColor = UIColor.green.cgColor
            view_image_1.layer.borderColor = UIColor(hex: themeColour)?.cgColor
            view_image_3.layer.borderColor = UIColor(hex: themeColour)?.cgColor
        collection_view_imgs.scrollToItem(at: indexpths[1] as IndexPath, at: .left, animated: true)
        }else if imageNumber == 3{
            
           view_image_3.layer.borderColor = UIColor.green.cgColor
            view_image_2.layer.borderColor = UIColor(hex: themeColour)?.cgColor
            view_image_1.layer.borderColor = UIColor(hex: themeColour)?.cgColor
            collection_view_imgs.scrollToItem(at: indexpths[2] as IndexPath, at: .left, animated: true)
            
        }
        
    }

    
    
//    MARK: - Segments
    @IBAction func segment(_ sender: Any) {
        if segment.selectedSegmentIndex == 0 {
            view_product.isHidden = false
            scroll_product_details.isHidden = false
            recommendedProductsIsEnabled = false
            view_details.isHidden = true
            tbl_recommend_products.isHidden = true
        }else if segment.selectedSegmentIndex == 1 {
            scroll_product_details.isHidden = false
            view_details.isHidden = false
            view_product.isHidden = true
            tbl_recommend_products.isHidden = true
            recommendedProductsIsEnabled = false
        }else if segment.selectedSegmentIndex == 2{
            
            recommendedProductsIsEnabled = true
            scroll_product_details.isHidden = true
            if recomendedProducts.isEmpty{
                tbl_recommend_products.isHidden = true
            }else{
            tbl_recommend_products.isHidden = false
            }
        }
    }
    
    //    MARK: - Prepare For Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        recommendedProductsIsEnabled = false
        if segue.identifier == "gotoCartVc" {
        CartEnabled = true
        guard let navigationController = self.navigationController else {
            recommendedProductsIsEnabled = true
            return
            
            }
        var navigationArray = navigationController.viewControllers // To get all UIViewController stack as Array
        for i in 0..<navigationArray.count {
            if navigationArray[i].title == "Cart"{
                navigationArray.remove(at: i)
                self.navigationController?.viewControllers = navigationArray
                break
            }
        }
        }
    }
}
