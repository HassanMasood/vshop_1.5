//
//  OrderCell.swift
//  Food Service
//
//  Created by Hassan  on 11/03/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import SCLAlertView
import CoreData
protocol PendingCellDelegate: class {
//    func DeleteButtonPressedPendingOrderCell(objectID:NSManagedObjectID,cell:PendingOrdersCell)
    func cancelButtonPressedPendingOrderCell(cell:PendingOrdersCell)
    func EditButtonPressedPendingOrderCell(objectID:NSManagedObjectID,cell:PendingOrdersCell)
    func paynowButtonPressedPendingOrderCell(cell:PendingOrdersCell)
}

class PendingOrdersCell: UITableViewCell {

    @IBOutlet weak var lbl_selected_date: UILabel!
    @IBOutlet weak var lbl_selected_time_slot: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_address: UILabel!
    @IBOutlet weak var lbl_delivery_type: UILabel!
    @IBOutlet weak var lbl_total_price: UILabel!
    @IBOutlet weak var lbl_total_products: UILabel!
    @IBOutlet weak var lbl_order_date: UILabel!
    @IBOutlet weak var lbl_ID: UILabel!
    @IBOutlet weak var lbl_mobile: UILabel!
    @IBOutlet weak var btn_edit: UIButton!
    @IBOutlet weak var btn_delete: UIButton!
    @IBOutlet weak var btn_payNow: UIButton!
    @IBOutlet weak var lbl_pickup_address : UILabel!
//    @IBOutlet weak var btn_edit_address : UIButton!
    
    var objectID = NSManagedObjectID()
    
    weak var delegate : PendingCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupButtonUI(for: btn_delete)
        setupButtonUI(for: btn_edit)
        setupButtonUI(for: btn_payNow)
//        setupButtonUI(for: btn_edit_address)
            }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    func setupButtonUI(for button: UIButton) {
        button.layer.cornerRadius = button.frame.size.height / 10
        button.layer.shadowColor = button.backgroundColor?.cgColor
        button.layer.shadowRadius = 0.0
        button.layer.masksToBounds = false
    }
    
    
    //    MARK: - New Address
        @IBAction func editAddress(_ sender: Any) {
             let Appearance = SCLAlertView.SCLAppearance()
                         let alert = SCLAlertView(appearance: Appearance)
                         let txt = alert.addTextField()
            txt.placeholder = "Enter Address"
                   DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                       txt.becomeFirstResponder()
                   }
            alert.addButton("Done") {
                self.lbl_address.text = txt.text
            }
            alert.showEdit("New Address",closeButtonTitle: "Cancel",colorStyle: 999999)
        }
    
    @IBAction func deleteOrder(_ sender: Any){
//        print(objectID)
        delegate?.cancelButtonPressedPendingOrderCell(cell: self)
    }
    
    @IBAction func edit(_ sender: Any){
        delegate?.EditButtonPressedPendingOrderCell(objectID: objectID, cell: self)
        
    }
    
    @IBAction func payNow(_sender : Any){
        delegate?.paynowButtonPressedPendingOrderCell(cell: self)
    }
    
    
}
