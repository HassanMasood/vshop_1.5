//
//  ChooseBranchVc.swift
//  Food Service
//
//  Created by Hassan  on 18/04/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import SCLAlertView
import PKHUD
class ChooseBranchVc: CommonVc , UITableViewDelegate , UITableViewDataSource {
   
    
    @IBOutlet weak var tbl_branches: UITableView!
    
    var arr_branches = [Branch]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fetchStores()

    }
    
//    MARK:- Table View Data source
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arr_branches.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "branchCell") as! BranchCell
        cell.lbl_branch_title.text = arr_branches[indexPath.row].title
        cell.lbl_branch_address.text = arr_branches[indexPath.row].address
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedBranch = arr_branches[indexPath.row]
        performSegue(withIdentifier: "gotoDeliverySection", sender: self)
    }
//    MARK: - Fetch Stores Info
    func fetchStores(){
        let param = ["office_name":office_name,"type":"store_list"]  as [String:String]
        HUD.show(.progress)
        APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: param).responseJSON(completionHandler: {(response)
            in
            HUD.hide()
            switch response.result {
            case .success:
                print(response.result.value)
                print(response.request?.url)
                
                let dict = response.result.value as! [String:AnyObject]
                if dict["message"] as! String == "Available store list" {
                   let data = dict["DATA"] as! [String:AnyObject]
                    let storelist = data["store_list"] as! [[String:AnyObject]]
                    for i in 0..<storelist.count{
                        let branch = Branch()
                        branch.title = storelist[i]["pickup_title"] as? String ?? ""
                        branch.storeLogo = storelist[i]["pickup_store_logo"] as? String ?? ""
                        branch.postcode = storelist[i]["pickup_postcode"] as? String ?? ""
                        branch.phone = storelist[i]["pickup_phone"] as? String ?? ""
                        branch.minOrder = storelist[i]["pickup_minimun_orders"] as? String ?? ""
                        branch.deliveryFee = storelist[i]["pickup_delivery_fee"] as? String ?? ""
                        branch.city = storelist[i]["pickup_city"] as? String ?? ""
                        branch.address = storelist[i]["pickup_address"] as? String ?? ""
                        
                        
                        self.arr_branches.append(branch)
                        
                    }
                }else {
                    appDelegate.showerror(str: dict["message"] as? String)
                }
                
                if self.arr_branches.isEmpty {
                    self.tbl_branches.isHidden = true
                    
                }else {
                    self.tbl_branches.isHidden = false
                }
                self.tbl_branches.reloadData()
 
 
 
            case .failure(let error):
                appDelegate.showerror(str: error.localizedDescription)
            }
        })
    }
//    MARK: - Prepare For Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! DeliverySectionVc
        destination.sender.title = self.title
    }
    
}
