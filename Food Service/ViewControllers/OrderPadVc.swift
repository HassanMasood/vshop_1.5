//
//  OrderPadVc.swift
//  Food Service
//
//  Created by Hassan  on 09/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import PKHUD
import SCLAlertView

class OrderPadVc: CommonVc,UITextFieldDelegate  {

    @IBOutlet weak var btn_delivery_order: UIButton!
    @IBOutlet weak var btn_collect_order: UIButton!
    @IBOutlet weak var lbl_order_num: UITextField!
    @IBOutlet weak var view_clickCollect : UIView!
    @IBOutlet weak var view_delivery : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        setupUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupUI()
    }
//    MARK: - SetupUI
    
    func setupUI(){
        setupButtonUI(for: btn_collect_order)
        setupButtonUI(for: btn_delivery_order)
        
        if AppDelegate.sharedInstance.deliveryStatus == "0"{
            //    segue performed from side menu vc
            if let Dra = self.drawer(){
                       Dra.showLeftSlider(isShow: false)
                       Dra.setMainWith(identifier: "gotoOrderPad2Vc")
                   }

        }else if AppDelegate.sharedInstance.deliveryStatus == "1"{
            view_clickCollect.isHidden = true
        }else if AppDelegate.sharedInstance.deliveryStatus == "2"{
            view_delivery.isHidden = true
        }
    }
    
    
//    MARK: - Button Delivery Order
    
    @IBAction func deliveryOrder(_ sender: Any) {
        deliveryType = "delivery_selection"
        fetchDateTime()
//        performSegue(withIdentifier: "gotoDeliverySection", sender: self)
    }
    //    MARK: - Button Click and collect
    
    @IBAction func clickCollect(_ sender: Any) {
        deliveryType = "click_and_collect"
//        fetchDateTime()
        performSegue(withIdentifier: "gotoChooseBranch", sender: self)
    }
    
    //    MARK: - Side Menu
    @IBAction func sideMenu(_ sender: Any) {
        if let drawer = self.drawer() ,
            let manager = drawer.getManager(direction: .left){
            let value = !manager.isShow
            drawer.showLeftSlider(isShow: value)
        }
    }
    
//    MARK: - TextField Delegate
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        let paramter = ["office_name":office_name,"type":"search_product","search_text":textField.text!] as [String : Any]
//        APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: paramter).responseJSON(completionHandler: {(response)
//            in
//            print(response.result.value)
//            print(response.request?.url)
//
//        })
//    }

    
    //    MARK: - fetch date & time slots
        func fetchDateTime(){
            if internetConnectionIsOk(){
                let C_date = Date()
                let date_formater = DateFormatter()
                date_formater.dateFormat = "dd-MM-yyyy"
                let today = date_formater.string(from: C_date)
                //            print(today)
                HUD.show(.progress)
                var parameters = ["slot_date":today,"type":"future_slot_details","office_name":office_name] as [String:String]
                if deliveryType == "click_and_collect"{
                    date_formater.dateFormat = "HH:mm"
                    let time = date_formater.string(from: C_date)
                    print(time)
                    parameters = ["slot_date":today,"slot_time":time,"type":"click_collect_slots","office_name":office_name] as [String:String]
                }
    //            let parameters = ["slot_date":today,"type":"future_slot_details","office_name":office_name]
                APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameters).responseJSON(completionHandler: {(response) in
                    HUD.hide()
                                    print(response.result.value)
                                    print(response.request?.url)
                    switch response.result{
                    case .success:
                        let dict = response.result.value as! [String:AnyObject]
                        if dict["message"] as! String == "Available Slots"{
                            if deliveryType == "delivery_selection"{
                                self.performSegue(withIdentifier: "gotoDeliverySection", sender: self)
                            }else{
                                self.performSegue(withIdentifier: "gotoChooseBranch", sender: self)
                            }
                            
                        }else if dict["message"] as! String == "Any slot not available"{
   
                            
    //                        --- side menu segue ---
                            if let Dra = self.drawer(){
                                Dra.showLeftSlider(isShow: false)
                                Dra.setMainWith(identifier: "gotoOrderPad2Vc")
                            }

                            
                        }
                        else{
                            SCLAlertView().showError(dict["message"] as! String)
                        }
                    case .failure(let error):
                        SCLAlertView().showError(error.localizedDescription)
                    }
                })
            }
        }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if segue.identifier == "gotoDeliverySection" {
            let destination = segue.destination as! DeliverySectionVc
        destination.sender.title = "Select Service"
       }
        
    }
    
}
