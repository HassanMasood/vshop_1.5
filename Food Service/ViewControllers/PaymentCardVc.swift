//
//  PaymentCardVc.swift
//  Food Service
//
//  Created by Hassan  on 09/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit

class PaymentCardVc: CommonVc {

    @IBOutlet weak var btn_add_card: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
            
        
    }
    
    func setupUI(){
        setupButtonUI(for: btn_add_card)
    }
    
    @IBAction func sideMenu(_ sender: Any) {
        if let drawer = self.drawer() ,
            let manager = drawer.getManager(direction: .left){
            let value = !manager.isShow
            drawer.showLeftSlider(isShow: value)
        }
    }
    
}
