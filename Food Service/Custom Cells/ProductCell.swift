//
//  Product_cell.swift
//  Food Service
//
//  Created by Hassan  on 24/02/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import SCLAlertView

@objc protocol productCellDelegate: class  {
    func quantityUpdated(tag:Int)
    func update_ProductID_For_AppCart(cell:UITableViewCell )
    @objc optional func productCellInfoButtonPressed(tag:Int)
    @objc optional func deleteItemButtonPressed(cell:UITableViewCell)
}

class ProductCell: UITableViewCell,UITextFieldDelegate {
    @IBOutlet weak var view_bottom: UIView!
    @IBOutlet weak var lbl_product_id : UILabel!
    @IBOutlet weak var lbl_product_title : UILabel!
    @IBOutlet weak var lbl_product_our_price : UILabel! //    selling Price/Our price
    @IBOutlet weak var lbl_product_orignal_price : UILabel!
    @IBOutlet weak var lbl_product_description: UILabel!
    @IBOutlet weak var img_Product: UIImageView!
    @IBOutlet weak var btn_fav: UIButton!
    @IBOutlet weak var btn_plus: UIButton!
    @IBOutlet weak var btn_info: UIButton!
    @IBOutlet weak var btn_minus: UIButton!
    @IBOutlet weak var lbl_quantity: UILabel!
    @IBOutlet weak var txt_quantity: UITextField!
    @IBOutlet weak var lbl_discount: UILabel!
//    @IBOutlet weak var shadow_view: UIView!
    @IBOutlet weak var lbl_discountDate: UILabel!
//    @IBOutlet weak var lbl_rating: UILabel!
//    @IBOutlet weak var lbl_sold: UILabel!
//    @IBOutlet weak var img_product_rating : UIImageView!
    @IBOutlet weak var view_orignal_price : UIView!
    
    @IBOutlet weak var Stack_orignal_price : UIStackView!
//    func animate() {
//        UIView.animate(withDuration: 0.5, delay: 0.3, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
//            self.contentView.layoutIfNeeded()
//        })
//    }
    
    weak var delegate : productCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUI()
        txt_quantity.delegate = self

    }
    
    
    
    
//    MARK: - SetUP UI
    func setUI(){
//        img_Product.layer.cornerRadius = img_Product.frame.size.height / 2
        lbl_quantity.layer.cornerRadius = lbl_quantity.frame.size.height / 2
        lbl_quantity.layer.masksToBounds = true

//             backgroundColor = UIColor.clear
     //            Colors.colorClear

//             self.shadow_view.layer.borderWidth = 1
//     //        1
//             self.shadow_view.layer.cornerRadius = 3
//     //        3
//             self.shadow_view.layer.borderColor = UIColor.clear.cgColor
//             self.shadow_view.layer.masksToBounds = true
//
//             self.layer.shadowOpacity = 0.12
//             self.layer.shadowOffset = CGSize(width: 0, height: 2)
//             self.layer.shadowRadius = 2
//             self.layer.shadowColor = UIColor.black.cgColor
//     //            .colorBlack.cgColor
//             self.layer.masksToBounds = false
        
    }
    
//    MARK: -
    
//    MARK: - set Selected
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
// MARK: -  Button Favrouite
    @IBAction func Fav(_ sender: UIButton){
        if AppDelegate.sharedInstance.userLoggedIn{
        sender.isSelected = !sender.isSelected
        delegate?.update_ProductID_For_AppCart(cell: self)
        delegate?.quantityUpdated(tag: self.tag)
        updateFav(sender)
        }else{
            loginResquest(VC: self.viewContainingController()! , nextViewController: nil)
        }
    }
    
//    MARK: - Delete Item Button pressed
    
    @IBAction func delete_item(_ sender: Any) {
        delegate.self?.deleteItemButtonPressed?(cell: self)
    }
    //    MARK: - Update Fav
    func updateFav(_ sender:UIButton){
        var fav_tpye = ""
        if sender.isSelected == true {
            fav_tpye = "yes"
        }else{
            fav_tpye = "no"
        }
        
            let dict = UserDefaults.standard.value(forKey:"user_information") as? [String:AnyObject]
            let parameter = ["type":"save_favourite_product","favourite_type":fav_tpye,"customer_id":dict!["user_id"] as! String ,"product_id":lbl_product_id.text!,"office_name":office_name] as [String:String]
            APIManager.sharedInstance.sessionManager.request(BaseURL,parameters: parameter).responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    print(response.request?.url)
                    print(response.result.value)
                    let data = response.result.value as! [String:AnyObject]
                    if fav_tpye == "yes"{
                    if data["message"] as! String == "saved"{
                        self.btn_fav.tintColor = UIColor.init(hex: themeColour)
                        appDelegate.showSuccess(str: "Saved in favourites")
                        AppDelegate.sharedInstance.appCart[Row].favorite = "yes"
                    }else{
                        SCLAlertView().showError(data["message"] as! String)
                    }
                    }else if fav_tpye == "no"{
                        self.btn_fav.tintColor = UIColor.lightGray
                        AppDelegate.sharedInstance.appCart[Row].favorite = "no"
                    }
                case .failure(let error):
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: error.localizedDescription)
                    })
                }
            })
    }

    
//    MARK: - Info Button
    @IBAction func info(_ sender: Any) {
//        updateRow()
        delegate?.productCellInfoButtonPressed?(tag: self.tag)
    }
//    MARK: - Buttons Quantity
    @IBAction func UpdateQuntity(_ sender: UIButton) {
        updateProductQunatity(sender: sender, Quantity: nil)
//        updateQunatitySearchedProducts()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let Appearance = SCLAlertView.SCLAppearance()
        let alert = SCLAlertView(appearance: Appearance)
        let txt = alert.addTextField()
        txt.keyboardType = UIKeyboardType.numberPad
        alert.addButton("Done") {
        self.updateProductQunatity(sender: nil,Quantity: txt.text!)
        }
        alert.showEdit("Change Product Quantity",closeButtonTitle: "Cancel",colorStyle: 999999)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            txt.becomeFirstResponder()
        }
    }
//    MARK: - Update Product Quantity
    func updateProductQunatity(sender:AnyObject?,Quantity:String?){
//        updateRow()
        delegate?.update_ProductID_For_AppCart(cell: self)
       if IsProductFound{
        var quantity = Int(AppDelegate.sharedInstance.appCart[Row].quantity) ?? 0
       
        if sender != nil{
            if sender?.tag == 0 {
                quantity += 1
            }else{
                if quantity > 0{
                    quantity -= 1
                }
            }
        }else{
            if Quantity != ""{
               quantity = Int(Quantity!) ?? 0
            }else{
                quantity = 0
            }
        }
        if quantity > 0{
            // check Product Stock
            if let Stock = AppDelegate.sharedInstance.appCart[Row].Stock{
              let  intStock = Int(Stock)
                if (intStock! + 1) > quantity{
                    AppDelegate.sharedInstance.appCart[Row].quantity = String(quantity)
//                    updateAppCart(productID: arr_Products[Row].id!, qunatity: arr_Products[Row].quantity)
                    lbl_quantity.text = String(quantity)
                    txt_quantity.text = String(quantity)
                    lbl_quantity.isHidden = false
                    
                }else{
                    if intStock == 0 || intStock! < 0 {
                        appDelegate.showerror(str: "Product out of stock today")
                    }else{
                    appDelegate.showerror(str: "Maximum allowed quantity is \(intStock!) today")
                    }
                }
            }
        }else{
            lbl_quantity.text = "0"
            lbl_quantity.isHidden = true
            txt_quantity.text = "0"
            AppDelegate.sharedInstance.appCart[Row].quantity = "0"
//            updateAppCart(productID: arr_Products[Row].id!, qunatity: arr_Products[Row].quantity)
            }
        delegate?.quantityUpdated(tag: self.tag)
        }else{
        appDelegate.showerror(str: "Product of stock today.")
        }
    }
    
//    func updateQunatitySearchedProducts(){
//        for i in 0..<AppDelegate.sharedInstance.appCart.count{
//            if Int(AppDelegate.sharedInstance.appCart[i].quantity)! > 0 {
//                let id = AppDelegate.sharedInstance.appCart[i].id
//                let qunatity = AppDelegate.sharedInstance.appCart[i].quantity
//                for i in 0..<searchedProducts.count{
//                    if id == searchedProducts[i].id{
//                        searchedProducts[i].quantity = qunatity
//                    }
//                }
//            }
//        }
//    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    

}
