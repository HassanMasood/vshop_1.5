//
//  CheckOutCell.swift
//  Food Service
//
//  Created by Hassan  on 15/03/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit

class CheckOutCell: UITableViewCell {

    @IBOutlet weak var lbl_product_id : UILabel!
    @IBOutlet weak var lbl_product_title : UILabel!
    @IBOutlet weak var lbl_product_price : UILabel!
    @IBOutlet weak var lbl_quantity: UILabel!
    @IBOutlet weak var img_Product: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        func setUI(){
            img_Product.layer.cornerRadius = img_Product.frame.size.height / 2
            lbl_quantity.layer.cornerRadius = lbl_quantity.frame.size.height / 2
            lbl_quantity.layer.masksToBounds = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
