//
//  Branch.swift
//  Food Service
//
//  Created by Hassan  on 15/05/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import Foundation

class Branch {
    var address = String()
    var city = String()
    var deliveryFee = String()
    var minOrder = String()
    var phone = String()
    var postcode = String()
    var storeLogo = String()
    var title = String()
}
