//
//  PreviousPurchasesCell.swift
//  Food Service
//
//  Created by Hassan  on 03/04/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit
import SCLAlertView

protocol PurchasescellDelegate: class  {
    func deleteButtonPressed(orderId:String,cell:PurchasesCell)
}
class PurchasesCell: UITableViewCell {

    @IBOutlet weak var lbl_callerName : UILabel!
//    @IBOutlet weak var lbl_deliveryCharges : UILabel!
    @IBOutlet weak var lbl_destination : UILabel!
    @IBOutlet weak var lbl_orderId : UILabel!
    @IBOutlet weak var lbl_notes : UITextView!
    @IBOutlet weak var lbl_mobile : UILabel!
//    @IBOutlet weak var lbl_pickup : UILabel!
    @IBOutlet weak var lbl_totalPrice : UILabel!
    @IBOutlet weak var lbl_totalItems : UILabel!
    @IBOutlet weak var lbl_totalProducts : UILabel!
    @IBOutlet weak var stack_labels: UIStackView!
    
    @IBOutlet weak var btn_delete : UIButton!
    @IBOutlet weak var btn_details : UIButton!
    
    @IBOutlet weak var stack_buttons : UIStackView!
    @IBOutlet weak var stack_comments: UIStackView!
    
    @IBOutlet weak var view_line : UIView!
    
    weak var delegate : PurchasescellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUp()
        let height = stack_labels.bounds.size.height
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    func setUp(){
        setupButtonUI(for: btn_delete)
        setupButtonUI(for: btn_details)
    }
    
    func setupButtonUI(for button: UIButton) {
        button.layer.cornerRadius = button.frame.size.height / 10
        button.layer.shadowColor = button.backgroundColor?.cgColor
        button.layer.shadowRadius = 0.0
        button.layer.masksToBounds = false
    }
// MARK: - Button Delete Order
    @IBAction func deleteOrder(_ sender: Any) {
        delegate?.deleteButtonPressed(orderId: lbl_orderId.text!, cell: self)

    }
}
