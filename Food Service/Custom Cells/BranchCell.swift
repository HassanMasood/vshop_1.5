//
//  BranchCell.swift
//  Food Service
//
//  Created by Hassan  on 15/05/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit

class BranchCell: UITableViewCell {

    @IBOutlet weak var lbl_branch_title : UILabel!
    @IBOutlet weak var lbl_branch_address : UILabel!
    @IBOutlet weak var backView : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = UIColor.clear
//            Colors.colorClear

        self.backView.layer.borderWidth = 1
//        1
        self.backView.layer.cornerRadius = 3
//        3
        self.backView.layer.borderColor = UIColor.clear.cgColor
        self.backView.layer.masksToBounds = true

        self.layer.shadowOpacity = 0.12
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 2
        self.layer.shadowColor = UIColor.black.cgColor
//            .colorBlack.cgColor
        self.layer.masksToBounds = false
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
