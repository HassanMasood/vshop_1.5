//
//  CategoryCollectionViewCell.swift
//  Food Service
//
//  Created by Hassan  on 28/09/2020.
//  Copyright © 2020 Hassan . All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image_View_product : UIImageView!
    @IBOutlet weak var lbl_title : UILabel!
}
